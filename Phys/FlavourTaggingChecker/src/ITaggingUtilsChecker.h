/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef ITAGGINGUTILSCHECKER_H
#define ITAGGINGUTILSCHECKER_H 1

#include "Event/GenHeader.h"
#include "Event/MCParticle.h"
#include "Event/Particle.h"
#include "Event/RecVertex.h"
#include "GaudiKernel/IAlgTool.h"
#include <string>

static const InterfaceID IID_ITaggingUtilsChecker( "ITaggingUtilsChecker", 1, 0 );

/** @class ITaggingUtilsChecker ITaggingUtilsChecker.h
 *
 *
 *  @author Marco Musy, Marc Grabalosa
 *  @date   2007-06-09
 */

class ITaggingUtilsChecker : virtual public IAlgTool {

public:
  /// Retrieve interface ID
  static const InterfaceID& interfaceID() { return IID_ITaggingUtilsChecker; };

  virtual StatusCode calcIP( const LHCb::Particle*, const LHCb::VertexBase*, double&, double& ) = 0;

  virtual StatusCode calcIP( const LHCb::Particle*, const LHCb::RecVertex::ConstVector&, double&, double& ) = 0;

  virtual StatusCode calcIPPU( const LHCb::Particle*, const LHCb::Particle*, const LHCb::RecVertex::ConstVector&,
                               const double, double&, double&, double&, double&, double&, double&, double&, double&,
                               double&, int&, double&, double& ) = 0;

  virtual int countTracks( LHCb::Particle::ConstVector& ) = 0;

  virtual const LHCb::Particle* motherof( const LHCb::Particle*, const LHCb::Particle::ConstVector& ) = 0;

  virtual bool isinTree( const LHCb::Particle*, LHCb::Particle::ConstVector&, double& ) = 0;

  virtual LHCb::Particle::ConstVector FindDaughters( const LHCb::Particle* ) = 0;

  virtual const LHCb::MCParticle* originof( const LHCb::MCParticle* ) = 0;

  virtual int comes_from_excitedB( const LHCb::MCParticle*, const LHCb::MCParticle* ) = 0;

  virtual StatusCode calcDOCAmin( const LHCb::Particle*, const LHCb::Particle*, const LHCb::Particle*, double&,
                                  double& ) = 0;
};
#endif // ITAGGINGUTILSCHECKER_H
