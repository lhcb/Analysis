/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef DEBUG_BTAGGINGINCLUSIVE_H
#define DEBUG_BTAGGINGINCLUSIVE_H 1

// Include files
#include "Event/FlavourTag.h"
#include "MCInterfaces/IPrintMCDecayTreeTool.h"

#include "Event/GenCollision.h"
#include "Event/GenHeader.h"
#include "Event/HepMCEvent.h"
#include "Kernel/IEvtTypeSvc.h"
#include "MCInterfaces/IMCDecayFinder.h"
#include "MCInterfaces/IPrintMCDecayTreeTool.h"
//#include "MCInterfaces/IForcedBDecayTool.h"
#include "IBDecayTool.h"

// from DaVinci
#include "Kernel/DaVinciAlgorithm.h"

// from local

/** @class BTaggingInclusive BTaggingInclusive.h debug/BTaggingInclusive.h
 *
 *
 *  @author Onuora Awunor
 *  @date   2006-05-11
 */
class BTaggingInclusive : public DaVinciAlgorithm {
public:
  /// Standard constructor
  BTaggingInclusive( const std::string&, ISvcLocator* );

  virtual ~BTaggingInclusive(); ///< Destructor

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

private:
  /// Vector of locations of the tags to monitor
  std::string m_tagslocation;
  std::string m_ostagslocation;

  IPrintMCDecayTreeTool* m_debug;

  IBDecayTool*      m_forcedBtool;
  LHCb::MCParticle* associatedofHEP( HepMC::GenParticle* );

  int nsele, nrt[50], nwt[50];
};
#endif // DEBUG_BTAGGINGINCLUSIVE_H
