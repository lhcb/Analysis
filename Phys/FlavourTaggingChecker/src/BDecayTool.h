/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: BDecayTool.h,v 1.2 2007-05-29 08:48:16 cattanem Exp $
#ifndef BDECAYTOOL_H
#define BDECAYTOOL_H 1
// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"

// from Event
#include "Event/GenHeader.h"
#include "Event/MCParticle.h"

// Interface
//#include "MCInterfaces/IBDecayTool.h"
#include "IBDecayTool.h"
#include "MCInterfaces/IMCDecayFinder.h"

/** @class BDecayTool BDecayTool.h
 *
 */

class BDecayTool : public GaudiTool, virtual public IBDecayTool {
public:
  /// Standard constructor
  BDecayTool( const std::string& type, const std::string& name, const IInterface* parent );

  ~BDecayTool(); ///< Destructor

  /// Initialize
  StatusCode initialize() override;

  /// Finalize
  StatusCode finalize() override;

  const LHCb::MCParticle* forcedB( void ) override;

  // IMCDecayFinder* mcDecayFinder() const;
  IMCDecayFinder* m_mcDecay; //< the DecayTypeFinder tool

private:
  // IMCDecayFinder* m_mcDecay;         //< the DecayTypeFinder tool

  // LHCb::MCParticle* associatedofHEP(HepMC::GenParticle* ) ;
};
#endif // BDECAYTOOL_H
