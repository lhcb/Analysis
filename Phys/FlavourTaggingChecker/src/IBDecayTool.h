/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef IBDECAYTOOL_H
#define IBDECAYTOOL_H 1

#include "GaudiKernel/IAlgTool.h"
#include <string>

namespace LHCb {
  class MCParticle;
}

static const InterfaceID IID_IBDecayTool( "IBDecayTool", 1, 0 );

/** @class IBDecayTool IBDecayTool.h
 *
 */

class IBDecayTool : virtual public IAlgTool {
public:
  /// Retrieve interface ID
  static const InterfaceID& interfaceID() { return IID_IBDecayTool; };

  virtual const LHCb::MCParticle* forcedB( void ) = 0;
};
#endif // IBDECAYTOOL_H
