###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Phys/FlavourTaggingChecker
--------------------------
#]=======================================================================]

gaudi_add_module(FlavourTaggingChecker
    SOURCES
        src/BDecayTool.cpp
        src/BOppositeMonitoring.cpp
        src/BTaggingAnalysis.cpp
        src/BTaggingChecker.cpp
        src/BTaggingInclusive.cpp
        src/CheatedSelection.cpp
        src/TaggingHelpers.cpp
        src/TaggingUtilsChecker.cpp
    LINK
        Gaudi::GaudiAlgLib
        Gaudi::GaudiKernel
        LHCb::CaloUtils
        LHCb::DAQEventLib
        LHCb::GenEvent
        LHCb::HltEvent
        LHCb::L0Event
        LHCb::LHCbKernel
        LHCb::LoKiCoreLib
        LHCb::MCAssociatorsLib
        LHCb::MCEvent
        LHCb::MCInterfacesLib
        LHCb::PhysEvent
        LHCb::RecEvent
        LHCb::TrackEvent
        Phys::DaVinciInterfacesLib
        Phys::DaVinciKernelLib
        Phys::DaVinciMCKernelLib
        Phys::FlavourTaggingLib
        Phys::LoKiPhysLib
        Rec::TrackInterfacesLib
)

lhcb_env(SET FLAVOURTAGGINGCHECKEROPTS ${CMAKE_CURRENT_SOURCE_DIR}/options)
