/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: TupleToolRecoStats.h,v 1.7 2010-09-09 12:22:42 pkoppenb Exp $
#ifndef TUPLETOOLRECOSTATS_H
#define TUPLETOOLRECOSTATS_H 1

// Include files
// from Gaudi
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "Kernel/IEventTupleTool.h" // Interface

#include "Event/RecSummary.h"
#include "Event/Track.h"
#include "HltDAQ/IReportConvert.h"
#include "HltDAQ/ReportConvertTool.h"

/** @class TupleToolRecoStats TupleToolRecoStats.h
 *
 *  Fills Reco stats, from RecSummary
 *
 *  Columns written to the tuple:
 *
 *  - head_nPVs
 *  - nTracks
 *  - nLongTracks
 *  - nDownstreamTracks
 *  - nUpstreamTracks
 *  - nVeloTracks
 *  - nTTracks
 *  - nBackTracks
 *  - nRich1Hits
 *  - nRich2Hits
 *  - nVeloClusters
 *  - nITClusters
 *  - nTTClusters
 *  - nOTClusters
 *  - nSPDHits
 *  - nMuonCoordsS0
 *  - nMuonCoordsS1
 *  - nMuonCoordsS2
 *  - nMuonCoordsS3
 *  - nMuonCoordsS4
 *  - nMuonTracks
 *  @author Patrick Koppenburg, Fatima Soomro, Jibo He
 *  @date   2009-02-11
 */
class TupleToolRecoStats : public TupleToolBase, virtual public IEventTupleTool {

public:
  /// Standard constructor
  TupleToolRecoStats( const std::string& type, const std::string& name, const IInterface* parent );

  virtual ~TupleToolRecoStats();              ///< Destructor
  StatusCode fill( Tuples::Tuple& ) override; ///< Fill tuple

private:
  /// for converting objects in to summaries
  IReportConvert* m_conv;
};

#endif // TUPLETOOLRECOSTATS_H
