/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: TupleToolProtopinfo.h,v 1.1 2010-05-21 17:03:11 odescham Exp $
#ifndef _TUPLETOOLEPROTOPDATA_H
#define _TUPLETOOLEPROTOPDATA_H 1

// Include files
// from Gaudi
#include "DecayTreeTupleBase/TupleToolBase.h"
// Interface
#include "Event/ProtoParticle.h"
#include "Kernel/IParticleTupleTool.h"

/** @class TupleToolProtoPData TupleToolProtoPData.h
 * \brief Fills the additional info of proto particle
 *
 * Columns filled:
 * - head_PP_ + additional info
 *
 *  @author Olivier Deschamps
 *  @date   2011-06-08
 */

//============================================================================
class TupleToolProtoPData : public TupleToolBase, virtual public IParticleTupleTool {
  //==========================================================================
public:
  // Standard constructor
  TupleToolProtoPData( const std::string& type, const std::string& name, const IInterface* parent );

  virtual ~TupleToolProtoPData(){};
  StatusCode fill( const LHCb::Particle*, const LHCb::Particle*, const std::string&, Tuples::Tuple& ) override;

private:
  std::vector<std::string> m_list;
  int                      m_last;
};
#endif // _TUPLETOOLEPROTOPDATA_H
