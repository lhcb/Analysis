/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#ifndef JBOREL_TUPLETOOLTRACKINFO_H
#define JBOREL_TUPLETOOLTRACKINFO_H 1

// Include files
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "Kernel/IParticleTupleTool.h"          // Interface
#include "TrackInterfaces/ITrackExtrapolator.h" // Interface

/** @class TupleToolTrackInfo TupleToolTrackInfo.h
 *
 * \brief Fill track information for DecayTreeTuple
 *
 * - "basic":
 *   - "head_TRACK_CHI2NDOF" : "track chi2/ndof"
 *   - "head_TRACK_Type" : "track type"
 *   - "head_TRACK_PCHI2" : "track Chi2 probability"
 *   - "head_TRACK_GhostProb" : "Ghost probability (run NeuralNetTmva to fill it)"
 *   - "head_TRACK_CloneDist" : "Only available for 2009 data"
 *   - "head_TRACK_Key" : "key of the track"
 *   - "head_TRACK_MatchCHI2" : "LHCb::Track::AdditionalInfo::FitMatchChi2"
 *   - "head_TRACK_Likelihood" : "likelihood of the track"
 *   - "Verbose":
 *     - "basic":
 *       - "head_TRACK_CHI2" : "track chi2"
 *       - "head_TRACK_NDOF" : "track ndof"
 *       - "head_TRACK_VeloCHI2NDOF" : "Track fit velo chi2/nDoF"
 *       - "head_TRACK_TCHI2NDOF" : "Track fit T chi2/nDoF"
 *       - "head_TRACK_VELO_UTID" : "hopefully unique double constructed from multiplying all Velo hit IDs"
 *       - "head_TRACK_TT_UTID" : "hopefully unique double constructed from multiplying all TT hit IDs"
 *       - "head_TRACK_IT_UTID" : "hopefully unique double constructed from multiplying all IT hit IDs"
 *       - "head_TRACK_OT_UTID" : "hopefully unique double constructed from multiplying all OT hit IDs"
 *       - "head_TRACK_nVeloHits" : "Number of Velo hits on the track"
 *       - "head_TRACK_nVeloRHits" : "Number of Velo R hits on the track"
 *       - "head_TRACK_nVeloPhiHits" : "Number of Velo phi hits on the track"
 *       - "head_TRACK_nVeloPileUpHits" : "Number of Velo pile-up hits on the track"
 *       - "head_TRACK_nTTHits" : "Number of TT hits on the track"
 *       - "head_TRACK_nITHits" : "Number of IT hits on the track"
 *       - "head_TRACK_nOTHits" : "Number of OT hits on the track"
 *       - "head_TRACK_History" : "Algorithm which the track was made with"
 *       - "head_TRACK_FirstMeasurementX" : "x position of state at 'FirstMeasurement'
 *       - "head_TRACK_FirstMeasurementY" : "y position of state at 'FirstMeasurement'
 *       - "head_TRACK_FirstMeasurementZ" : "z position of state at 'FirstMeasurement'
 *       - "head_TRACK_qOverp" : "q/p of state at 'FirstMeasurement'
 *       - "head_TRACK_Tx" : "x slope of state at 'FirstMeasurement'
 *       - "head_TRACK_Ty" : "y slope of state at 'FirstMeasurement'
 *
 *
 * \sa DecayTreeTuple
 *
 *  @author Stephane Poss
 *  @date   2008-03-17
 */

class TupleToolTrackInfo : public TupleToolBase, virtual public IParticleTupleTool {
public:
  /// Standard constructor
  TupleToolTrackInfo( const std::string& type, const std::string& name, const IInterface* parent );

  virtual ~TupleToolTrackInfo(){}; ///< Destructor

  StatusCode fill( const LHCb::Particle*, const LHCb::Particle*, const std::string&, Tuples::Tuple& ) override;
  StatusCode initialize() override; ///< init

private:
  void                                         defineStates();       ///< init
  std::vector<std::string>                     m_storedMeasurements; ///< Stored measurements
  std::set<LHCb::State::Location>              m_measurements;       ///< Stored measurements
  std::map<LHCb::State::Location, std::string> m_stateDict;          ///< name of states
  std::map<LHCb::State::Location, double>      m_stateZ;             ///< position of fixed states

  std::string         m_extrapolatorName; ///< State extrapolator
  ITrackExtrapolator* m_extrapolator;     ///< State extrapolator
};

#endif
