###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Phys/DisplVerticesMC
--------------------
#]=======================================================================]

gaudi_add_module(DisplVerticesMC
    SOURCES
        src/MCDisplVertices.cpp
        src/SaveParticlesFrom.cpp
    LINK
        LHCb::CaloDetLib
        LHCb::DAQEventLib
        LHCb::DetDescLib
        LHCb::DigiEventLib
        LHCb::GenEvent
        LHCb::HltEvent
        LHCb::HltInterfaces
        LHCb::L0Event
        LHCb::L0InterfacesLib
        LHCb::MCAssociatorsLib
        LHCb::MCEvent
        LHCb::PhysEvent
        LHCb::RecEvent
        Phys::DaVinciInterfacesLib
        Phys::DaVinciKernelLib
        Rec::TrackInterfacesLib
)
