###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# $Id$
# =============================================================================
## @file
#  The configuration file to run HepMC <--> MC stuff "on-demand"
#
#   This file is a part of LoKi project -
#     "C++ ToolKit  for Smart and Friendly Physics Analysis"
#
#  The package has been designed with the kind help from
#  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
#  contributions and advices from G.Raven, J.van Tilburg,
#  A.Golutvin, P.Koppenburg have been used in the design.
#
#  @see Kernel/HepMC2MC.h
#  @see LoKi::HepMC2MCAlg
#  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
#  @date   2008-11-04
# =============================================================================
"""
The configuration file to run HepMC <--> MC stuff 'on-demand'

This file is a part of LoKi project -
\"C++ ToolKit  for Smart and Friendly Physics Analysis\"

The package has been designed with the kind help from
Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
contributions and advices from G.Raven, J.van Tilburg,
A.Golutvin, P.Koppenburg have been used in the design.

"""
# =============================================================================
__author__ = " Vanya BELYAEV Ivan.Belyaev@nikhef.nl "
__version__ = " CVS Tag $Name: not supported by cvs2svn $, version $Revision$ "
# =============================================================================

from Gaudi.Configuration import *
from Configurables import DataOnDemandSvc
from Configurables import LoKi__HepMC2MCAlg

alg = LoKi__HepMC2MCAlg()

dod = DataOnDemandSvc(OutputLevel=2)
dod.AlgMap['Relations/Gen/HepMCEvents'] = alg.getFullName()

# =============================================================================
# The END
# =============================================================================
