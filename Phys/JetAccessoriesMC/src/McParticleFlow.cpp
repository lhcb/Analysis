/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Local.
#include "McParticleFlow.h"

//-----------------------------------------------------------------------------
// Implementation file for class : McParticleFlow
//
// 2016-05-25 : Cedric Potterat and Philip Ilten
//-----------------------------------------------------------------------------

// Declare the algorithm factory.
DECLARE_COMPONENT( McParticleFlow )

//=============================================================================
// Constructor.
//=============================================================================
McParticleFlow::McParticleFlow( const string& name, ISvcLocator* svc ) : GaudiHistoAlg( name, svc ) {
  // Declare the input/output properties.
  declareProperty( "Inputs", m_inLocs,
                   "PF inputs of the form [<class name>, <object type>, "
                   "<location>]." );
  declareProperty( "Output", m_outLoc = "Phys/PF/MCParticles", "PF output MCParticle location." );
}

//=============================================================================
// Initialize.
//=============================================================================
StatusCode McParticleFlow::initialize() {
  StatusCode sc = GaudiHistoAlg::initialize();
  if ( sc.isFailure() ) { return sc; }
  // Create the inputs.
  m_inPrts.clear();
  unordered_set<int> pidsBan, pidsPrt;
  for ( unsigned int inLoc = 0; inLoc < m_inLocs.size(); ++inLoc ) {
    Input in( &inLoc, &m_inLocs[inLoc], &pidsBan, &pidsPrt );
    if ( !in.valid )
      warning() << in.warn << endmsg;
    else if ( in.name == "MCParticle" )
      m_inPrts.push_back( in );
    else if ( in.name == "PID" )
      ;
    else
      warning() << "Skipping input element " << inLoc << " with unknown class '" << in.name << "'." << endmsg;
  }

  // Print the configuration, if requested.
  verbose() << "\n"
            << "*-------------------------------------------------------*\n"
            << "*             McParticleFlow Configuration              *\n"
            << "*-------------------------------------------------------*\n"
            << "Inputs:\n"
            << setw( 4 ) << "idx" << setw( 15 ) << "class name" << setw( 16 ) << "object type" << setw( 40 )
            << "location"
            << "\n";
  for ( vector<Input>::iterator inPrt = m_inPrts.begin(); inPrt != m_inPrts.end(); ++inPrt ) inPrt->print( verbose() );
  verbose() << endmsg;
  return StatusCode::SUCCESS;
}

//=============================================================================
// Execute.
//=============================================================================
StatusCode McParticleFlow::execute() {
  // Clear the event and create the output.
  double energy( 0 );
  m_used.clear();
  m_prts = new MCParticles();
  put( m_prts, m_outLoc );

  // Add the inputs.
  vector<Input>::iterator inPrt( m_inPrts.begin() );
  for ( unsigned int inLoc = 0; inLoc < m_inLocs.size(); ++inLoc ) {
    if ( inPrt == m_inPrts.end() || inLoc != inPrt->index ) continue;
    MCParticles* prts = getIfExists<MCParticles>( inPrt->loc );
    if ( !prts )
      warning() << "Could not retrieve " << inPrt->name << "s from " << inPrt->loc << endmsg;
    else {
      m_save.clear();
      int key( 0 );
      for ( MCParticles::iterator prt = prts->begin(); prt != prts->end(); ++prt ) {
        add( *prt, *inPrt, key );
        ++key;
      }
      for ( MCParticles::iterator prt = prts->begin(); prt != prts->end(); ++prt )
        if ( m_save.find( *prt ) != m_save.end() ) {
          m_prts->insert( *prt );
          energy += ( *prt )->momentum().e();
        }
    }
    ++inPrt;
  }
  verbose() << "Summed energy [GeV]: " << energy / Gaudi::Units::GeV << endmsg;
  setFilterPassed( true );
  return StatusCode::SUCCESS;
}

//=============================================================================
// Input structure class for PF.
//=============================================================================
// Constructor.
McParticleFlow::Input::Input( const unsigned int* idx, const vector<string>* prp, unordered_set<int>* idb,
                              unordered_set<int>* idp )
    : valid( false ), ban( false ), prt( false ) {
  if ( !idx ) {
    warn += "Invalid pointer to index.";
    return;
  }
  warn = "Skipping input element " + to_string( *idx ) + ", ";
  if ( !prp ) {
    warn += "invalid pointer to input list.";
    return;
  }
  if ( prp->size() != 3 ) {
    warn += "input list size " + to_string( prp->size() ) + ", must be 3.";
    return;
  }

  // Set the members.
  string s0( prp->at( 0 ) ), s1( prp->at( 1 ) ), s2( prp->at( 2 ) );
  if ( s2.empty() ) {
    warn += "empty location.";
    return;
  }
  if ( s1 != "ban" && s1 != "particle" && s1 != "daughters" ) {
    warn += "incompatible object type, " + s1 + " for class " + s0 + ".";
    return;
  } else if ( s0 == "MCParticle" ) {
    ban = ( s1 == "ban" );
    prt = ( s1 == "particle" );
    if ( idb ) pidsBan = *idb;
    if ( idp ) pidsPrt = *idp;
  } else if ( s0 == "PID" ) {
    int          pid;
    stringstream ss( s2 );
    while ( ss >> pid ) {
      if ( s1 == "ban" && idb )
        idb->insert( pid );
      else if ( s1 == "particle" && idp )
        idp->insert( pid );
      while ( ss.peek() == ',' || ss.peek() == ',' ) ss.ignore();
    }
  } else
    return;
  name  = s0;
  type  = s1;
  loc   = s2;
  index = *idx;
  valid = true;
}

// Print the input configuration to #msg.
void McParticleFlow::Input::print( MsgStream& msg, bool idx ) {
  if ( idx ) {
    msg << setw( 4 ) << index << setw( 15 ) << name << setw( 16 ) << type << setw( 40 ) << loc << "\n"
        << setw( 19 ) << "bans:";
    int len( 0 );
    for ( unordered_set<int>::iterator pid = pidsBan.begin(); pid != pidsBan.end(); ++pid ) {
      int n( log10( abs( *pid ) ) + 2 + ( *pid < 0 ? 1 : 0 ) );
      if ( len + n > 59 ) {
        msg << "\n" << setw( 19 ) << " ";
        len = n;
      } else {
        msg << " " << *pid;
        len += n;
      }
    }
    len = 0;
    msg << "\n" << setw( 19 ) << "prts:";
    for ( unordered_set<int>::iterator pid = pidsPrt.begin(); pid != pidsPrt.end(); ++pid ) {
      int n( log10( abs( *pid ) ) + 2 + ( *pid < 0 ? 1 : 0 ) );
      if ( len + n > 59 ) {
        msg << "\n" << setw( 19 ) << " ";
        len = n;
      } else {
        msg << " " << *pid;
        len += n;
      }
    }
  } else
    msg << setw( 4 ) << " " << setw( 15 ) << name << setw( 16 ) << type << setw( 40 ) << loc << "\n";
}

//=============================================================================
// Additional methods.
//=============================================================================
// Add an MCParticle and its daughters to the PF output MCParticles.
void McParticleFlow::add( const MCParticle* prt, const Input& in, int key ) {
  if ( !primary( prt ) ) return;
  if ( prt->mother() && prt->mother()->key() > key ) return;
  bool                                  stable( false ), banned( false ), used( false );
  vector<pair<const MCParticle*, bool>> dtrs;
  status( prt, in, dtrs, stable, banned, used );
  if ( used && !banned ) return;
  for ( vector<pair<const MCParticle*, bool>>::iterator dtr = dtrs.begin(); dtr != dtrs.end(); ++dtr ) {
    m_used.insert( dtr->first );
    if ( dtr->second ) m_save.insert( dtr->first );
  }
}

// Recursively determine the status of an MCParticle decay tree.
void McParticleFlow::status( const MCParticle* prt, const Input& in, vector<pair<const MCParticle*, bool>>& prts,
                             bool& stable, bool& banned, bool& used ) {
  int pid( prt->particleID().pid() );
  stable = in.prt || in.pidsPrt.find( pid ) != in.pidsPrt.end();
  banned = banned || in.ban || in.pidsBan.find( pid ) != in.pidsBan.end();
  used   = used || m_used.find( prt ) != m_used.end();

  // Add the daughters from the decay tree.
  const SmartRefVector<MCVertex> vrts = prt->endVertices();
  bool                           material( false ), decay( false );
  for ( SmartRefVector<MCVertex>::const_iterator ivrt = vrts.begin(); ivrt != vrts.end(); ++ivrt ) {
    const MCVertex* vrt = ivrt->target();
    if ( vrt->type() < 1 || vrt->type() > 4 )
      material = true;
    else if ( vrt->type() == 2 || vrt->type() == 3 ) {
      decay                                 = true;
      const SmartRefVector<MCParticle> dtrs = vrt->products();
      for ( SmartRefVector<MCParticle>::const_iterator iDtr = dtrs.begin(); iDtr != dtrs.end(); ++iDtr ) {
        bool dtrStable( false ), dtrBanned( stable || banned );
        status( iDtr->target(), in, prts, dtrStable, dtrBanned, used );
      }
    }
  }
  stable = stable || ( vrts.size() == 0 || ( material && !decay ) );
  prts.push_back( make_pair( prt, stable && !banned && !used ) );
}

// Check if a particle is from a collision.
bool McParticleFlow::primary( const MCParticle* prt ) {
  const MCVertex* vrt = prt->originVertex();
  if ( !vrt ) return false;
  if ( vrt->isPrimary() ) return true;
  if ( vrt->type() < 1 || vrt->type() > 4 ) return false;
  if ( !vrt->mother() ) return false;
  return primary( vrt->mother() );
}
