/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
//
// Helper class to classify jets using truth for jet tagging
//
//-----------------------------------------------------------------------------

#ifndef LOKIJETTAGTRUTHNEW_H
#define LOKIJETTAGTRUTHNEW_H 1

// ============================================================================
// Includes

// Gaudi
#include "GaudiAlg/GaudiTool.h"

// Kernel
#include "Kernel/IDVAlgorithm.h"
#include "Kernel/IJetMaker.h"
#include "Kernel/IJetTagTool.h"
#include "Kernel/IJets2Jets.h"

#include "Event/MCParticle.h"

// Relations
#include "Relations/Relation2D.h"

// Event
#include "Event/HepMCEvent.h"

class IParticle2MCAssociator;
// ============================================================================
// Declaration
namespace LoKi {
  class JetTagTruthNew : public virtual IJetTagTool, public GaudiTool {
  public:
    // standard initialise
    StatusCode initialize() override;
    // standard finalise
    StatusCode finalize() override;

    // Method that calculates the truth tag of a jet
    //   -1 - not found, 1 - light jet, 4 - charm jet, 5 - bottom jet
    bool calculateJetProperty( const LHCb::Particle* jet, std::map<std::string, double>& jetTruth ) override;

    // standard constructor
    JetTagTruthNew( const std::string& type, const std::string& name, const IInterface* parent );

  private:
    IParticle2MCAssociator* m_p2mcAssoc;
    std::string             m_p2mcAssocType;

    IDVAlgorithm* m_dva;

    std::vector<double>     TruthBWeight( const LHCb::Particle* jet );
    std::vector<double>     TruthCWeight( const LHCb::Particle* jet );
    const LHCb::MCParticle* Bancestor( const LHCb::MCParticle* mcpart );
    const LHCb::MCParticle* Cancestor( const LHCb::MCParticle* mcpart );
    bool                    MuonDecay( const LHCb::MCParticle* mcpart );

    // table linking visible particle jets and reconstructed jets
    std::string m_jetMatchTableHepMC2Jets;
    // table linking particles and genparticles
    std::string m_jetMatchTablePart2GenPart;
  };
} // namespace LoKi

#endif
