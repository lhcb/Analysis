/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MCJETBUILDER_H
#define MCJETBUILDER_H 1

// STL.
#include <unordered_map>
#include <unordered_set>

// Gaudi.
#include "GaudiAlg/GaudiHistoAlg.h"

// Event.
#include "Event/MCParticle.h"

// Tools.
#include "Kernel/DaVinciStringUtils.h"

// FastJet.
#include "fastjet/ClusterSequence.hh"
#include "fastjet/config.h"

using namespace std;
using namespace fastjet;
using namespace LHCb;

/**
 * Jet building (JB) algorithm class for use with MCParticles.
 *
 * JB takes MCParticle inputs and clusters them into jets using the
 * FastJet package.
 *
 * Jets can be built per primary vertex or inclusively using the
 * 'ChrVrt' and 'NeuVrt' flags. If both are false, jets are built
 * inclusively. If 'ChrVrt' is true, charged particles are associated
 * to PVs while if 'NeuVrt' is true, neutral particles are associated
 * to PVs. Particles with no PV are associated to all PVs. The jets
 * are built using the input for each PV and the PV location is set as
 * the reference point of the jet. Note that when building per PV, any
 * particles without PVs are double counted, when considering jets
 * from all PVs.
 *
 * @class  McJetBuilder
 * @file   McJetBuilder.h
 * @author Cedric Potterat and Philip Ilten
 * @date   2016-05-26
 */
class McJetBuilder : public GaudiHistoAlg {
public:
  /// Constructor.
  McJetBuilder( const string& name, ISvcLocator* svc );
  StatusCode initialize() override; ///< Initialize.
  StatusCode execute() override;    ///< Execute.
  StatusCode finalize() override;   ///< Finalize.

private:
  /// Definition of map relating MCVertices to MCParticles.
  typedef unordered_map<const MCVertex*, vector<const MCParticle*>> VrtsPrts;

  // Additional methods.
  /// Build jets from #prts and add to the TES.
  void build( const vector<const MCParticle*>* prts, const MCVertex* vrt );
  /// Associate an MCParticle with a primary vertex.
  const MCVertex* primary( const MCParticle* prt );

  // Input/output property members.
  /// Input locations of MCParticles to use.
  vector<string> m_inLocs;
  /// JB output MCParticle location.
  string m_outLoc;
  /// JB output prefix for non-MCParticle locations (determined from m_outLoc).
  string m_outPre;

  // Jet property members.
  /// Particle ID to assign the jets.
  int m_jetPid;
  /// Minimum jet pT to build with FastJet.
  double m_jetPtMin;
  /// Jet radius parameter to use with FastJet.
  double m_jetR;
  /// If true, associate charged particles to a primary vertex.
  bool m_chrVrt;
  /// If true, associate neutral particles to a primary vertex.
  bool m_neuVrt;
  /// Jet sorting method: 1 E, 2 pT, or 3 y.
  int m_jetSort;

  // FastJet property members.
  /// FastJet finder to use: 0 kt, 1 cambridge, 2 anti-kt, ...; see
  /// fasjet::JetAlgorithm for more options.
  int m_fjAlg;
  /// FastJet recombination scheme: 0 E, 1 pT, 2 pT^2, ...; see
  /// fastjet::RecombinationScheme for more options.
  int m_fjScheme;
  /// FastJet clustering strategy: 0 N3Dumb, 1 Best, 2 NlnN, ...; see
  /// fastjet::Strategy for more options.
  int m_fjStrategy;

  // Additional members (not properties).
  /// The FastJet jet definition.
  JetDefinition m_def;
  /// Pointer to the jet sorting function.
  vector<PseudoJet> ( *m_sort )( const vector<PseudoJet>& jets );
  /// Output MCParticle container.
  MCParticles* m_prts;
  /// Output MCVertex container.
  MCVertices* m_vrts;
};

#endif // MCJETBUILDER_H
