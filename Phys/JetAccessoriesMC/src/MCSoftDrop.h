/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#ifndef MCSOFTDROP_H
#define MCSOFTDROP_H 1

// ============================================================================
// Includes
// ----------------------------------------------------------------------------
// Gaudi
// ----------------------------------------------------------------------------
#include "GaudiAlg/GaudiTool.h"
// ----------------------------------------------------------------------------
// DaVinci
// ----------------------------------------------------------------------------
#include "Kernel/DaVinciAlgorithm.h"
#include "Kernel/DaVinciStringUtils.h"
#include "Kernel/IParticleCombiner.h"
// ----------------------------------------------------------------------------
// Event
// ----------------------------------------------------------------------------
#include "Event/MCParticle.h"
#include "LoKi/Geometry.h"
#include "LoKi/Kinematics.h"
// ----------------------------------------------------------------------------
// FastJet
// ----------------------------------------------------------------------------
#include "fastjet/ClusterSequence.hh"
#include "fastjet/contrib/RecursiveSymmetryCutBase.hh"
#include "fastjet/contrib/SoftDrop.hh"
// ============================================================================

/** Soft Drop class for use with MC particles.
 *
 *  @see 	arXiv:1402.2657
 *
 *  The SoftDrop algorithm takes a jet that has already been clustered,
 *  reclusters it with the Cambridge-Aachen algorithm, then recursively steps
 *  through the clustering tree, dropping constituents that are too soft or
 *  not colinear. This tool performs this algorithm on a container of input
 *  jets, and outputs all jets that pass SoftDrop to a single container. The
 *  daughters of an output jet are the two subjets, and the daughters of the
 *  subjets are the constituent particles. Please note that the output location
 *  should be specified by a folder, such as Output = /Phys/SD/MCParticles/ .
 *  The output jets would then be located in the TES at /Phys/SD/MCParticles .
 *
 *  @class  MCSoftDrop
 *  @file   MCSoftDrop.h
 *  @author Jake Pfaller
 *  @date   30/10/2024
 */
class MCSoftDrop : public DaVinciAlgorithm {

public:
  // --------------------------------------------------------------------------
  /** Constructor
   */
  MCSoftDrop( const std::string& name, ISvcLocator* svc );

  // --------------------------------------------------------------------------
  /** Destructor
   */
  ~MCSoftDrop() {}

  // --------------------------------------------------------------------------
  /** Initialize the algorithm
   *
   *  @return StatusCode
   */
  StatusCode initialize() override;

  // --------------------------------------------------------------------------
  /** Excecute the algorithm
   *
   *  Applies the SoftDrop algorithm to a container of jets. Please note that
   *  the jets should have already been clustered, and must have type
   *  LHCb::MCParticle
   *
   *  @return StatusCode
   */
  StatusCode execute() override;

  // --------------------------------------------------------------------------
  /** Finalize
   *
   *  @return StatusCode
   */
  StatusCode finalize() override;

protected:
  // Useful types -------------------------------------------------------------

  typedef fastjet::PseudoJet Jet;
  typedef LHCb::MCParticle   Prt;
  typedef LHCb::MCParticles  Prts;
  typedef LHCb::MCVertex     Vrt;
  typedef LHCb::MCVertices   Vrts;

  // --------------------------------------------------------------------------
  /** Convert an LHCb::MCParticle to a fastjet::PseudoJet
   *
   *  @param prt Input MCParticle
   *  @param index Index to be assigned to the output PseudoJet
   *  @return a fastjet::PseudoJet version of the input particle
   */
  fastjet::PseudoJet makeJet( const Prt* prt, const int& index );

  // Members ------------------------------------------------------------------

  // Beta parameter
  double m_beta;
  // Value of the cut on the symmetry measure
  double m_symmetryCut;
  // Choice of symmetry measure
  int m_symmetryMeasure;
  // Jet radius
  double m_r0;
  // Maximal allowed value of the mass drop variable
  double m_muCut;
  // Strategy to decide which subjet to recurse into
  int m_recursionChoice;
  // Jet definition for dummy reclustering
  fastjet::JetDefinition m_jd;

private:
  // Default constructor is disabled
  MCSoftDrop() = delete;

  // Copy constructor is disabled
  MCSoftDrop( MCSoftDrop& ) = delete;

  // Assignment operator is disabled
  MCSoftDrop& operator=( const MCSoftDrop& ) = delete;
};

#endif // MCSOFTDROP_H