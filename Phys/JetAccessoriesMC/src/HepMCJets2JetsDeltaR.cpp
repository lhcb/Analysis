/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
// from Gaudi
#include "Event/Particle.h"
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/IJets2Jets.h" // Interface
#include "LoKi/Geometry.h"
#include "LoKi/Kinematics.h"
#include "LoKi/MCMatchObj.h"

#include "Relations/Relation1D.h"
#include "Relations/Relation2D.h"

#include "LoKi/MCParticleCuts.h"
#include "LoKi/ParticleCuts.h"
#include "LoKi/PhysMCParticleCuts.h"

#include <math.h>
namespace LoKi {
  class HepMCJets2JetsDeltaR : public virtual IJets2Jets, public GaudiTool {
  public:
    // ========================================================================
    /// standard initialization of the tool
    void makeRelation( const IJets2Jets::Jets& StdPrimaryJets, const IJets2Jets::Jets& StdSecondaryJets,
                       IJets2Jets::Table& table ) const override;
    // ========================================================================
    /// standard constructor
    HepMCJets2JetsDeltaR( const std::string& type, ///< tool type ???
                          const std::string& name, ///< tool name
                          const IInterface*  parent )
        : GaudiTool( type, name, parent ) {
      declareInterface<IJets2Jets>( this );
    }
    // ========================================================================
  };
} // namespace LoKi
// end of namespace LoKi

void LoKi::HepMCJets2JetsDeltaR::makeRelation( const IJets2Jets::Jets& StdPrimaryJets,
                                               const IJets2Jets::Jets& StdSecondaryJets,
                                               IJets2Jets::Table&      table ) const {
  for ( IJets2Jets::Jets::const_iterator secjet = StdSecondaryJets.begin(); StdSecondaryJets.end() != secjet;
        ++secjet ) {
    LoKi::Types::Fun DR2 = LoKi::Cuts::DR2( ( *secjet )->momentum() );
    for ( IJets2Jets::Jets::const_iterator primjet = StdPrimaryJets.begin(); StdPrimaryJets.end() != primjet;
          ++primjet ) {
      table.relate( *primjet, *secjet, std::sqrt( DR2( *primjet ) ) )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }
  }
}

// ============================================================================
/// Declaration of the Tool Factory
DECLARE_COMPONENT( LoKi::HepMCJets2JetsDeltaR )
// ============================================================================
// The END
// ============================================================================
