/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Local.
#include "McJetBuilder.h"

//-----------------------------------------------------------------------------
// Implementation file for class : McJetBuilder
//
// 2016-05-26 : Cedric Potterat and Philip Ilten
//-----------------------------------------------------------------------------

// Declare the algorithm factory.
DECLARE_COMPONENT( McJetBuilder )

//=============================================================================
// Constructor.
//=============================================================================
McJetBuilder::McJetBuilder( const string& name, ISvcLocator* svc ) : GaudiHistoAlg( name, svc ) {
  // Declare the input/output properties.
  declareProperty( "Inputs", m_inLocs, "Input locations of MCParticles to use." );
  declareProperty( "Output", m_outLoc = "Phys/JB/MCParticles", "JB output MCParticle location." );

  // Declare the jet properties.
  declareProperty( "JetPid", m_jetPid = 98, "Particle ID to assign the jets." );
  declareProperty( "JetPtMin", m_jetPtMin = 5 * Gaudi::Units::GeV, "Minimum jet pT to build with FastJet." );
  declareProperty( "JetR", m_jetR = 0.5, "Jet radius parameter to use with FastJet." );
  declareProperty( "ChrVrt", m_chrVrt = false, "If true, associate charged particles to a primary vertex." );
  declareProperty( "NeuVrt", m_neuVrt = false, "If true, associate neutral particles to a primary vertex." );
  declareProperty( "JetSort", m_jetSort = 2, "Jet sorting method: 1 E, 2 pT, or 3 y." );

  // Declare the FastJet properties.
  declareProperty( "FjAlg", m_fjAlg = 2,
                   "FastJet finder to use: 0 kt, 1 cambridge, 2 anti-kt, ...; "
                   "see fasjet::JetAlgorithm for more options." );
  declareProperty( "FjScheme", m_fjScheme = 0,
                   "FastJet recombination scheme: 0 E, 1 pT, 2 pT^2, ...; see "
                   "fastjet::RecombinationScheme for more options." );
  declareProperty( "FjStrategy", m_fjStrategy = 1,
                   "FastJet clustering strategy: 0 N3Dumb, 1 Best, 2 NlnN, ...; "
                   "see fastjet::Strategy for more options." );
}

//=============================================================================
// Initialize.
//=============================================================================
StatusCode McJetBuilder::initialize() {
  m_def = JetDefinition( static_cast<JetAlgorithm>( m_fjAlg ), m_jetR, static_cast<RecombinationScheme>( m_fjScheme ),
                         static_cast<Strategy>( m_fjStrategy ) );
  if ( m_jetSort == 1 )
    m_sort = &sorted_by_E;
  else if ( m_jetSort == 2 )
    m_sort = &sorted_by_pt;
  else if ( m_jetSort == 3 )
    m_sort = &sorted_by_rapidity;
  else
    m_sort = &sorted_by_pt;
  // Determine the output location base.
  m_outPre = m_outLoc;
  DaVinci::StringUtils::removeEnding( m_outPre, "/" );
  return StatusCode::SUCCESS;
}

//=============================================================================
// Execute.
//=============================================================================
StatusCode McJetBuilder::execute() {
  // Create the output.
  m_prts = new MCParticles();
  m_vrts = new MCVertices();
  put( m_prts, m_outLoc );
  put( m_vrts, m_outPre + "/MCVertices" );

  // Assign the input Particles by PV (if requested).
  vector<const MCParticle*> cntrPrts;
  VrtsPrts                  vrtsPrts;
  for ( vector<string>::iterator inLoc = m_inLocs.begin(); inLoc != m_inLocs.end(); ++inLoc ) {
    MCParticles* prts = getIfExists<MCParticles>( *inLoc );
    if ( !prts ) {
      warning() << "Could not retrieve MCParticles from " << *inLoc << endmsg;
      continue;
    }
    for ( MCParticles::iterator prt = prts->begin(); prt != prts->end(); ++prt ) {
      const MCVertex* vrt = primary( *prt );
      int             q   = ( *prt )->particleID().threeCharge();
      if ( !vrt || ( !m_chrVrt && q != 0 ) || ( !m_neuVrt && q == 0 ) ) {
        cntrPrts.push_back( *prt );
        continue;
      }
      VrtsPrts::iterator vrtPrt = vrtsPrts.find( vrt );
      if ( vrtPrt == vrtsPrts.end() )
        vrtsPrts[vrt] = vector<const MCParticle*>( 1, *prt );
      else
        vrtPrt->second.push_back( *prt );
    }
  }

  // Build the jets.
  if ( m_chrVrt || m_neuVrt ) {
    for ( VrtsPrts::iterator vrtPrt = vrtsPrts.begin(); vrtPrt != vrtsPrts.end(); ++vrtPrt ) {
      vrtPrt->second.insert( vrtPrt->second.end(), cntrPrts.begin(), cntrPrts.end() );
      build( &vrtPrt->second, vrtPrt->first );
    }
  } else
    build( &cntrPrts, 0 );
  setFilterPassed( true );
  return StatusCode::SUCCESS;
}

//=============================================================================
// Finalize.
//=============================================================================
StatusCode McJetBuilder::finalize() { return StatusCode::SUCCESS; }

//=============================================================================
// Additional methods.
//=============================================================================
// Build jets from #prts and add to the TES.
void McJetBuilder::build( const vector<const MCParticle*>* prts, const MCVertex* vrt ) {
  // Run FastJet.
  vector<PseudoJet> fjps;
  for ( unsigned int iPrt = 0; iPrt < prts->size(); ++iPrt ) {
    const Gaudi::LorentzVector& p = prts->at( iPrt )->momentum();
    fjps.push_back( PseudoJet( p.Px(), p.Py(), p.Pz(), p.E() ) );
    fjps.back().set_user_index( iPrt );
  }
  ClusterSequence   clst( fjps, m_def );
  vector<PseudoJet> jets = ( *m_sort )( clst.inclusive_jets( m_jetPtMin ) );

  // Create the MCParticle jets and insert into output.
  for ( vector<PseudoJet>::iterator jet = jets.begin(); jet != jets.end(); ++jet ) {
    MCParticle* prt = new MCParticle();
    MCVertex*   end = new MCVertex();
    prt->setMomentum( Gaudi::LorentzVector( jet->px(), jet->py(), jet->pz(), jet->e() ) );
    prt->setParticleID( ParticleID( m_jetPid ) );
    prt->addToEndVertices( end );
    if ( vrt ) prt->setOriginVertex( vrt );
    if ( vrt )
      end->setPosition( vrt->position() );
    else
      end->setPosition( Gaudi::XYZPoint( 0, 0, 0 ) );
    end->setType( MCVertex::Unknown );
    vector<PseudoJet> dtrs = jet->constituents();
    for ( vector<PseudoJet>::iterator dtr = dtrs.begin(); dtr != dtrs.end(); ++dtr )
      end->addToProducts( prts->at( dtr->user_index() ) );
    m_prts->insert( prt );
    m_vrts->insert( end );
  }
}

// Return the primary MCVertex for an MCParticle.
const MCVertex* McJetBuilder::primary( const MCParticle* prt ) {
  const MCVertex* vrt = prt->originVertex();
  if ( !vrt ) return 0;
  if ( vrt->isPrimary() ) return vrt;
  if ( vrt->type() < 1 || vrt->type() > 4 ) return 0;
  if ( !vrt->mother() ) return 0;
  return primary( vrt->mother() );
}
