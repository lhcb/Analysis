/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "MCSoftDrop.h"

// ----------------------------------------------------------------------------
/** Constructor */
MCSoftDrop::MCSoftDrop( const std::string& name, ISvcLocator* svc ) : DaVinciAlgorithm( name, svc ) {

  declareProperty( "Beta", m_beta = 0, "SoftDrop beta parameter" );
  declareProperty( "SymmetryCut", m_symmetryCut = 0.1, "SoftDrop symmetry cut parameter" );
  declareProperty( "SymmetryMeasure", m_symmetryMeasure = 0,
                   "See fastjet::contrib::RecursiveSymmetryCutBase::SymmetryMeasure" );
  declareProperty( "JetR", m_r0 = 0.5, "Jet radius for reclustering" );
  declareProperty( "RecursionChoice", m_recursionChoice = 0,
                   "See fastjet::contrib::RecursiveSymmetryCutBase::recursion_choice" );
}

// ----------------------------------------------------------------------------
/** Initialize the algorithm */
StatusCode MCSoftDrop::initialize() {

  // Initialize parent algorithm
  DaVinciAlgorithm::initialize().ignore();

  // Set the reclustering definition
  m_jd = fastjet::JetDefinition( fastjet::cambridge_aachen_algorithm, 1000. );

  return StatusCode::SUCCESS;
}

// ----------------------------------------------------------------------------
/** Excecute the algorithm */
StatusCode MCSoftDrop::execute() {

  // Grab the input jets
  Prt::ConstVector inputs;
  for ( std::string inLoc : inputLocations() ) {
    LHCb::MCParticles* prts = getIfExists<LHCb::MCParticles>( inLoc );
    if ( !prts ) {
      warning() << "Could not retrieve MCParticles from " << inLoc << endmsg;
      continue;
    }
    for ( Prts::iterator prt = prts->begin(); prt < prts->end(); prt++ ) { inputs.push_back( *prt ); }
  }

  // Container for the subjets and vertices
  LHCb::MCParticles* jets   = new LHCb::MCParticles();
  LHCb::MCParticles* sjs    = new LHCb::MCParticles();
  LHCb::MCVertices*  sjVrts = new LHCb::MCVertices();
  LHCb::MCVertices*  jVrts  = new LHCb::MCVertices();
  std::string        outPre = outputLocation();
  DaVinci::StringUtils::removeEnding( outPre, "/" );
  put( jets, outputLocation() );
  put( sjs, outPre + "/MCSubjets" );
  put( sjVrts, outPre + "/MCSubjets/MCVertices" );
  put( jVrts, outPre + "/MCVertices" );

  // Loop over the input jets
  for ( unsigned int ip = 0; ip < inputs.size(); ip++ ) {

    // Recluster constituents because an in-scope cluster sequence is needed
    const SmartRefVector<Prt>& dtrs = inputs[ip]->endVertices()[0]->products();
    std::vector<Jet>           fjInputs;
    for ( unsigned int i = 0; i < dtrs.size(); i++ ) { fjInputs.push_back( makeJet( dtrs[i], i ) ); }
    fastjet::ClusterSequence cs( fjInputs, m_jd );
    Jet                      inJet = cs.inclusive_jets()[0];

    // Perform the Soft Drop algorithm
    fastjet::contrib::SoftDrop softDrop(
        m_beta, m_symmetryCut,
        static_cast<fastjet::contrib::RecursiveSymmetryCutBase::SymmetryMeasure>( m_symmetryMeasure ), m_r0,
        std::numeric_limits<double>::infinity(),
        static_cast<fastjet::contrib::RecursiveSymmetryCutBase::RecursionChoice>( m_recursionChoice ) );
    Jet outJet = softDrop( inJet );
    if ( !outJet.has_constituents() || !outJet.has_pieces() ) continue;

    // Output jet and subjets
    Prt *pJet = new Prt(), *pSubjet0 = new Prt(), *pSubjet1 = new Prt();
    Vrt *vJet = new Vrt(), *vSubjet0 = new Vrt(), *vSubjet1 = new Vrt();

    // Loop over the subjets
    for ( int iSubjet = 0; iSubjet < 2; iSubjet++ ) {

      // Grab the appropriate subjet
      Jet  subjet  = outJet.pieces()[iSubjet];
      Prt* pSubjet = ( iSubjet ) ? pSubjet1 : pSubjet0;
      Vrt* vSubjet = ( iSubjet ) ? vSubjet1 : vSubjet0;

      // Loop over the constituents
      for ( Jet& j : subjet.constituents() ) {

        // Grab the particle
        vSubjet->addToProducts( dtrs[j.user_index()] );
      }

      // Create the output subjet
      pSubjet->addToEndVertices( vSubjet );
      pSubjet->setMomentum( Gaudi::LorentzVector( subjet.px(), subjet.py(), subjet.pz(), subjet.e() ) );
      sjVrts->insert( vSubjet );
      sjs->insert( pSubjet );
      vJet->addToProducts( pSubjet );
    }

    // Create the output jet
    pJet->addToEndVertices( vJet );
    pJet->setMomentum( Gaudi::LorentzVector( outJet.px(), outJet.py(), outJet.pz(), outJet.e() ) );
    // jVrts->insert(vJet);
    jets->insert( pJet );
  }

  setFilterPassed( true );
  return StatusCode::SUCCESS;
}

// ----------------------------------------------------------------------------
/** Finalize
 *
 *  @return StatusCode
 */
StatusCode MCSoftDrop::finalize() { return StatusCode::SUCCESS; }

// ----------------------------------------------------------------------------
/** Convert an LHCb::MCParticle to a fastjet::PseudoJet */
fastjet::PseudoJet MCSoftDrop::makeJet( const Prt* prt, const int& index ) {

  if ( prt == 0 ) { return Jet(); }
  const Gaudi::LorentzVector& v = prt->momentum();
  Jet                         jet( v.Px(), v.Py(), v.Pz(), v.E() );
  jet.set_user_index( index );
  return jet;
}

// ============================================================================
// Declare the algorithm factory
DECLARE_COMPONENT( MCSoftDrop )
// ============================================================================