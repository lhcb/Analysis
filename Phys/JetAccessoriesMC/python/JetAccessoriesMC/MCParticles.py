###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# =============================================================================
__author__ = " Albert Bursche "
__date__ = " 2011-07-22 "
__version__ = "0.1"
# =============================================================================

# =============================================================================
"""
Select MC particles having a chance to pass the offline criteria. Needs updating if these citeria change.
"""

from Configurables import FilterDesktop
MCPartFilter = FilterDesktop("FilteredStableParticles")  #,OutputLevel = DEBUG)
MCPartFilter.InputLocations = ["Phys/StdHepMCParticles/Particles"]
MCPartFilter.Code = "(PT > 150*MeV) & (ETA >0)"  # corresponds to an 200 MeV cut on reco tracks
# eta cut for speed optimisation
