/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef NEUTRALPP2MC_H
#define NEUTRALPP2MC_H 1

// Include files
// from Gaudi
#include "AsctAlgorithm.h"
#include "GaudiAlg/GaudiAlgorithm.h"

/** @class NeutralPP2MC NeutralPP2MC.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2008-09-25
 */
class NeutralPP2MC : public AsctAlgorithm {
public:
  /** standard execution of the algorithm
   *  @return StatusCode
   */
  StatusCode execute() override;
  /** Standard constructor
   *  @param name name of the algorithm
   *  @param svc  service locator
   */
  NeutralPP2MC( const std::string& name, ISvcLocator* svc );
  StatusCode initialize() override;

private:
  std::string m_mcTable;
  std::string m_test;
};
#endif // NEUTRALPP2MC_H
