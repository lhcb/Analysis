/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
#ifndef DUMPPARTICLE_H
#define DUMPPARTICLE_H 1

// Include files
// from DaVinci, this is a specialized GaudiAlgorithm
#include "Kernel/DaVinciAlgorithm.h"

/** @class DumpParticle DumpParticle.h
 *
 * Print a Particle
 *
 *  @author Patrick Koppenburg
 *  @date   2012-03-14
 */
class DumpParticle : public DaVinciAlgorithm {

public:
  /// Standard constructor
  DumpParticle( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~DumpParticle(); ///< Destructor

  StatusCode execute() override; ///< Algorithm execution

private:
  StatusCode dump( const LHCb::Particle* );
};
#endif // DUMPPARTICLE_H
