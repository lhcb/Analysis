/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: IsBEvent.h,v 1.4 2010-06-01 09:38:21 pkoppenb Exp $
#ifndef ISBEVENT_H
#define ISBEVENT_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
// Event
#include "Event/MCParticle.h"

/** @class IsBEvent IsBEvent.h
 *
 *  Test if required particles are there. One can also ask for a b-quark (default)
 *  or a c-quark (RequiredParticles = {"c"} ;)
 *
 *  @author Patrick KOPPENBURG
 *  @date   2005-01-12
 */
class IsBEvent : public GaudiAlgorithm {
public:
  /// Standard constructor
  IsBEvent( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~IsBEvent(); ///< Destructor

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

protected:
private:
  bool goodEvent( LHCb::MCParticles* ); ///< loop

private:
  std::vector<std::string> m_required; ///< List of required particles
  bool                     m_andMode;  ///< Require all particles (default: no)

  std::vector<int> m_particles; ///< required particles
};
#endif // ISBEVENT_H
