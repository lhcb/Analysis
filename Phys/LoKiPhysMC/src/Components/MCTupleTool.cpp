/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// GaudiAlg
// ============================================================================
#include "GaudiAlg/GaudiTool.h"
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"
// ============================================================================
// DaVinciMCKernel
// ============================================================================
#include "Kernel/IMCParticleTupleTool.h"
// ============================================================================
// LoKiCore
// ============================================================================
#include "LoKi/ILoKiSvc.h"
// ============================================================================
// LoKiMC
// ============================================================================
#include "LoKi/IMCHybridFactory.h"
// ============================================================================
namespace LoKi {
  // ==========================================================================
  namespace Hybrid {
    // ========================================================================
    /** @class MCTupleTool
     *  Simple implementation of IMCParticleTupleTool interface
     *  @see IMCParticleTupleTool
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date   2010-02-18
     */
    class MCTupleTool : public GaudiTool, public virtual IMCParticleTupleTool {
    public:
      // ======================================================================
      /** Fill the tuple.
       *  @see IMCParticleTupleTool
       *  @param top  : may be used to provide additional information about
       *                "part", like the top particle of the decay.
       *  @param part : the particle about which some info are filled.
       *  @param head : prefix for the tuple column name.
       *  @param tuple: the tuple to fill
       *  @return status code
       */
      StatusCode fill( const LHCb::MCParticle* top, const LHCb::MCParticle* part, const std::string& head,
                       Tuples::Tuple& tuple ) override;
      // ======================================================================
    public:
      // ======================================================================
      /// initializaton of the tool
      StatusCode initialize() override; // initializaton of the tool
      /// finalization  of the tool
      StatusCode finalize() override; // finalization  of the tool
      /** Standard constructor
       *  @param type the tool type (?)
       *  @param name the tool instance name
       *  @param parent the parent
       */
      MCTupleTool( const std::string& type, const std::string& name, const IInterface* parent );
      // ======================================================================
    public:
      // ======================================================================
      /// the update handler
      void propHandler( Gaudi::Details::PropertyBase& /* p */ );
      // ======================================================================
    protected:
      // ======================================================================
      /** helper class to keep the N-tuple items
       *  it is needed due to absence f the default constructor for
       *  the class LoKi::MCTypes::MCFun
       *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
       *  @date 2010-02-18
       */
      class Item {
      public:
        // ====================================================================
        // the default constructor
        Item() : m_name(), m_fun( LoKi::BasicFunctors<const LHCb::MCParticle*>::Constant( -1.e+10 ) ) {}
        // ====================================================================
      public:
        /// the variable name
        std::string m_name; // the variable name
        /// the functor
        LoKi::MCTypes::MCFun m_fun; /// the functor
      };
      // ======================================================================
      /// the actual type for list of items
      typedef std::vector<Item> Items;
      /// map { "name" : "functor" }
      typedef std::map<std::string, std::string> Map;
      // ======================================================================
    protected:
      // ======================================================================
      /// build the preambulo-string from preambulo-list
      std::string preambulo() const {
        const std::vector<std::string>& lines = m_preambulo;
        //
        std::string result;
        for ( std::vector<std::string>::const_iterator iline = lines.begin(); lines.end() != iline; ++iline ) {
          if ( lines.begin() != iline ) { result += "\n"; }
          result += ( *iline );
        }
        return result;
      }
      // ======================================================================
    protected:
      // ======================================================================
      /// initialization of the tool
      virtual StatusCode initVariables();
      virtual StatusCode initVariablesHelper( const Map& map, Items& items );
      // ======================================================================
    private:
      // ======================================================================
      /// the actual type/name of the factory
      std::string m_factory; // the factory name
      /// "Preambulo" list
      std::vector<std::string> m_preambulo; // "Preambulo" list
      /// map { "name" : "functor" }
      Map m_map, m_bool_map, m_int_map, m_float_map; // map { "name" : "functor" }
      // ======================================================================
    private:
      // ======================================================================
      /// the actual functors for n-tuple
      Items m_items, m_bool_items, m_int_items, m_float_items; // the actual functors for n-tuple
      // ======================================================================
    };
    // ========================================================================
  } // namespace Hybrid
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
/*  Standard constructor
 *  @param type the tool type (?)
 *  @param name the tool instance name
 *  @param parent the parent
 */
// ============================================================================
LoKi::Hybrid::MCTupleTool::MCTupleTool( const std::string& type, const std::string& name, const IInterface* parent )
    : GaudiTool( type, name, parent )
    , m_factory( "LoKi::Hybrid::MCTool/MCHybridFactory:PUBLIC" )
    , m_preambulo()
    , m_map()
    , m_bool_map()
    , m_int_map()
    , m_float_map()
    , m_items()
    , m_bool_items()
    , m_int_items()
    , m_float_items() {
  //
  // interface!
  //
  declareInterface<IMCParticleTupleTool>( this );
  //
  // properties
  //
  // factory
  declareProperty( "Factory", m_factory, "Type/Name for C++/Python Hybrid Factory" )
      ->declareUpdateHandler( &LoKi::Hybrid::MCTupleTool::propHandler, this );
  // varibales
  declareProperty( "Variables", m_map, "The {'name':'functor'}-map of columns for N-tuple " )
      ->declareUpdateHandler( &LoKi::Hybrid::MCTupleTool::propHandler, this );

  declareProperty( "BoolVariables", m_bool_map, "The {'name':'functor'}-map of columns for N-tuple " )
      ->declareUpdateHandler( &LoKi::Hybrid::MCTupleTool::propHandler, this );

  declareProperty( "IntVariables", m_int_map, "The {'name':'functor'}-map of columns for N-tuple " )
      ->declareUpdateHandler( &LoKi::Hybrid::MCTupleTool::propHandler, this );

  declareProperty( "FloatVariables", m_float_map, "The {'name':'functor'}-map of columns for N-tuple " )
      ->declareUpdateHandler( &LoKi::Hybrid::MCTupleTool::propHandler, this );

  // the preambulo
  declareProperty( "Preambulo", m_preambulo, "The preambulo to be used for Bender/Python script" )
      ->declareUpdateHandler( &LoKi::Hybrid::MCTupleTool::propHandler, this );
  //
}
// ============================================================================
/*  Fill the tuple.
 *  @see IMCParticleTupleTool
 *  @param top  : may be used to provide additional information about
 *                "part", like the top particle of the decay.
 *  @param part : the particle about which some info are filled.
 *  @param head : prefix for the tuple column name.
 *  @param tuple: the tuple to fill
 *  @return status code
 */
// ============================================================================
StatusCode LoKi::Hybrid::MCTupleTool::fill( const LHCb::MCParticle* top, const LHCb::MCParticle* particle,
                                            const std::string& head, Tuples::Tuple& tuple ) {
  if ( 0 == top ) { Warning( "LHCb::MCParticle* 'Top' points to NULL" ).ignore(); }
  if ( 0 == particle ) { Warning( "LHCb::MCParticle*       points to NULL" ).ignore(); }
  //
  if ( !tuple.valid() ) { return Error( "Invalid tuple " ); }
  //
  std::string head_ = head + "_";
  for ( Items::const_iterator item = m_items.begin(); m_items.end() != item; ++item ) {
    // fill N-tuple
    tuple->column( head_ + item->m_name, item->m_fun( particle ) )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }
  for ( Items::const_iterator item = m_bool_items.begin(); m_bool_items.end() != item; ++item ) {
    tuple->column( head_ + item->m_name, bool( item->m_fun( particle ) ) ).ignore();
  }
  for ( Items::const_iterator item = m_float_items.begin(); m_float_items.end() != item; ++item ) {
    tuple->column( head_ + item->m_name, float( item->m_fun( particle ) ) ).ignore();
  }
  for ( Items::const_iterator item = m_int_items.begin(); m_int_items.end() != item; ++item ) {
    tuple->column( head_ + item->m_name, int( item->m_fun( particle ) ) ).ignore();
  }

  //
  return StatusCode::SUCCESS;
}
// ============================================================================
// standard initialization of the tool
// ============================================================================
StatusCode LoKi::Hybrid::MCTupleTool::initialize() {
  StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) { return sc; } // RETURN
  svc<IService>( "LoKiSvc", true );
  //
  return initVariables();
}
// ============================================================================
// finalization of the tool
// ============================================================================
StatusCode LoKi::Hybrid::MCTupleTool::finalize() {
  // reset all functors:
  m_items.clear();
  m_bool_items.clear();
  m_float_items.clear();
  m_int_items.clear();
  // finalize the base
  return GaudiTool::finalize();
}
// ============================================================================
// helper for initialisation of m_<type>_items from m_<type>_map
// ============================================================================
StatusCode LoKi::Hybrid::MCTupleTool::initVariablesHelper( const Map& map, Items& items ) {
  // get the factory
  IMCHybridFactory* factory = tool<IMCHybridFactory>( m_factory, this );
  //
  items.clear();
  items.reserve( map.size() );
  for ( Map::const_iterator ivar = map.begin(); map.end() != ivar; ++ivar ) {
    Item       item;
    StatusCode sc = factory->get( ivar->second, item.m_fun, preambulo() );
    if ( sc.isFailure() ) { return Error( "Unable to decode " + ivar->first + " : " + ivar->second, sc ); }
    //
    item.m_name = ivar->first;
    items.push_back( item );
    //
    debug() << "The decoded variable name is '" << items.back().m_name << "'\t, the functor : '" << items.back().m_fun
            << "'" << endmsg;
  }
  release( factory ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ ); // we do not need the factory anymore
  //
  return StatusCode::SUCCESS;
}
// ============================================================================
// initialization of variables
// ============================================================================
StatusCode LoKi::Hybrid::MCTupleTool::initVariables() {

  auto sc = initVariablesHelper( m_map, m_items );
  if ( !sc.isSuccess() ) { return sc; };
  sc = initVariablesHelper( m_bool_map, m_bool_items );
  if ( !sc.isSuccess() ) { return sc; };
  sc = initVariablesHelper( m_float_map, m_float_items );
  if ( !sc.isSuccess() ) { return sc; };
  sc = initVariablesHelper( m_int_map, m_int_items );
  if ( !sc.isSuccess() ) { return sc; };

  if ( m_items.empty() && m_bool_items.empty() && m_float_items.empty() && m_int_items.empty() ) {
    Warning( "No variables/items are defined" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }
  //
  return StatusCode::SUCCESS;
}
// ============================================================================
// the update handler
// ============================================================================
void LoKi::Hybrid::MCTupleTool::propHandler( Gaudi::Details::PropertyBase& /* p */ ) {
  //
  if ( Gaudi::StateMachine::INITIALIZED > FSMState() ) { return; }
  //
  Warning( "Reintialization of Variables/Factory&Preambulo" ).ignore();
  //
  StatusCode sc = initVariables();
  Assert( sc.isSuccess(), "Unable to set 'Variables'", sc );
}
// ============================================================================
/// Declaration of the Tool Factory
DECLARE_COMPONENT( LoKi::Hybrid::MCTupleTool )
// ============================================================================
// The END
// ============================================================================
