/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ===========================================================================
// Relations
// ===========================================================================
#include "Relations/RelationWeighted2D.h"
// ===========================================================================
// DaVinciMCKernel
// ===========================================================================
#include "Kernel/Particle2MC.h"
// ===========================================================================
// LoKi
// ===========================================================================
#include "P2MCBase.h"
// ===========================================================================
/** @file
 *
 *  Simple algorithm for conversion of Particle->MC links from
 *  "linker" form into usable form of relation table with weight
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-08-17
 */
// ============================================================================
namespace LoKi {
  /** @class P2MCW
   *
   *  Simple algorithm for conversion of Particle->MC links from
   *  "linker" form into usable form of relation table with weight
   *
   *  Many thanks to Edwin Bos for kind help!
   *
   *  @todo replace 'include Particle2MCLinker' with
   *   'include Particle2MCmethod' from new package DaVinciMCTools
   *
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date 2006-08-17
   */
  class P2MCW : public LoKi::P2MCBase {
  public:
    // ========================================================================
    /// execution of the algorithm
    StatusCode execute() override {
      // avoid long names
      typedef LHCb::RelationWeighted2D<LHCb::Particle, LHCb::MCParticle, double> Table;
      // check the inheritance scheme
      BOOST_STATIC_ASSERT( INHERITS( Table, LHCb::P2MC2DW ) );
      // create the new relation table and register it in TES
      Table* table = new Table( 100 );
      put( table, m_output );
      // copy all links from the linker to the table
      return fillTable( table );
    }
    /// initialize the algorithm
    StatusCode initialize() override {
      // inittialize the base
      StatusCode sc = LoKi::P2MCBase::initialize();
      if ( sc.isFailure() ) { return sc; }
      // check the method
      switch ( m_method ) {
      case Particle2MCMethod::WithChi2:
        break;
      case Particle2MCMethod::Chi2:
      case Particle2MCMethod::Links:
      case Particle2MCMethod::Composite:
        Warning( "Unweighted method is selected!", sc ).ignore();
        break;
      default:
        return Error( "Invalid method is choosen!" );
      }
      //
      return StatusCode::SUCCESS; // RETURN
    }
    /// standard constructor
    P2MCW( const std::string& name, ISvcLocator* pSvc ) : LoKi::P2MCBase( name, pSvc ) {
      m_method = Particle2MCMethod::WithChi2;
    }
    // ========================================================================
  };
  // ==========================================================================
} // end of namespace LoKi
// ============================================================================
/// Declaration of the Algorithm Factory
// ============================================================================
DECLARE_COMPONENT( LoKi::P2MCW )
// ============================================================================
/// The END
// ============================================================================
