/*****************************************************************************\
* (c) Copyright 2000-2025 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
// from Gaudi
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"
// local
#include "TupleToolApplypMuIsolation.h"

#include <Kernel/GetIDVAlgorithm.h>
#include <Kernel/IDVAlgorithm.h>
#include <Kernel/IDistanceCalculator.h>
#include <Kernel/IVertexFit.h>

#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"

#include "Event/Particle.h"
#include "Kernel/IPVReFitter.h"
#include "LoKi/ParticleCuts.h"
#include "TrackInterfaces/ITrackVertexer.h"
#include <functional>

#include <map>
#include <string>

#include "TROOT.h"
#include "TString.h"

#include "TMVA/Config.h"
#include "TMVA/Reader.h"
#include "TMVA/Tools.h"

//-----------------------------------------------------------------------------
// Implementation file for class : TupleToolApplypMuIsolation
//
// @author Mitesh Patel, Patrick Koppenburg, Will Sutcliffe
// @date   2024-02-06
//-----------------------------------------------------------------------------
using namespace LHCb;
// Declaration of the Tool Factory
// actually acts as a using namespace TupleTool
DECLARE_COMPONENT( TupleToolApplypMuIsolation )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TupleToolApplypMuIsolation::TupleToolApplypMuIsolation( const std::string& type, const std::string& name,
                                                        const IInterface* parent )
    : TupleToolBase( type, name, parent ), m_dva( 0 ), m_dist( 0 ), m_pVertexFit( 0 ) {
  declareInterface<IParticleTupleTool>( this );

  m_inputParticles.push_back( "/Event/Phys/StdAllNoPIDsPions" );
  m_inputParticles.push_back( "/Event/Phys/StdNoPIDsUpPions" );
  m_inputParticles.push_back( "Phys/StdNoPIDsVeloPions" );

  // havent removed / added any of this yet
  declareProperty( "InputParticles", m_inputParticles );
  ;
  declareProperty( "OutputSuffix", m_outputSuffix = "" );
  declareProperty( "WeightsFile",
                   m_weightsName = System::getEnv( "TMVAWEIGHTSROOT" ) + "/data/pmu_isolation_weights.xml" );
}

//=============================================================================

StatusCode TupleToolApplypMuIsolation::initialize() {
  if ( !TupleToolBase::initialize() ) return StatusCode::FAILURE;

  m_dva = Gaudi::Utils::getIDVAlgorithm( contextSvc() );
  if ( 0 == m_dva ) return Error( "Couldn't get parent DVAlgorithm", StatusCode::FAILURE ).ignore();
  m_dist = tool<IDistanceCalculator>( "LoKi::DistanceCalculator", this );
  if ( !m_dist ) {
    Error( "Unable to retrieve the IDistanceCalculator tool" ).ignore();
    return StatusCode::FAILURE;
  }
  m_pvReFitter = tool<IPVReFitter>( "AdaptivePVReFitter", this );
  m_pVertexFit = m_dva->vertexFitter();
  // m_pVertexFit= tool<ITrackVertexer>

  if ( !m_pVertexFit ) {
    Error( "Unable to retrieve the IVertexFit tool" ).ignore();
    return StatusCode::FAILURE;
  }

  m_Reader = new TMVA::Reader( "!Silent" );

  m_Reader->AddVariable( "pt", &m_pt );
  m_Reader->AddVariable( "ipchi2", &m_chi2 );
  m_Reader->AddVariable( "minipchi2", &m_minipchi2 );
  m_Reader->AddVariable( "opening", &m_opening );
  m_Reader->AddVariable( "trackchi2", &m_trackchi2 );
  m_Reader->AddVariable( "ghost", &m_ghostprob );
  m_Reader->AddSpectator( "type", &m_type );
  m_Reader->BookMVA( "BDT method", m_weightsName );

  if ( !m_Reader ) {
    Error( "Unable to retrieve the IVertexFit tool" ).ignore();
    return StatusCode::FAILURE;
  }
  return StatusCode::SUCCESS;
}

//=============================================================================

StatusCode TupleToolApplypMuIsolation::fill( const Particle* mother, const Particle* P, const std::string& head,
                                             Tuples::Tuple& tuple ) {

  const std::string prefix = fullName( head );
  Assert( P && mother && m_dist, "This should not happen, you are inside TupleToolApplypMuIsolation.cpp :(" );

  bool test = true;

  m_daughtertracks.clear();
  m_daughtertracks.push_back( mother );

  LHCb::Particle::ConstVector finalStates;
  LHCb::Particle::ConstVector parts2Vertex;
  double                      maxbdt = -2;
  double                      bdt2( -2. ), bdt3( -2. ), bdt4( -2. );
  const LHCb::Particle*       tr1( NULL );
  const LHCb::Particle*       tr2( NULL );
  const LHCb::Particle*       tr3( NULL );
  const LHCb::Particle*       tr4( NULL );
  bool                        ismu1 = false, ismu2 = false, ismu3 = false, ismu4 = false;
  bool                        b1 = false, b2 = false, b3 = false, b4 = false;
  double                      p1x( 0 ), p1y( 0 ), p1z( 0 ), p2x( 0 ), p2y( 0 ), p2z( 0 );
  double                      p3x( 0 ), p3y( 0 ), p3z( 0 ), p4x( 0 ), p4y( 0 ), p4z( 0 );
  double                      pid1pi( -1000.0 ), pid1K( -1000.0 ), pid1p( -1000.0 ), pid1mu( -1000.0 );
  double                      pid2pi( -1000.0 ), pid2K( -1000.0 ), pid2p( -1000.0 ), pid2mu( -1000.0 );
  double                      pid3pi( -1000.0 ), pid3K( -1000.0 ), pid3p( -1000.0 ), pid3mu( -1000.0 );
  double                      pid4pi( -1000.0 ), pid4K( -1000.0 ), pid4p( -1000.0 ), pid4mu( -1000.0 );
  int                         chtr1( 0 ), chtr2( 0 ), chtr3( 0 ), chtr4( 0 );
  parts2Vertex.clear();

  saveDecayParticles( mother );

  for ( LHCb::Particle::ConstVector::const_iterator track = m_daughtertracks.begin(); track != m_daughtertracks.end();
        track++ ) {
    if ( ( *track )->proto() ) finalStates.push_back( ( *track ) );
  }

  if ( msgLevel( MSG::DEBUG ) ) debug() << "Final states size= " << finalStates.size() << endmsg;

  LHCb::Vertex v;

  parts2Vertex = finalStates;
  m_pVertexFit->fit( v, parts2Vertex ).ignore();

  for ( std::vector<std::string>::iterator i = m_inputParticles.begin(); i != m_inputParticles.end(); ++i ) {

    if ( !exist<LHCb::Particle::Range>( *i + "/Particles" ) ) {
      if ( msgLevel( MSG::DEBUG ) ) debug() << "No particles at " << *i << " !!!!!" << endmsg;
      continue;
    }

    LHCb::Particle::Range parts = get<LHCb::Particle::Range>( *i + "/Particles" );
    if ( msgLevel( MSG::DEBUG ) )
      debug() << "Getting particles from " << *i << " with " << ( parts ).size() << " particles" << endmsg;
    for ( LHCb::Particle::Range::const_iterator iparts = ( parts ).begin(); iparts != ( parts ).end(); ++iparts ) {
      const LHCb::Particle* part = ( *iparts );

      if ( part->proto()->track()->type() < 5 && !isTrackInDecay( part->proto()->track() ) ) {
        LHCb::Vertex vtxWithExtraTrack;
        parts2Vertex.push_back( *iparts );
        m_pVertexFit->fit( vtxWithExtraTrack, parts2Vertex ).ignore();
        parts2Vertex.pop_back();
        m_opening   = getopening( part->proto()->track(), P );
        m_minipchi2 = getminipchi2( part );
        m_ghostprob = part->proto()->track()->ghostProbability();
        m_trackchi2 = part->proto()->track()->chi2PerDoF();
        m_type      = part->proto()->track()->type();

        if ( part->proto()->track()->type() == 1 )
          m_pt = part->proto()->track()->momentum().z();
        else
          m_pt = part->proto()->track()->pt();

        if ( m_ghostprob > 0.5 ) continue;

        if ( part->proto()->track()->type() == 3 && !( m_opening > 0.994 ) ) { continue; }
        if ( part->proto()->track()->type() == 4 && !( m_opening > 0.98 ) ) { continue; }
        if ( part->proto()->track()->type() == 1 && !( m_opening > 0.98 ) ) { continue; }
        double tmpip, tmpchi2;
        m_dist->distance( (const LHCb::Particle*)part, (const LHCb::Vertex*)&v, tmpip, tmpchi2 ).ignore();
        m_chi2 = tmpchi2;

        if ( m_chi2 < 50 ) {
          float bdtval = m_Reader->EvaluateMVA( "BDT method" );
          if ( bdtval > maxbdt ) {
            bdt4   = bdt3;
            bdt3   = bdt2;
            bdt2   = maxbdt;
            tr4    = tr3;
            tr3    = tr2;
            tr2    = tr1;
            maxbdt = bdtval;
            tr1    = part;
          } else if ( bdtval > bdt2 ) {
            bdt4 = bdt3;
            bdt3 = bdt2;
            bdt2 = bdtval;
            tr4  = tr3;
            tr3  = tr2;
            tr2  = part;
          } else if ( bdtval > bdt3 ) {
            bdt4 = bdt3;
            bdt3 = bdtval;
            tr4  = tr3;
            tr3  = part;
          } else if ( bdtval > bdt4 ) {
            bdt4 = bdt3;
            tr4  = part;
          }
        }
      }
    } // end particles loop
  }   // end particle types loop

  if ( tr1 != NULL ) {
    b1    = true;
    p1x   = tr1->momentum().px();
    p1y   = tr1->momentum().py();
    p1z   = tr1->momentum().pz();
    chtr1 = tr1->charge();
  }
  if ( tr2 != NULL ) {
    b2    = true;
    p2x   = tr2->momentum().px();
    p2y   = tr2->momentum().py();
    p2z   = tr2->momentum().pz();
    chtr2 = tr2->charge();
  }
  if ( tr3 != NULL ) {
    b3    = true;
    p3x   = tr3->momentum().px();
    p3y   = tr3->momentum().py();
    p3z   = tr3->momentum().pz();
    chtr3 = tr3->charge();
  }
  if ( tr4 != NULL ) {
    b4    = true;
    p4x   = tr4->momentum().px();
    p4y   = tr4->momentum().py();
    p4z   = tr4->momentum().pz();
    chtr4 = tr4->charge();
  }

  if ( tr1 != NULL ) {
    if ( tr1->proto() != NULL ) {
      ismu1  = LoKi::Cuts::ISMUON( tr1 );
      pid1K  = tr1->proto()->info( LHCb::ProtoParticle::CombDLLk, -1000 );
      pid1p  = tr1->proto()->info( LHCb::ProtoParticle::CombDLLp, -1000 );
      pid1mu = tr1->proto()->info( LHCb::ProtoParticle::CombDLLmu, -1000 );
      pid1pi = tr1->proto()->info( LHCb::ProtoParticle::CombDLLpi, -1000 );
    }
  }
  if ( tr2 != NULL ) {
    if ( tr2->proto() != NULL ) {
      ismu2  = LoKi::Cuts::ISMUON( tr2 );
      pid2K  = tr2->proto()->info( LHCb::ProtoParticle::CombDLLk, -1000 );
      pid2p  = tr2->proto()->info( LHCb::ProtoParticle::CombDLLp, -1000 );
      pid2mu = tr2->proto()->info( LHCb::ProtoParticle::CombDLLmu, -1000 );
      pid2pi = tr2->proto()->info( LHCb::ProtoParticle::CombDLLpi, -1000 );
    }
  }
  if ( tr3 != NULL ) {
    if ( tr3->proto() != NULL ) {
      ismu3  = LoKi::Cuts::ISMUON( tr3 );
      pid3K  = tr3->proto()->info( LHCb::ProtoParticle::CombDLLk, -1000 );
      pid3p  = tr3->proto()->info( LHCb::ProtoParticle::CombDLLp, -1000 );
      pid3mu = tr3->proto()->info( LHCb::ProtoParticle::CombDLLmu, -1000 );
      pid3pi = tr3->proto()->info( LHCb::ProtoParticle::CombDLLpi, -1000 );
    }
  }
  if ( tr4 != NULL ) {
    if ( tr4->proto() != NULL ) {
      ismu4  = LoKi::Cuts::ISMUON( tr4 );
      pid4K  = tr4->proto()->info( LHCb::ProtoParticle::CombDLLk, -1000 );
      pid4p  = tr4->proto()->info( LHCb::ProtoParticle::CombDLLp, -1000 );
      pid4mu = tr4->proto()->info( LHCb::ProtoParticle::CombDLLmu, -1000 );
      pid4pi = tr4->proto()->info( LHCb::ProtoParticle::CombDLLpi, -1000 );
    }
  }
  test &= tuple->column( prefix + "_pmu_ISOLATION_BDT1" + m_outputSuffix, maxbdt );
  test &= tuple->column( prefix + "_pmu_ISOLATION_BDT2" + m_outputSuffix, bdt2 );
  test &= tuple->column( prefix + "_pmu_ISOLATION_BDT3" + m_outputSuffix, bdt3 );
  test &= tuple->column( prefix + "_pmu_ISOLATION_BDT4" + m_outputSuffix, bdt4 );
  test &= tuple->column( prefix + "_pmu_TR1_PX" + m_outputSuffix, p1x );
  test &= tuple->column( prefix + "_pmu_TR1_PY" + m_outputSuffix, p1y );
  test &= tuple->column( prefix + "_pmu_TR1_PZ" + m_outputSuffix, p1z );
  test &= tuple->column( prefix + "_pmu_TR2_PX" + m_outputSuffix, p2x );
  test &= tuple->column( prefix + "_pmu_TR2_PY" + m_outputSuffix, p2y );
  test &= tuple->column( prefix + "_pmu_TR2_PZ" + m_outputSuffix, p2z );
  test &= tuple->column( prefix + "_pmu_TR4_PX" + m_outputSuffix, p4x );
  test &= tuple->column( prefix + "_pmu_TR4_PY" + m_outputSuffix, p4y );
  test &= tuple->column( prefix + "_pmu_TR4_PZ" + m_outputSuffix, p4z );
  test &= tuple->column( prefix + "_pmu_TR3_PX" + m_outputSuffix, p3x );
  test &= tuple->column( prefix + "_pmu_TR3_PY" + m_outputSuffix, p3y );
  test &= tuple->column( prefix + "_pmu_TR3_PZ" + m_outputSuffix, p3z );
  test &= tuple->column( prefix + "_pmu_TR1_isMu" + m_outputSuffix, ismu1 );
  test &= tuple->column( prefix + "_pmu_TR2_isMu" + m_outputSuffix, ismu2 );
  test &= tuple->column( prefix + "_pmu_TR3_isMu" + m_outputSuffix, ismu3 );
  test &= tuple->column( prefix + "_pmu_TR4_isMu" + m_outputSuffix, ismu4 );
  test &= tuple->column( prefix + "_pmu_TR1_NNULL" + m_outputSuffix, b1 );
  test &= tuple->column( prefix + "_pmu_TR2_NNULL" + m_outputSuffix, b2 );
  test &= tuple->column( prefix + "_pmu_TR3_NNULL" + m_outputSuffix, b3 );
  test &= tuple->column( prefix + "_pmu_TR4_NNULL" + m_outputSuffix, b4 );

  test &= tuple->column( prefix + "_pmu_TR1_PIDK" + m_outputSuffix, pid1K );
  test &= tuple->column( prefix + "_pmu_TR1_PIDp" + m_outputSuffix, pid1p );
  test &= tuple->column( prefix + "_pmu_TR1_PIDmu" + m_outputSuffix, pid1mu );
  test &= tuple->column( prefix + "_pmu_TR1_PIDpi" + m_outputSuffix, pid1pi );

  test &= tuple->column( prefix + "_pmu_TR2_PIDK" + m_outputSuffix, pid2K );
  test &= tuple->column( prefix + "_pmu_TR2_PIDp" + m_outputSuffix, pid2p );
  test &= tuple->column( prefix + "_pmu_TR2_PIDmu" + m_outputSuffix, pid2mu );
  test &= tuple->column( prefix + "_pmu_TR2_PIDpi" + m_outputSuffix, pid2pi );

  test &= tuple->column( prefix + "_pmu_TR3_PIDK" + m_outputSuffix, pid3K );
  test &= tuple->column( prefix + "_pmu_TR3_PIDp" + m_outputSuffix, pid3p );
  test &= tuple->column( prefix + "_pmu_TR3_PIDmu" + m_outputSuffix, pid3mu );
  test &= tuple->column( prefix + "_pmu_TR3_PIDpi" + m_outputSuffix, pid3pi );

  test &= tuple->column( prefix + "_pmu_TR4_PIDK" + m_outputSuffix, pid4K );
  test &= tuple->column( prefix + "_pmu_TR4_PIDp" + m_outputSuffix, pid4p );
  test &= tuple->column( prefix + "_pmu_TR4_PIDmu" + m_outputSuffix, pid4mu );
  test &= tuple->column( prefix + "_pmu_TR4_PIDpi" + m_outputSuffix, pid4pi );

  test &= tuple->column( prefix + "_pmu_TR1_CH" + m_outputSuffix, chtr1 );
  test &= tuple->column( prefix + "_pmu_TR2_CH" + m_outputSuffix, chtr2 );
  test &= tuple->column( prefix + "_pmu_TR3_CH" + m_outputSuffix, chtr3 );
  test &= tuple->column( prefix + "_pmu_TR4_CH" + m_outputSuffix, chtr4 );

  return StatusCode( test );
}

//=============================================================================
// Check if the track is already in the decay
//=============================================================================
bool TupleToolApplypMuIsolation::isTrackInDecay( const LHCb::Track* track, std::vector<const LHCb::Track*> daughters ) {
  bool isInDecay = false;
  // loop over daughters
  for ( std::vector<const LHCb::Track*>::iterator it = daughters.begin(); it != daughters.end(); ++it ) {
    const LHCb::Track* itrack = ( *it );
    if ( itrack ) {
      if ( itrack == track ) {
        if ( msgLevel( MSG::DEBUG ) ) debug() << "Track is in decay, skipping it" << endmsg;
        isInDecay = true;
      }
    }
  } // end daughter loop

  return isInDecay;
}

//=============================================================================
// MINIPCHI2 for a track
//=============================================================================
double TupleToolApplypMuIsolation::getminipchi2( const LHCb::Particle* track ) {
  double                 minchi2 = -1;
  const RecVertex::Range PV      = m_dva->primaryVertices();
  if ( !PV.empty() ) {
    for ( RecVertex::Range::const_iterator pv = PV.begin(); pv != PV.end(); ++pv ) {
      double ip, chi2;
      m_dist->distance( (const LHCb::Particle*)track, *pv, ip, chi2 ).ignore();
      if ( ( chi2 < minchi2 ) || ( minchi2 < 0. ) ) {
        LHCb::RecVertex newPV( **pv );
        m_pvReFitter->remove( track, &newPV ).ignore();
        LHCb::RecVertex* newPVPtr = (LHCb::RecVertex*)&newPV;
        m_dist->distance( (LHCb::Particle*)track, (LHCb::VertexBase*)newPVPtr, ip, chi2 ).ignore();
        minchi2 = chi2;
      }
    }
  }
  return minchi2;
}

//=============================================================================
// Opening angle for a track and particle
//=============================================================================
double TupleToolApplypMuIsolation::getopening( const LHCb::Track* track, const LHCb::Particle* P ) {
  Gaudi::XYZVector A          = P->momentum().Vect();
  Gaudi::XYZVector B          = track->momentum();
  double           cosopening = A.Dot( B ) / std::sqrt( A.Mag2() * B.Mag2() );
  return cosopening;
}

//=============================================================================
// Check if the track is already in the decay
//=============================================================================
bool TupleToolApplypMuIsolation::isTrackInDecay( const LHCb::Track* track ) {
  bool isInDecay = false;

  for ( std::vector<const LHCb::Particle*>::iterator ip = m_daughtertracks.begin(); ip != m_daughtertracks.end();
        ++ip ) {
    const LHCb::ProtoParticle* proto = ( *ip )->proto();
    if ( proto ) {
      const LHCb::Track* myTrack = proto->track();
      if ( myTrack ) {
        if ( myTrack == track ) {
          if ( msgLevel( MSG::DEBUG ) ) debug() << "Track is in decay, skipping it" << endmsg;
          isInDecay = true;
        }
      }
    }
  }
  return isInDecay;
}
//=============================================================================
// Save the particles in the decay chain (recursive function)
//=============================================================================
void TupleToolApplypMuIsolation::saveDecayParticles( const LHCb::Particle* top ) {
  // -- Get the daughters of the top particle
  const SmartRefVector<LHCb::Particle> daughters = top->daughters();

  // -- Fill all the daugthers in m_decayParticles
  for ( SmartRefVector<LHCb::Particle>::const_iterator ip = daughters.begin(); ip != daughters.end(); ++ip ) {

    // -- If the particle is stable, save it in the vector, or...
    if ( ( *ip )->isBasicParticle() ) {
      if ( msgLevel( MSG::DEBUG ) ) debug() << "Filling particle with ID " << ( *ip )->particleID().pid() << endmsg;

      m_daughtertracks.push_back( ( *ip ) );
    } else {
      // -- if it is not stable, call the function recursively
      m_daughtertracks.push_back( ( *ip ) );
      if ( msgLevel( MSG::DEBUG ) ) debug() << "Filling particle with ID " << ( *ip )->particleID().pid() << endmsg;

      saveDecayParticles( ( *ip ) );
    }
  }
  return;
}
