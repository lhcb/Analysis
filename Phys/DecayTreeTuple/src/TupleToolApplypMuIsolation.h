/*****************************************************************************\
* (c) Copyright 2000-2025 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#ifndef TupleToolApplypMuIsolation_H
#define TupleToolApplypMuIsolation_H

// Include files
// from Gaudi
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "Kernel/IParticleTupleTool.h" // Interface
#include "TrackInterfaces/ITrackVertexer.h"

#include "TMVA/Reader.h"

class IDVAlgorithm;
class IDistanceCalculator;
class IVertexFit;
class IPVReFitter;

namespace LHCb {
  class Particle;
  class Vertex;
} // namespace LHCb

/** @class TupleToolApplypMuIsiolation TupleToolApplypMuIsolation.h
 *
 * \brief Fill isolation information for DecayTreeTuple
 *
 * \sa DecayTreeTuple
 *
 *  @author Mitesh Patel, Patrick Koppenburg, Will Sutcliffe
 *  @date   2024-02-06
 */
class TupleToolApplypMuIsolation : public TupleToolBase, virtual public IParticleTupleTool {
public:
  /// Standard constructor
  TupleToolApplypMuIsolation( const std::string& type, const std::string& name, const IInterface* parent );

  virtual ~TupleToolApplypMuIsolation(){}; ///< Destructor

  StatusCode initialize() override;

  StatusCode fill( const LHCb::Particle*, const LHCb::Particle*, const std::string&, Tuples::Tuple& ) override;

protected:
private:
  Float_t m_opening, m_minipchi2, m_ghostprob, m_trackchi2, m_pt, m_chi2, m_type;

  std::vector<const LHCb::Particle*> m_daughtertracks;

  bool isTrackInDecay( const LHCb::Track*, std::vector<const LHCb::Track*> );
  bool isTrackInDecay( const LHCb::Track* );
  void saveDecayParticles( const LHCb::Particle* );

  double        getminipchi2( const LHCb::Particle* );
  double        getopening( const LHCb::Track*, const LHCb::Particle* );
  IDVAlgorithm* m_dva;

  IDistanceCalculator*     m_dist;
  const IVertexFit*        m_pVertexFit;
  IPVReFitter*             m_pvReFitter;
  TMVA::Reader*            m_Reader;
  std::string              m_outputSuffix;
  std::string              m_weightsName;
  std::vector<std::string> m_inputParticles;
};
#endif // TupleToolApplypMuIsolation_H
