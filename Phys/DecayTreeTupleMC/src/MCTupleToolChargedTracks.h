/*****************************************************************************\
 * (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
 \*****************************************************************************/
#ifndef MCTUPLETOOLChargedTracks_H
#define MCTUPLETOOLChargedTracks_H 1

// Include files
// from Gaudi
#include "Kernel/IEventTupleTool.h" // Interface
#include "Kernel/IParticleTupleTool.h"
#include "TrackInterfaces/ITrackVertexer.h"
// from Analysis
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "Kernel/PP2MCLocation.h"
#include "Kernel/Particle2MCLinker.h"
using namespace LHCb;

class ITupleTool;

/** @class MCTupleToolChargedTracks, MCTupleToolChargedTracks.h
 *
 * \brief Add Number of Generated Prompt Charged Particles info to MCDecayTreeTuple or EventTuple
 *
 * Tuple columns:
 * - Ngen_user: number of the Prompt Charged Particles generated in the event
 *         within the kinematic range chosen by the user
 *
 * Default min pt 500 Mev/c, min p 2GeV/c, eta between 2 and 4.8
 * The user can change the cuts applied on the particles to be counted.
 * To set no cuts, the value should be set to -1.
 *
 * VERBOSE mode:
 *
 * - Ngen_eta1: number of Prompt Charged Particles generated in the event
 *              within the pt range chosen by the user and eta between
 *              2.5 and 3.3 (applying boost to CMS in pPb collisions)
 * - Ngen_eta2: number of Prompt Charged Particles generated in the event
 *              within the pt range chosen by the user and eta between
 *              3.3 and 4.3 (applying boost to CMS in pPb collisions)
 *
 * - Ngen_LHCb: number of Prompt Charged Particles generated in the event
 *              within the kinematic range chosen by the user and eta between
 *              2 and 4.8
 *
 *  If AddMaxChargedParticles is set to a value different from 0, the properties
 *  of the charged tracks within the user defined range are added in arrays.
 *  AddMaxChargedParticles is the maximal number of added particles.
 *  Note that this option may signiticantly increase the size of the tuple.
 *  The branches added are:
 *
 * "H_TRUEID"
 * "H_MOTHER_ID"
 * "H_GD_MOTHER_ID"
 * "H_GD_GD_MOTHER_ID"
 * "H_TRUEORIGINVERTEX_X"
 * "H_TRUEORIGINVERTEX_Y"
 * "H_TRUEORIGINVERTEX_Z"
 * "H_ORIGINVERTEX_Type"
 * "H_TRUEP_X"
 * "H_TRUEP_Y"
 * "H_TRUEP_Z"
 * "H_TRUEP"
 * "H_TRUEPT"
 * "H_TRUETHETA"
 * "H_TRUEPHI"
 * "H_TRUEETA"
 * "H_ThreeCharge"
 * "H_truthWeight"
 * "H_prompt"
 *
 * \sa MCDecayTreeTuple
 *  @author Samuel Belin, Imanol Corredoira, Clara Landesa
 *  @date  29/11/2024
 */

class MCTupleToolChargedTracks : public TupleToolBase, virtual public IEventTupleTool {
public:
  /// Standard constructor
  MCTupleToolChargedTracks( const std::string& type, const std::string& name, const IInterface* parent );

  ~MCTupleToolChargedTracks() override = default; ///< Destructor

  StatusCode fill( Tuples::Tuple& ) override;
  StatusCode initialize() override;
  // StatusCode finalize() override;

private:
  Object2MCLinker<LHCb::Track>* track2MCLink() const {
    if ( !m_track2MCLink ) { m_track2MCLink = new Object2MCLinker<LHCb::Track>( this, "", "", m_InputParticles ); }
    return m_track2MCLink;
  }

  Double_t getTau( int mcid ) {
    double tau = 0;
    // Need to include all particle with tau>0.3E-10
    std::map<int, double> lifetimes = {
        {211, 2.6033E-8},       // pi+-
        {321, 1.2380E-8},       // K+-
        {2212, 10E3},           // p+-
        {13, 2.1969811E10 - 6}, // mu+-
        {11, 10E3},             // e+-
        {3312, 1.639E-10},      // Xi- (+) baryon
        {3222, 0.8018E-10},     // Sigma+(-) baryon
        {3112, 1.479E-10},      // Sigma-(+) baryon
        {3334, 0.821E-10},      // Omega-(+) baryon
        {22, 10E3},             // Gamma (photon)
        {12, 10E3},             // neutrino e
        {14, 10E3},             // neutrino mu
        {16, 10E3},             // neutrino tau
        {310, 0.89E-10},        // Kshort
        {130, 5.2E-8},          // Klong
        {2112, 881.5},          // neutron
        {3122, 2.631E-10},      // Lambda baryon
        {3322, 2.9E-10},        // Xi0 baryon
    };
    if ( lifetimes.count( mcid ) > 0 ) {
      tau = lifetimes[mcid];
    } else {
      if ( mcid > 10000 ) {
        tau = 10E3;
      } else {
        tau = 0;
      }
    }
    return tau;
  }

  bool IsFrompp( const LHCb::MCParticle* p ) {
    // Returns true if the particle comes from a decay chain originated in the pp collision (not in a interaction from
    // the material, for instance) Also allows for middle steps as: Oscillate & decay (type == 3) String fragmentation &
    // (type == 4) Type >4: they all correspond to other kinds of interactions (hadronic, conversion, delta ray..) Type
    // 0: unknown, following tracking twiki it is considered as a hadronic interaction not yet recognized (It seems that
    // there is no need to include these two cases for pA/Ap (EPOS). However, in the case of pp (pythia), if they are
    // not included, a fraction of particles that should be prompt are classificated as secondaries. The reason is
    // probably that the B oscillations and string fragmentations from quarks are not implemented in EPOS, but they are
    // in pythia. (Pythia uses Lund string model, EPOS has other approach).

    bool output = false;
    bool stop   = false; // To control loop
    while ( !stop ) {
      int originV_type = p->originVertex()->type();
      if ( originV_type == 1 ) { // ppCollision
        stop   = true;
        output = true;
      } else if ( originV_type > 4 || originV_type == 0 ) { // no ppCollision (1), decay (2), oscillate and decay (3),
                                                            // string fragmentation (4)
        stop   = true;
        output = false;
      } else if ( originV_type == 2 || originV_type == 3 || originV_type == 4 ) {
        stop = false; // Keep in the loop and pass to the next iteration
        p    = p->mother();
      }
      //
      else {
        warning() << "There has been an error, not expected originV_type " << originV_type << endmsg;
      }
    }
    return output;
  }

  bool isprompt( const LHCb::MCParticle* p ) {
    const LHCb::MCVertex* OV       = p->originVertex();
    double                tau      = getTau( int( p->particleID().abspid() ) );
    bool                  isPrompt = false;
    //# 1: Check that decay chain of the particle comes from pp collision (not hadronic interaction, etc. )
    if ( IsFrompp( p ) == false ) { return false; }
    //# 2: Check particle origin
    if ( OV->type() == 1 ) { // # type 1 == ppCollision
      if ( tau > 0.3E-10 ) {
        isPrompt = true;
      } else if ( tau < 0.3E-10 ) {
        isPrompt = false;
      }
    } else if ( OV->type() == 2 || OV->type() == 3 || OV->type() == 4 ) { // type 2 == decayVertex CHECK
      const LHCb::MCParticle* p_mother   = p->mother();
      double                  tau_mother = getTau( int( p_mother->particleID().abspid() ) );
      if ( tau > 0.3E-10 ) {
        if ( tau_mother < 0.3E-10 ) {
          isPrompt = true;
        } else {
          isPrompt = false;
        }
      } else {
        isPrompt = false;
      }
    } else {
      isPrompt = false;
    }
    return isPrompt;
  }

private:
  // Tools
  mutable Object2MCLinker<LHCb::Track>* m_track2MCLink;

  // member variables
  std::vector<std::string> m_InputParticles;
  std::string              m_PVLocation;
  std::string              m_Beam;
  double                   m_y;
  float                    m_pmin;
  float                    m_pmax;
  float                    m_ptmin;
  float                    m_ptmax;
  float                    m_etamin;
  float                    m_etamax;
  unsigned long            m_mult;
};
#endif //
