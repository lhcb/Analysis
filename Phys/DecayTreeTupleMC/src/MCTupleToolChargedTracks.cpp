/*****************************************************************************\
 * (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
 \*****************************************************************************/

// Include files

#include "MCTupleToolChargedTracks.h"

#include "GaudiAlg/ITupleTool.h"
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"

#include "GaudiKernel/IRegistry.h" // IOpaqueAddress

#include "Kernel/IDistanceCalculator.h"

#include "Event/Particle.h"
#include "Event/RecVertex.h"
#include "Event/Track.h"
#include "Event/VertexBase.h"

#include "LoKi/ParticleProperties.h"

#include "Event/MCParticle.h"
#include "Kernel/IParticle2MCAssociator.h"

//-----------------------------------------------------------------------------
// Implementation file for class : MCTupleToolChargedTracks
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( MCTupleToolChargedTracks )

// Standard constructor, initializes variables
//=============================================================================
MCTupleToolChargedTracks::MCTupleToolChargedTracks( const std::string& type, const std::string& name,
                                                    const IInterface* parent )
    : TupleToolBase( type, name, parent ), m_track2MCLink( NULL ) {
  declareInterface<IEventTupleTool>( this );

  declareProperty( "InputParticles", m_InputParticles = {LHCb::MCParticleLocation::Default}, "List of input tracks" );
  declareProperty( "InputPrimaryVertices", m_PVLocation = LHCb::RecVertexLocation::Primary, "List of input PVs" );

  declareProperty( "Beam", m_Beam = "pPb", "Collision type: pp, pPb, Pbp" ); // pp, pPb, Pbp
  declareProperty( "Pmin", m_pmin = 2000.,
                   "Cut on the minimum momentum of the charged particles (MeV/c). If -1, no cut applied." );
  declareProperty( "Pmax", m_pmax = -1,
                   "Cut on the maximum momentum of the charged particles (MeV/c). If -1, no cut applied." );
  declareProperty( "PTmin", m_ptmin = 500.,
                   "Cut on the min transverse momentum of the charged particles (MeV/c). If -1, no cut applied." );
  declareProperty( "PTmax", m_ptmax = 8000,
                   "Cut on transverse momentum of the charged particles (MeV/c). If -1, no cut applied." );
  declareProperty( "Etamin", m_etamin = -2, "Cut on eta angle of the charged particles. If -1, no cut applied." );
  // default value is given in the initialisation
  declareProperty( "Etamax", m_etamax = -2, "Cut on eta angle of the charged particles. If -1, no cut applied." );
  // default value is given in the initialisation
  declareProperty(
      "AddMaxChargedParticles", m_mult = 0,
      "Maximal number of added particles. If 0, the columns with single-particle information are not added." );
}
//=============================================================================

//______________________________________________________________________________
StatusCode MCTupleToolChargedTracks::initialize() {
  if ( !TupleToolBase::initialize() ) return StatusCode::FAILURE;

  m_y = 0;
  if ( m_Beam == "pPb" ) {
    m_y = +0.465;
  } else if ( m_Beam == "Pbp" ) {
    m_y = -0.465;
  }

  if ( m_etamin == -2 ) { m_etamin = 2.5 + m_y; }
  if ( m_etamax == -2 ) { m_etamax = 4.3 + m_y; }

  msg() << "----------- MCTupleToolChargedTracks --------------" << endmsg;
  msg() << " Etamin: " << m_etamin << " ; Etamax:  " << m_etamax << endmsg;
  msg() << " PTmin (MeV/c): " << m_ptmin << " ; PTmax (MeV/c):  " << m_ptmax << endmsg;
  msg() << " CMS_to_lab: " << m_y << endmsg;

  return StatusCode::SUCCESS;
}

StatusCode MCTupleToolChargedTracks::fill( Tuples::Tuple& tuple ) {
  const std::string prefix = fullName();

  // get particles
  bool   test     = true;
  double npartsel = 0;
  double nch_eta1 = 0;
  double nch_eta2 = 0;
  double nch_LHCb = 0;

  std::vector<int>    h_trueid, h_motherid, h_gd_motherid, h_gd_gd_motherid;
  std::vector<double> h_truex, h_truey, h_truez;
  std::vector<int>    h_ov_type;
  std::vector<int>    h_gen_type; // 1: prompt and in acceptance. 0: non-prompt or not in acceptance
  std::vector<double> h_truepx, h_truepy, h_truepz, h_truep, h_truept, h_truepe;
  std::vector<double> h_trueeta, h_truephi, h_truetheta, h_truthWeight, h_prompt, h_trueCharge;

  // Loop over all particles
  for ( auto location : m_InputParticles ) {

    LHCb::MCParticles* mcps = get<LHCb::MCParticles>( location );

    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "Getting particles from " << location << " with " << ( mcps )->size() << " mcparticles" << endmsg;
    }
    // loop over MC particles
    for ( LHCb::MCParticles::const_iterator it = mcps->begin(); it != mcps->end(); ++it ) {

      LHCb::MCParticle* mcp = ( *it );

      if ( !mcp ) {
        info() << "particle to be added not found" << endmsg;
        continue;
      }

      double threeCharge = 0;
      threeCharge        = mcp->particleID().threeCharge();

      if ( threeCharge == 0 ) { continue; }

      int    trueid             = 0;
      int    mc_mother_id       = 0;
      int    mc_gd_mother_id    = 0;
      int    mc_gd_gd_mother_id = 0;
      double originV_x          = 0.;
      double originV_y          = 0.;
      double originV_z          = 0.;
      int    originV_type       = 0;
      double truePX             = 0.;
      double truePY             = 0.;
      double truePZ             = 0.;
      double truePT             = 0.;
      double truePE             = 0.;
      double trueP              = 0.;
      double trueETA            = 0.;
      double truePHI            = 0.;
      double trueTHETA          = 0.;
      int    gen_type           = 0;
      double isPrompt           = 0;

      isPrompt = isprompt( mcp );

      auto mcp_p = mcp->momentum();
      truePT     = mcp_p.pt();
      trueP      = mcp_p.r();
      trueETA    = mcp_p.eta();

      // Counting generated particles
      if ( isPrompt == 1 ) {

        if ( ( truePT > m_ptmin || m_ptmin == -1 ) && ( truePT < m_ptmax || m_ptmax == -1 ) &&
             ( trueP > m_pmin || m_pmin == -1 ) && ( trueP < m_pmax || m_pmax == -1 ) ) {

          if ( ( trueETA > m_etamin || m_etamin == -1 ) && ( trueETA < m_etamax || m_etamax == -1 ) ) {
            npartsel++;
            gen_type = 1;
          }

          if ( isVerbose() ) {
            if ( ( trueETA > 2.5 + m_y ) && ( trueETA < 3.3 + m_y ) ) { nch_eta1++; }
            if ( ( trueETA > 3.3 + m_y ) && ( trueETA < 4.3 + m_y ) ) { nch_eta2++; }
            if ( ( trueETA > 2 ) && ( trueETA < 4.8 ) ) { nch_LHCb++; }
          }
        }

        if ( m_mult > 0 ) {

          if ( gen_type == 1 ) {
            // retrieve additional information
            trueid = mcp->particleID().pid();
            const LHCb::MCParticle* mcpmom( 0 );
            mcpmom = mcp->mother();
            if ( mcpmom ) {
              mc_mother_id = mcpmom->particleID().pid();
              const LHCb::MCParticle* mcpmom_mom( 0 );
              mcpmom_mom = mcpmom->mother();
              if ( mcpmom_mom ) {
                mc_gd_mother_id = mcpmom_mom->particleID().pid();
                const LHCb::MCParticle* mcpmom_mom_mom( 0 );
                mcpmom_mom_mom = mcpmom_mom->mother();
                if ( mcpmom_mom_mom ) { mc_gd_gd_mother_id = mcpmom_mom_mom->particleID().pid(); }
              }
            }
            trueTHETA    = mcp_p.theta();
            truePX       = mcp_p.px();
            truePY       = mcp_p.py();
            truePZ       = mcp_p.pz();
            truePE       = mcp_p.E();
            truePHI      = mcp_p.phi();
            auto mcp_ov  = mcp->originVertex()->position();
            originV_x    = mcp_ov.x();
            originV_y    = mcp_ov.y();
            originV_z    = mcp_ov.z();
            originV_type = mcp->originVertex()->type();

            h_trueid.push_back( trueid );
            h_motherid.push_back( mc_mother_id );
            h_gd_motherid.push_back( mc_gd_mother_id );
            h_gd_gd_motherid.push_back( mc_gd_gd_mother_id );
            h_truex.push_back( originV_x );
            h_truey.push_back( originV_y );
            h_truez.push_back( originV_z );
            h_ov_type.push_back( originV_type );
            h_truepx.push_back( truePX );
            h_truepy.push_back( truePY );
            h_truepz.push_back( truePZ );
            h_truept.push_back( truePT );
            h_truepe.push_back( truePE );
            h_truep.push_back( trueP );
            h_trueeta.push_back( trueETA );
            h_truephi.push_back( truePHI );
            h_truetheta.push_back( trueTHETA );
            h_prompt.push_back( isPrompt );
            h_trueCharge.push_back( threeCharge );
            h_gen_type.push_back( gen_type );
          }
        }
      }
    }
  }

  test &= tuple->column( prefix + "Ngen_user", npartsel );

  if ( isVerbose() ) {
    test &= tuple->column( "Ngen_eta1", nch_eta1 );
    test &= tuple->column( "Ngen_eta2", nch_eta2 );
    test &= tuple->column( "Ngen_LHCb", nch_LHCb );
  }

  if ( m_mult > 0 ) {

    test &= tuple->farray( prefix + "H_TRUEID", h_trueid.begin(), h_trueid.end(), prefix + "n_Particles", m_mult );
    test &=
        tuple->farray( prefix + "H_MOTHER_ID", h_motherid.begin(), h_motherid.end(), prefix + "n_Particles", m_mult );
    test &= tuple->farray( prefix + "H_GD_MOTHER_ID", h_gd_motherid.begin(), h_gd_motherid.end(),
                           prefix + "n_Particles", m_mult );
    test &= tuple->farray( prefix + "H_GD_GD_MOTHER_ID", h_gd_gd_motherid.begin(), h_gd_gd_motherid.end(),
                           prefix + "n_Particles", m_mult );
    test &= tuple->farray( prefix + "H_TRUEORIGINVERTEX_X", h_truex.begin(), h_truex.end(), prefix + "n_Particles",
                           m_mult );
    test &= tuple->farray( prefix + "H_TRUEORIGINVERTEX_Y", h_truey.begin(), h_truey.end(), prefix + "n_Particles",
                           m_mult );
    test &= tuple->farray( prefix + "H_TRUEORIGINVERTEX_Z", h_truez.begin(), h_truez.end(), prefix + "n_Particles",
                           m_mult );
    test &= tuple->farray( prefix + "H_ORIGINVERTEX_Type", h_ov_type.begin(), h_ov_type.end(), prefix + "n_Particles",
                           m_mult );
    test &= tuple->farray( prefix + "H_TRUEP_X", h_truepx.begin(), h_truepx.end(), prefix + "n_Particles", m_mult );
    test &= tuple->farray( prefix + "H_TRUEP_Y", h_truepy.begin(), h_truepy.end(), prefix + "n_Particles", m_mult );
    test &= tuple->farray( prefix + "H_TRUEP_Z", h_truepz.begin(), h_truepz.end(), prefix + "n_Particles", m_mult );
    test &= tuple->farray( prefix + "H_TRUEP", h_truep.begin(), h_truep.end(), prefix + "n_Particles", m_mult );
    test &= tuple->farray( prefix + "H_TRUEPT", h_truept.begin(), h_truept.end(), prefix + "n_Particles", m_mult );
    test &=
        tuple->farray( prefix + "H_TRUETHETA", h_truetheta.begin(), h_truetheta.end(), prefix + "n_Particles", m_mult );
    test &= tuple->farray( prefix + "H_TRUEPHI", h_truephi.begin(), h_truephi.end(), prefix + "n_Particles", m_mult );
    test &= tuple->farray( prefix + "H_TRUEETA", h_trueeta.begin(), h_trueeta.end(), prefix + "n_Particles", m_mult );
    test &= tuple->farray( prefix + "H_ThreeCharge", h_trueCharge.begin(), h_trueCharge.end(), prefix + "n_Particles",
                           m_mult );
    test &= tuple->farray( prefix + "H_truthWeight", h_truthWeight.begin(), h_truthWeight.end(), prefix + "n_Particles",
                           m_mult );
    test &= tuple->farray( prefix + "H_prompt", h_prompt.begin(), h_prompt.end(), prefix + "n_Particles", m_mult );
    test &=
        tuple->farray( prefix + "H_gen_type", h_gen_type.begin(), h_gen_type.end(), prefix + "n_Particles", m_mult );
  }
  return StatusCode( test );
}
