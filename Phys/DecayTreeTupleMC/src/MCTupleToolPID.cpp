/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "Event/MCParticle.h"
#include "gsl/gsl_sys.h"

// local
#include "MCTupleToolPID.h"

// from Gaudi
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"

#include <string>

// using namespace LHCb;
//-----------------------------------------------------------------------------
// Implementation file for class : MCTupleToolPID
//
// 2009-11-19 : Rob Lambert
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( MCTupleToolPID )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
MCTupleToolPID::MCTupleToolPID( const std::string& type, const std::string& name, const IInterface* parent )
    : TupleToolBase( type, name, parent ) {
  declareInterface<IMCParticleTupleTool>( this );
}

//=============================================================================
// Destructor
//=============================================================================
MCTupleToolPID::~MCTupleToolPID() {}

//=============================================================================
// Fill
//=============================================================================
StatusCode MCTupleToolPID::fill( const LHCb::MCParticle*, const LHCb::MCParticle* mcp, const std::string& head,
                                 Tuples::Tuple& tuple ) {
  const std::string prefix = fullName( head );
  bool              test   = true;

  if ( msgLevel( MSG::DEBUG ) ) debug() << "MCTupleToolPID::fill " << head << endmsg;

  int mcPid = 0;

  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "MCTupleToolPID::fill mcp " << mcp << endmsg;
  // pointer is ready, prepare the values:
  if ( mcp ) { mcPid = mcp->particleID().pid(); }
  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "MCTupleToolPID::fill filling " << head << endmsg;

  test &= tuple->column( prefix + "_ID", mcPid );

  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "MCTupleToolPID::fill bye " << head << endmsg;

  return StatusCode( test );
}
