/*****************************************************************************\
* (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: MCTupleToolReconstructed.h,v 1.5 2010-01-26 15:39:26 rlambert Exp $
#ifndef MCTUPLETOOLMCRECONSTRUCTED_H
#define MCTUPLETOOLMCRECONSTRUCTED_H 1

// Include files
// from Gaudi
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "Event/ProtoParticle.h"
#include "Kernel/IMCParticleTupleTool.h" // Interface
#include "Kernel/Particle2MCLinker.h"

/** @class MCTupleToolReconstructed MCTupleToolReconstructed.h
 *
 *  Fill MC Particle
 *
 * See the properties inline documentation to toggle on/off some of
 * the columns
 *
 * - head_Reconstructible : IMCReconstructible category
 * - head_Reconstructed : IMCReconstructed  category
 * - head_PP_PX,Y,Z : ProtoParticle PX, PY, PZ
 * - head_PP_Weight : ProtoParticle association weight
 * - head_PP_tr_pchi2 : ProtoParticle association chi2
 * - head_PP_DLLe
 * - head_PP_DLLk
 * - head_PP_DLLp
 * - head_PP_DLLmu
 * \sa MCDecayTreeTuple, IMCReconstructible, IMCReconstructed
 *
 *  @author Patrick Koppenburg
 *  @date   2009-01-22
 */
struct IMCReconstructible;
struct IMCReconstructed;

class MCTupleToolReconstructed : public TupleToolBase, virtual public IMCParticleTupleTool {
public:
  /// Standard constructor
  MCTupleToolReconstructed( const std::string& type, const std::string& name, const IInterface* parent );

  virtual ~MCTupleToolReconstructed(); ///< Destructor
  StatusCode initialize() override;

public:
  StatusCode fill( const LHCb::MCParticle*, const LHCb::MCParticle*, const std::string&, Tuples::Tuple& ) override;

private:
  std::vector<std::pair<const LHCb::ProtoParticle*, double>> getProtos( const LHCb::MCParticle* ) const;

#include "isStable.h"

private:
  IMCReconstructible*     m_recible;   ///< Reconstructible tool
  IMCReconstructed*       m_rected;    ///< Reconstructed too
  ProtoParticle2MCLinker* m_pCPPAsct;  ///< Charged protoparticle associator
  ProtoParticle2MCLinker* m_pNPPAsct;  ///< Neutral protoparticle associator
  bool                    m_associate; ///< Do association
  bool                    m_pid;       ///< Do PID
};
#endif // MCTUPLETOOLMCRECONSTRUCTED_H
