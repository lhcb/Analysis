/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// from Gaudi
#include <Event/MCParticle.h>
#include <Event/Particle.h>
#include <GaudiAlg/Tuple.h>
#include <GaudiAlg/TupleObj.h>
#include <GenEvent/HepMCUtils.h>
#include <algorithm>

#include "TupleToolB2XMother.h"

//-----------------------------------------------------------------------------
// Implementation file for class : TupleToolB2XMother
//
// 2016-01-06 : Benedetto Gianluca Siddi
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( TupleToolB2XMother )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TupleToolB2XMother::TupleToolB2XMother( const std::string& type, const std::string& name, const IInterface* parent )
    : TupleToolBase( type, name, parent ) {
  declareInterface<IMCParticleTupleTool>( this );
}

//=============================================================================
StatusCode TupleToolB2XMother::initialize() {

  const StatusCode sc = TupleToolBase::initialize();
  if ( sc.isFailure() ) return sc;

  return StatusCode::SUCCESS;
}

//===========================================================================================
StatusCode TupleToolB2XMother::fill( const MCParticle* /* top */, const MCParticle* mcp, const std::string& head,
                                     Tuples::Tuple& tuple ) {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "TupleToolB2XMother::fill entry:" << m_entry << endmsg;

  // set test true
  bool             fromB = false;
  bool             fromD = false;
  std::vector<int> ancestorIDs;
  std::vector<int> ancestorKeys;

  if ( mcp ) {

    Int_t IDmcp = mcp->particleID().pid();
    if ( msgLevel( MSG::DEBUG ) )
      debug() << "HEAD: " << fullName( head ) << "\t"
              << "mcp ID: " << IDmcp << "\t" << endmsg;

    const MCParticle* PMother = (const MCParticle*)ancestor( mcp, ancestorIDs, ancestorKeys );
    fromB                     = PMother->particleID().hasBottom();
    fromD                     = PMother->particleID().hasCharm();
  }

  const std::vector<int>::size_type ANCESTOR_NB                = 15;
  int                               vancestorIDs[ANCESTOR_NB]  = {0};
  int                               vancestorKeys[ANCESTOR_NB] = {0};

  std::copy_n( ancestorIDs.begin(), std::min( ANCESTOR_NB, ancestorIDs.size() ), vancestorIDs );
  std::copy_n( ancestorKeys.begin(), std::min( ANCESTOR_NB, ancestorKeys.size() ), vancestorKeys );

  const std::string prefix = fullName( head );
  debug() << "IDsSize: " << ancestorIDs.size() << "\t KeysSize: " << ancestorKeys.size() << endmsg;

  bool test = true;
  test &= tuple->column( prefix + "_PANC_fromB", fromB );
  test &= tuple->column( prefix + "_PANC_fromD", fromD );
  test &= tuple->array( prefix + "_PANC_IDs", vancestorIDs, vancestorIDs + sizeof( vancestorIDs ) / sizeof( int ) );
  test &= tuple->array( prefix + "_PANC_Keys", vancestorKeys, vancestorKeys + sizeof( vancestorKeys ) / sizeof( int ) );
  test &= tuple->column( prefix + "_N_ANC", (int)ancestorKeys.size() );

  if ( test ) m_entry++;
  return StatusCode( test );
}

//=============================================================================
// Get ancestor
//=============================================================================
const LHCb::MCParticle* TupleToolB2XMother::ancestor( const LHCb::MCParticle* imc, std::vector<int>& ancIDs,
                                                      std::vector<int>& ancKeys ) const {

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Part PID " << imc->particleID() << endmsg;
    debug() << "Part Key " << imc->key() << endmsg;
  }

  ancIDs.push_back( imc->particleID().pid() );
  ancKeys.push_back( imc->key() );

  if ( imc->mother() ) {
    const LHCb::MCParticle* mc_mom = imc->mother();
    return ancestor( mc_mom, ancIDs, ancKeys );
  } else {
    return imc;
  }
}
