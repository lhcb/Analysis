/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TupleToolB2XMother_H
#define TupleToolB2XMother_H 1

// Include files
// from Gaudi
//#include "GaudiAlg/GaudiTool.h"
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "Kernel/IMCParticleTupleTool.h" // Interface

using namespace LHCb;

/** @class TupleToolB2XMother TupleToolB2XMother.h
 *
 * \brief extract ID and key of MC ancestor particles in a list.
 *
 * Extract columns with the list of ancestor IDs and ancestor keys for specified particles,
 * as well as whether they are from B or D quarks.
 *
 * Tuple columns:
 *  - _PANC_IDs: ancestors particle IDs
 *  - _PANC_Keys: ancestors unique keys
 *  - _PANC_fromB: whether the particle if froma B quark
 *  - _PANC_fromD: : whether the particle if froma D quark
 *  - _N_ANC: number of ancestors
 *
 * \sa DecayTreeTuple
 *
 *  @author Benedetto Gianluca Siddi
 *  @date   2016-01-07
 */
class TupleToolB2XMother : public TupleToolBase, virtual public IMCParticleTupleTool {
public:
  /// Standard constructor
  TupleToolB2XMother( const std::string& type, const std::string& name, const IInterface* parent );

  virtual ~TupleToolB2XMother() = default; ///< Destructor

  virtual StatusCode initialize() override;

  virtual StatusCode fill( const LHCb::MCParticle*, const LHCb::MCParticle*, const std::string&,
                           Tuples::Tuple& ) override;

protected:
private:
  const LHCb::MCParticle* ancestor( const LHCb::MCParticle*, std::vector<int>&, std::vector<int>& ) const;
  int                     m_entry = 0;
};
#endif // TupleToolB2XMother_H
