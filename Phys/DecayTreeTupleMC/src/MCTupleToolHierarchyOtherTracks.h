/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#ifndef MCTUPLETOOLHIERARCHYOTHERTRACKS
#define MCTUPLETOOLHIERARCHYOTHERTRACKS 1

// from Gaudi
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "Kernel/IParticleDescendants.h"
#include "Kernel/IParticleTupleTool.h" // Interface
#include "LHCbMath/BloomFilter.h"

#include <Kernel/GetIDVAlgorithm.h>
#include <Kernel/IDVAlgorithm.h>

// struct Particle2MCLinker;
#include "Kernel/IDaVinciAssociatorsWrapper.h"
#include "Kernel/Particle2MCLinker.h"

/** @class MCTupleToolHierarchyOtherTracks
 *
 * \brief Fill MC hierarchy info of additional tracks including complete track ancestry and origin information
 *
 * \paragraph doc Documentation
 * https://indico.cern.ch/event/967597/contributions/4072151/attachments/2127093/3589534/ttoolhierarchyothertracks.pdf
 *
 * \paragraph bsignalinfo Info related to B signal (scalars)
 * - \b Tr_MC_Bsignal_ID           : Id of mother particle in the signal decay
 * - \b Tr_MC_Bsignal_Key          : Track key of the mother particle in the signal decay
 * - \b Tr_MC_Bsignal_OSCIL        : Whether signal B has oscillated
 * - \b Tr_MC_ANCESTOR_Bsignal_ID  : ID of ancestor of signal b
 * - \b Tr_MC_ANCESTOR_Bsignal_Key : Track key of ancestor of signal b
 *
 * \paragraph ancestry Info related to the MC particle ancestor.
 * Ancestor = first mother of a track that does not have a mother itself (following the decay chain to the beginning)
 * - \b Tr_ANCESTOR_ID             : Ancestor particle ID
 * - \b Tr_ANCESTOR_Key            : Ancestor particle track key
 * - \b Tr_ANCESTOR_DAUGHTER_ID    : Ancestor particle track ID of track before ancestor is reached
 * - \b Tr_ANCESTOR_DAUGHTER_Key   : Ancestor particle track key of track before ancestor is reached
 *
 * \paragraph trackorigin Track origin flags
 * - \b Tr_ORIG_FLAGS              : Track origin flag
 *   * -1 : Track does not have an associated MC particle. Most likely ghost
 *   * 0  : Track is a signal decay track
 *   * 1  : Track comes from same side fragmentation
 *   * 2  : Track comes from a opposite side decay (particle has B0, B+, Bs or Bc+ mother)
 *   * 3  : Track comes from an OS decay of an excited B resonance, like B1(H)0 or B*+
 *   * 4  : Track comes from OS fragmentation process
 *   * 5  : Track is prompt
 *   * +100 : An offset of +100 is added if a track does not come from the same vertex as the signal
 *   Note: If a b quark which is neither the signal, nor the OS b quark is in the event and comes from the same vertex
 * as the signal, it will be treated like an OS b quark
 * - \b Tr_B_FLAGS                 : Track B origin flag
 *   *  0 : does not originates from particle containing b
 *   *  1 : originates from particle containing b
 *   * -1 : Track is one of the signal tracks
 * - \b Tr_fromSignal : Stores the LHCb::MCParticle fromSignal() flag
 *
 * \paragraph mctupletoolhierarchytrackinfo Info related to the MC particle (mother, gd mother, gd gd mother) =
 * variables written out by MCTupleToolHierarchy for signal tracks
 * - \b Tr_MC_ID                   : ID of the track
 * - \b Tr_MC_Key                  : Key of the track in the event (starts at 0 for each event)
 * - \b Tr_MC_ALL_Key              : Track key of every track in the collision, irrespective of type and origin
 * - \b Tr_MC_MOTHER_ID            : true mc mother ID
 * - \b Tr_MC_MOTHER_KEY           : true mc mother key
 * - \b Tr_MC_GD_MOTHER_ID         : grand mother ID (Verbose = True)
 * - \b Tr_MC_GD_MOTHER_KEY        : grand mother key (Verbose = True)
 * - \b Tr_MC_GD_GD_MOTHER_ID      : grand grand mother ID (Verbose = True)
 * - \b Tr_MC_GD_GD_MOTHER_KEY     : grand grand mother key (Verbose = True)
 * - \b N_MC                       : Number of tracks seen by this tool instance in each event
 *
 * \paragraph originvertexhierarchyinfo Info related to the origin vertex of the track ancestor
 * - \b Tr_MC_OV_AssocToSignalPV   : Whether the track ancestor is related to the same origin vertex as the signal
 * - \b Tr_MC_OV_isPrimary         : Whether the track ancestor is primary
 * - \b Tr_MC_OV_types             : Ancestor origin vertex type
 * - \b Tr_MC_OV_products          : Number of ancestor origin vertex products
 * - \b Tr_MC_OV_Key               : Key of the ancestor origin vertex
 * - \b Tr_MC_OV_X                 : Associated ancestor origin vertex x position
 * - \b Tr_MC_OV_Y                 : Associated ancestor origin vertex y position
 * - \b Tr_MC_OV_Z                 : Associated ancestor origin vertex z position
 * \sa DecayTreeTupleMC
 *
 *  @author Simon Akar
 *  @author Vukan Jevtic
 *  @author Basem Khanji
 */

using BF = BloomFilter<const LHCb::Track*, 12, 5, 100>;

class IParticle2MCAssociator;
class MCTupleToolHierarchyOtherTracks : public TupleToolBase, virtual public IParticleTupleTool {
public:
  MCTupleToolHierarchyOtherTracks( const std::string& type, const std::string& name, const IInterface* parent );

  virtual ~MCTupleToolHierarchyOtherTracks() = default;

  virtual StatusCode initialize() override;

  virtual StatusCode fill( const LHCb::Particle*, const LHCb::Particle*, const std::string&, Tuples::Tuple& ) override;

private:
  BF init_bloom_filter( const LHCb::Particle::ConstVector& parts ) const;

  bool isInDecay( const LHCb::Particle::ConstVector daugs, const BF& bloom_filter,
                  const LHCb::Particle* particle ) const;

  const LHCb::MCParticle* originof( const LHCb::MCParticle* );

  const LHCb::MCParticle* getAncestor( const LHCb::MCParticle*, bool& );

  const LHCb::MCParticle* getAncestorDght( const LHCb::MCParticle* );

  void FullTrackAncestry( const LHCb::MCParticle* mcp, std::vector<int>& keys, std::vector<int>& ids );

  IParticleDescendants* m_descend = nullptr;

  std::vector<IParticle2MCAssociator*> m_p2mcAssocs;

  Gaudi::Property<bool> m_removeSignalDaughters{
      this, "removeSignalDaughters", false,
      "If true, removes signal daughter tracks beforehand and does not write information for them."};
  Gaudi::Property<std::string> m_trackPrefix{
      this, "trackPrefix", "Tr_",
      "Variable prefix that should be used instead of <head>"}; // <head> does not really fit here
  Gaudi::Property<std::string> m_ParticlePath{this, "ParticlePath", "/Event/Phys/StdAllNoPIDsPions/Particles",
                                              "Path to all tracks"};
  Gaudi::Property<int>         m_writeCompleteTrackAncestry{
      this, "writeCompleteTrackAncestry", 0,
      "Maximum number of \"mother\"-steps to reach beginning of decay"}; // Only active if > 0, Note: this is very
                                                                                 // verbose
  Gaudi::Property<std::vector<std::string>> m_p2mcAssocTypes{
      this, "IP2MCPAssociatorTypes", {"DaVinciSmartAssociator", "MCMatchObjP2MCRelator"}};
};

#endif // MCTUPLETOOLHIERARCHYOTHERTRACKS_H
