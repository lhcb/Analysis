/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from Gaudi
#include <Event/MCHeader.h>
#include <Event/MCVertex.h>
#include <GaudiAlg/Tuple.h>
#include <GaudiAlg/TupleObj.h>

// local
#include "TupleToolPV2MC.h"

#include <Kernel/IPV2MC.h>

// Relations
#include <Relations/IRelation.h>
#include <Relations/RelationWeighted1D.h>
#include <Relations/RelationWeighted2D.h>

using std::vector;

//-----------------------------------------------------------------------------
// Implementation file for class : TupleToolPV2MC
//
// 2018-05-09 - Michael Alexander
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( TupleToolPV2MC )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TupleToolPV2MC::TupleToolPV2MC( const std::string& type, const std::string& name, const IInterface* parent )
    : TupleToolBase( type, name, parent ) {
  declareInterface<IEventTupleTool>( this );
}

StatusCode TupleToolPV2MC::initialize() {
  const StatusCode sc = TupleToolBase::initialize();
  if ( sc.isFailure() ) return StatusCode::FAILURE;
  m_pv2mc = tool<IPV2MC>( m_pv2mctooltype );
  return sc;
}

//=============================================================================
StatusCode TupleToolPV2MC::fill( Tuples::Tuple& tuple ) {
  const std::string prefix = fullName();

  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "getting MCHeader" << endmsg;
  if ( !exist<LHCb::MCHeader>( LHCb::MCHeaderLocation::Default ) ) {
    Warning( "No MCHeader at " + LHCb::MCHeaderLocation::Default, StatusCode::SUCCESS, 1 )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return StatusCode::SUCCESS;
  }
  const LHCb::MCHeader*         mch = get<LHCb::MCHeader>( LHCb::MCHeaderLocation::Default );
  vector<const LHCb::MCVertex*> mcpvs;
  for ( SmartRefVector<LHCb::MCVertex>::const_iterator i = mch->primaryVertices().begin();
        i != mch->primaryVertices().end(); ++i ) {
    if ( ( *i )->isPrimary() ) mcpvs.push_back( &( **i ) );
  }

  const LHCb::RecVertices* pvs = get<LHCb::RecVertices>( m_pvslocation );
  if ( !pvs ) { return StatusCode::FAILURE; } // RETURN

  const IPV2MC::PV2MC*  pv2mctable = m_pv2mc->pv2MC();
  vector<vector<int>>   mcpvindices;
  vector<vector<int>>   matchtracks;
  vector<int>           nmatches;
  vector<int>           nmatchtrackstot;
  vector<float>         fracmatchtot;
  vector<vector<float>> matchfracs;
  vector<vector<float>> matchchi2s;
  vector<vector<float>> matchdists;

  for ( auto& recpv : *pvs ) {
    int  pvntracks   = recpv->tracks().size();
    auto pvrelations = pv2mctable->relations( recpv );
    // match by position in case match by pointer fails.
    if ( pvrelations.empty() ) {
      for ( auto& relation : pv2mctable->relations() ) {
        if ( relation.from()->position() == recpv->position() ) {
          pvrelations = pv2mctable->relations( relation.from() );
          break;
        }
      }
    }
    nmatches.push_back( pvrelations.size() );
    int           trackstot = 0;
    vector<int>   pv_mcpvindices;
    vector<int>   pv_matchtracks;
    vector<float> pv_matchfracs;
    vector<float> pv_matchchi2s;
    vector<float> pv_matchdists;
    for ( auto imatch = pvrelations.rbegin(); imatch != pvrelations.rend(); ++imatch ) {
      auto&           match = *imatch;
      LHCb::MCVertex* mcpv  = match.to();
      pv_mcpvindices.push_back( find( mcpvs.begin(), mcpvs.end(), mcpv ) - mcpvs.begin() );
      pv_matchtracks.push_back( match.weight().first );
      trackstot += match.weight().first;
      pv_matchfracs.push_back( float( match.weight().first ) / pvntracks );
      pv_matchchi2s.push_back( -match.weight().second );
      pv_matchdists.push_back( sqrt( ( recpv->position() - match.to()->position() ).mag2() ) );
    }
    mcpvindices.push_back( pv_mcpvindices );
    matchtracks.push_back( pv_matchtracks );
    matchfracs.push_back( pv_matchfracs );
    matchchi2s.push_back( pv_matchchi2s );
    matchdists.push_back( pv_matchdists );
    nmatchtrackstot.push_back( trackstot );
    fracmatchtot.push_back( float( trackstot ) / pvntracks );
  }

  unsigned int maxrelations = 10;
  // make sure all the rows are the same length.
  for ( unsigned int jpv = 0; jpv < nmatches.size(); ++jpv ) {
    for ( unsigned int kpv = nmatches[jpv]; kpv < maxrelations; ++kpv ) {
      mcpvindices[jpv].push_back( -1 );
      matchtracks[jpv].push_back( -1 );
      matchfracs[jpv].push_back( -1. );
      matchchi2s[jpv].push_back( -1. );
      matchdists[jpv].push_back( 0. );
    }
  }

  unsigned int maxnpvs = 30;
  tuple->farray( prefix + "RecPV_nMatches", nmatches, "nPVs", maxnpvs )
      .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  tuple->fmatrix( prefix + "RecPV_matchMCPVIndices", mcpvindices, nmatches.size(), maxrelations, "nPVs", maxnpvs )
      .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  tuple->fmatrix( prefix + "RecPV_matchNTracks", matchtracks, nmatches.size(), maxrelations, "nPVs", maxnpvs )
      .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  tuple->fmatrix( prefix + "RecPV_matchFracs", matchfracs, nmatches.size(), maxrelations, "nPVs", maxnpvs )
      .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  tuple->fmatrix( prefix + "RecPV_matchChi2s", matchchi2s, nmatches.size(), maxrelations, "nPVs", maxnpvs )
      .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  tuple->fmatrix( prefix + "RecPV_matchDistances", matchdists, nmatches.size(), maxrelations, "nPVs", maxnpvs )
      .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  tuple->farray( prefix + "RecPV_totMatchNTracks", nmatchtrackstot, "nPVs", maxnpvs )
      .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  tuple->farray( prefix + "RecPV_totMatchFrac", fracmatchtot, "nPVs", maxnpvs )
      .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

  return StatusCode::SUCCESS;
}
