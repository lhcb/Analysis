/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include <gsl/gsl_sys.h>

// from Gaudi
#include <Kernel/GetIDVAlgorithm.h>
#include <Kernel/IDVAlgorithm.h>

// local
#include "TupleToolMCPVAssociation.h"

#include <Event/MCHeader.h>
#include <Event/MCParticle.h>
#include <Event/Particle.h>
#include <GaudiAlg/Tuple.h>
#include <GaudiAlg/TupleObj.h>
//#include <MCInterfaces/IMCReconstructible.h>
//#include <MCInterfaces/IMCReconstructed.h>
#include <Event/MCVertex.h>
#include <Event/RecVertex.h>
#include <Kernel/IPV2MC.h>
#include <LoKi/PhysTypes.h>
#include <LoKi/Vertices1.h>

// Relations
#include <Relations/IRelation.h>
#include <Relations/RelationWeighted1D.h>
#include <Relations/RelationWeighted2D.h>

// Kernel
#include <Kernel/IParticle2MCAssociator.h>

using namespace LHCb;
//-----------------------------------------------------------------------------
// Implementation file for class : TupleToolMCPVAssociation
//
// 2018-05-09 - Michael Alexander & Adam Davis
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( TupleToolMCPVAssociation )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TupleToolMCPVAssociation::TupleToolMCPVAssociation( const std::string& type, const std::string& name,
                                                    const IInterface* parent )
    : TupleToolBase( type, name, parent ) {
  declareInterface<IParticleTupleTool>( this );
}

//=============================================================================
// initialize
//=============================================================================

StatusCode TupleToolMCPVAssociation::initialize() {
  const StatusCode sc = TupleToolBase::initialize();
  if ( sc.isFailure() ) return StatusCode::FAILURE;
  m_pv2mctool = tool<IPV2MC>( m_pv2mctooltype );
  // the MC associators
  m_p2mcAssocs.clear();
  for ( std::vector<std::string>::const_iterator iMCAss = m_p2mcAssocTypes.begin(); iMCAss != m_p2mcAssocTypes.end();
        ++iMCAss ) {
    m_p2mcAssocs.push_back( tool<IParticle2MCAssociator>( *iMCAss, this ) );
  }
  if ( m_p2mcAssocs.empty() ) { return Error( "No MC associators configured" ); }

  m_dva = Gaudi::Utils::getIDVAlgorithm( contextSvc(), this );
  if ( 0 == m_dva ) return Error( "Couldn't get parent DVAlgorithm", StatusCode::FAILURE );

  return sc;
}

//=============================================================================
// Fill
//=============================================================================
StatusCode TupleToolMCPVAssociation::fill( const LHCb::Particle*, const LHCb::Particle* P, const std::string& head,
                                           Tuples::Tuple& tuple ) {
  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "TupleToolMCPVAssociation::fill " << head << endmsg;
    debug() << "P = " << P << endmsg;
  }

  const std::string prefix = fullName( head );

  LHCb::RecVertex* recpv = (LHCb::RecVertex*)m_dva->bestVertex( P );

  const LHCb::MCParticle* mcp( NULL );

  // get the MC particle. (depends on signal decay association) this can only work for true decays...
  if ( P ) {
    for ( std::vector<IParticle2MCAssociator*>::const_iterator iMCAss = m_p2mcAssocs.begin();
          iMCAss != m_p2mcAssocs.end(); ++iMCAss ) {
      // first check to see if there is a particle association to the head. If so, that's easy!
      mcp = ( *iMCAss )->relatedMCP( P );
      if ( mcp ) break;
    }
  }
  // get the PV of the head of the chain.
  LHCb::MCVertex* head_vtx( NULL );
  if ( mcp ) {
    head_vtx = (LHCb::MCVertex*)mcp->primaryVertex();

    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "head_vtx filled. checking" << endmsg;
      debug() << "Checkpoint 1, got MC Particle" << mcp << endmsg;
      if ( head_vtx ) { debug() << "Checkpoint 2, got head vertex" << head_vtx << endmsg; }
    }
  } else if ( msgLevel( MSG::DEBUG ) )
    debug() << "Checkpoint 1, no MC particle found." << endmsg;

  // Get the PV<->MCPV relations table.
  const IPV2MC::PV2MC* pv2mctable  = m_pv2mctool->pv2MC();
  auto                 pvrelations = pv2mctable->relations( recpv );
  // match by position in case match by pointer fails.
  if ( pvrelations.empty() ) {
    for ( auto& relation : pv2mctable->relations() ) {
      if ( relation.from()->position() == recpv->position() ) {
        pvrelations = pv2mctable->relations( relation.from() );
        break;
      }
    }
  }

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "name " << prefix << endmsg;
    debug() << "recpv " << recpv << " " << recpv->position() << endmsg;
    if ( head_vtx )
      debug() << "mcpv " << head_vtx << " " << head_vtx->position() << endmsg;
    else
      debug() << "mcpv " << head_vtx << " "
              << "None" << endmsg;
    for ( auto& relation : pv2mctable->relations() ) {
      debug() << "from " << relation.from() << " " << relation.from()->position() << " to " << relation.to() << " "
              << relation.to()->position() << " weight.first " << relation.weight().first << " weight.second "
              << relation.weight().second << endmsg;
    }
  }

  // fill the tuple
  bool test( true );
  if ( NULL == head_vtx ) {
    test &= tuple->column( prefix + "_MCPV_X", 0. );
    test &= tuple->column( prefix + "_MCPV_Y", 0. );
    test &= tuple->column( prefix + "_MCPV_Z", 0. );
    test &= tuple->column( prefix + "_RecPVToMCPV_Chi2", -1. );
    // test &= tuple->column(prefix + "_MCPV_recible", -1);
    test &= tuple->column( prefix + "_RecPV_correct", -1 );
    test &= tuple->column( prefix + "_RecPV_nMatchTracks", 0 );
    test &= tuple->column( prefix + "_RecPV_matchFrac", 0.F );
  } else {
    test &= tuple->column( prefix + "_MCPV_X", head_vtx->position().x() );
    test &= tuple->column( prefix + "_MCPV_Y", head_vtx->position().y() );
    test &= tuple->column( prefix + "_MCPV_Z", head_vtx->position().z() );
    // not sure now to get this in the current framework.
    // test &= tuple->column( prefix + "_MCPV_recible", -1);
    if ( pvrelations.empty() ) {
      test &= tuple->column( prefix + "_RecPVToMCPV_Chi2", -1. );
      test &= tuple->column( prefix + "_RecPV_correct", -1 );
      test &= tuple->column( prefix + "_RecPV_nMatchTracks", 0 );
      test &= tuple->column( prefix + "_RecPV_matchFrac", 0.F );
    } else {
      auto            relation  = pvrelations.back();
      LHCb::MCVertex* matchmcpv = relation.to();
      int             ntracks   = relation.weight().first;
      float           matchfrac = float( ntracks ) / relation.from()->tracks().size();
      double          chi2      = -relation.weight().second;
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "matchpv " << matchmcpv << " " << matchmcpv->position() << " " << ntracks << " " << chi2 << " "
                << matchfrac << endmsg;
      test &= tuple->column( prefix + "_RecPVToMCPV_Chi2", chi2 );
      test &= tuple->column( prefix + "_RecPV_correct", int( matchmcpv == head_vtx ) );
      test &= tuple->column( prefix + "_RecPV_nMatchTracks", ntracks );
      test &= tuple->column( prefix + "_RecPV_matchFrac", matchfrac );
    }
  }

  return StatusCode( test );
}
