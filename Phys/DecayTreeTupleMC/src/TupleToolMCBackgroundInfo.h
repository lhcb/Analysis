/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: TupleToolMCBackgroundInfo.h,v 1.2 2010-01-26 15:39:26 rlambert Exp $
#ifndef JBOREL_TUPLETOOLMCBACKGROUNDINFO_H
#define JBOREL_TUPLETOOLMCBACKGROUNDINFO_H 1

// Include files
// from Gaudi
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "Kernel/IParticleTupleTool.h" // Interface

class IBackgroundCategory;

namespace LHCb {
  class Particle;
}

/** @class TupleToolMCBackgroundInfo TupleToolMCBackgroundInfo.h
 *
 * \brief Fill the info from IBackgroundCategory
 *
 * The column is only filled for composite particles.
 *
 * IBackgroundCategory controlled by property <b>IBackgroundCatagoryType</b>, default "BackgroundCategory".
 *
 * head_BKGCAT : Background category.
 *
 * \sa DecayTreeTuple
 *
 *  @author Jeremie Borel
 *  @date   2007-11-07
 */
class TupleToolMCBackgroundInfo : public TupleToolBase, virtual public IParticleTupleTool {

public:
  /// Standard constructor
  TupleToolMCBackgroundInfo( const std::string& type, const std::string& name, const IInterface* parent );

  virtual ~TupleToolMCBackgroundInfo(){}; ///< Destructor

  StatusCode initialize() override;

  StatusCode fill( const LHCb::Particle*, const LHCb::Particle*, const std::string&, Tuples::Tuple& ) override;

private:
  std::vector<std::string>          m_backCatTypes;
  std::vector<IBackgroundCategory*> m_bkgs;
};

#endif // JBOREL_TUPLETOOLMCBACKGROUNDINFO_H
