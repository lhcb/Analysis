/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// from Gaudi
#include "GaudiKernel/PhysicalConstants.h"
// local
#include "MCTupleToolHierarchyOtherTracks.h"

#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"

#include "Kernel/IDVAlgorithm.h"
#include <Kernel/GetIDVAlgorithm.h>

#include "Event/MCParticle.h"
#include "Event/Particle.h"

#include "Kernel/IParticle2MCAssociator.h"

using namespace LHCb;

//-----------------------------------------------------------------------------
// Implementation file for class : MCTupleToolHierarchyOtherTracks
//
// Author: Simon Akar, Vukan Jevtic, Basem Khanji
//-----------------------------------------------------------------------------

// Declaration of Factory
DECLARE_COMPONENT( MCTupleToolHierarchyOtherTracks )
//=============================================================================
//============================================================================
MCTupleToolHierarchyOtherTracks::MCTupleToolHierarchyOtherTracks( const std::string& type, const std::string& name,
                                                                  const IInterface* parent )
    : TupleToolBase( type, name, parent ) {
  declareInterface<IParticleTupleTool>( this );
}
//=============================================================================
// =============================================================================
StatusCode MCTupleToolHierarchyOtherTracks::initialize() {
  const StatusCode sc = TupleToolBase::initialize();
  if ( sc.isFailure() ) return sc;

  m_descend = tool<IParticleDescendants>( "ParticleDescendants", this );
  if ( !m_descend ) {
    fatal() << "Unable to retrieve ParticleDescendants tool " << endmsg;
    return StatusCode::FAILURE;
  }

  m_p2mcAssocs.clear();
  for ( const auto& iMCAss : m_p2mcAssocTypes ) {
    m_p2mcAssocs.push_back( tool<IParticle2MCAssociator>( iMCAss, this ) );
  }
  if ( m_p2mcAssocs.empty() ) { return Error( "No MC associators configured", StatusCode::FAILURE ); }

  if ( m_removeSignalDaughters ) { info() << "Configured to perform signal daughter removal" << endmsg; }

  return sc;
}

//=============================================================================
//=============================================================================
StatusCode MCTupleToolHierarchyOtherTracks::fill( const LHCb::Particle* mother, // mother
                                                  const LHCb::Particle* P,      // P
                                                  const std::string&,           // head
                                                  Tuples::Tuple& tuple ) {
  // MCTupleToolHierarchy by Simon Akar, stores ancestry info of every track,
  // including type of origin (e.g. OS fragmentation, OS decay, SS decay, underlying event)

  //------------------ Trivial checks ------------------
  // Check we have a mother
  if ( !mother ) return StatusCode::SUCCESS;
  if ( !( P->particleID().hasBottom() ) ) return StatusCode::SUCCESS;

  //----------------------------------------------------
  //----------------------------------------------------
  // Store the signal B daughter (to remove them from the loop
  // over all the particles in the container, if so configured)
  Particle::ConstVector signalDaughters = m_descend->descendants( mother );
  if ( signalDaughters.empty() ) return StatusCode::SUCCESS;

  LHCb::Particle::Range allparts;
  // Store all the particles (to loop over)
  if ( exist<LHCb::Particle::Range>( m_ParticlePath ) ) {
    allparts = get<LHCb::Particle::Range>( m_ParticlePath );
  } else {
    return Error( "Nothing found at " + m_ParticlePath, StatusCode::SUCCESS, 1 );
  }

  BF bloom_filter = init_bloom_filter( signalDaughters );

  //----------------------------------------------------
  //----------------------------------------------------
  // Define the containers needed for the information to store
  // For the signal B particle
  int  mBsig_id      = 99;    // B signal ID
  int  mBsig_key     = -9;    // ... track key
  int  mBsig_anc_id  = 99;    // ... Ancestor ID
  int  mBsig_anc_key = -9;    // ... Ancestor track key
  bool mBsig_hasOsc  = false; // ... has oscillated

  // For all the tracks
  std::vector<int>  allparts_keys;
  std::vector<int>  mcp_ids, mcp_keys;
  std::vector<int>  mcp_anc_ids, mcp_anc_keys;
  std::vector<int>  mcp_anc_dght_ids, mcp_anc_dght_keys;
  std::vector<int>  mcp_mother_ids, mcp_mother_keys;
  std::vector<int>  mcp_gd_mother_ids, mcp_gd_mother_keys;
  std::vector<int>  mcp_gd_gd_mother_ids, mcp_gd_gd_mother_keys;
  std::vector<bool> mcp_fromSignal; // MCParticle::fromSignal() flag

  // Origin vertex info
  std::vector<bool>   mcp_OV_assocToSignal_PV_arr;
  std::vector<bool>   mcp_OV_isPrimary_arr;
  std::vector<int>    mcp_OV_products_arr;
  std::vector<int>    mcp_OV_type_arr;
  std::vector<int>    mcp_OV_key_arr;
  std::vector<double> mcp_OV_Xs;
  std::vector<double> mcp_OV_Ys;
  std::vector<double> mcp_OV_Zs;

  std::vector<std::vector<int>> complete_ancestry_ids, complete_ancestry_keys;

  // Flags describing track origin
  std::vector<int> originFlags, bFlags;

  //----------------------------------------------------
  // Get the MC information for the mother
  const LHCb::MCParticle* mBsig          = nullptr;
  const LHCb::MCParticle* mBsig_ancestor = nullptr;

  const LHCb::MCVertex* mBsig_OV = nullptr;

  // Retrieve MC particle associated to signal B
  for ( const auto& iMCAss : m_p2mcAssocs ) {
    mBsig = iMCAss->relatedMCP( mother );
    if ( mBsig ) {
      // Store the B info
      mBsig_id     = mBsig->particleID().pid();
      mBsig_key    = mBsig->key();
      mBsig_hasOsc = mBsig->hasOscillated();

      // Get the ancestor of the signal B
      {
        bool dummy     = false;
        mBsig_ancestor = getAncestor( mBsig, dummy );
      }
      if ( mBsig_ancestor ) {
        // Store the B ancestor info
        mBsig_anc_id  = mBsig_ancestor->particleID().pid();
        mBsig_anc_key = mBsig_ancestor->key();
        mBsig_OV      = mBsig_ancestor->originVertex();
      }
      break;
    }
  }

  //----------------------------------------------------
  //----------------------------------------------------
  // Make the loop over all the particles in the container
  for ( const auto& ite_P : allparts ) {
    //-----------------------------------------
    //-----------------------------------------
    // Get the pointer to the particle
    const Particle* thisP = ite_P;

    const LHCb::ProtoParticle* proto = thisP->proto();
    if ( !proto ) continue;

    const LHCb::Track* track = proto->track();
    if ( !track ) continue;

    // origin vertex info
    bool   mcp_OV_assocToSignalPV = false;                                 // Track ancestor is associated to signal pv
    bool   mcp_OV_isPrimary       = false;                                 // Ancestore origin vertex is primary
    int    mcp_OV_products        = 0;                                     // Number of OV products
    int    mcp_OV_type            = LHCb::MCVertex::MCVertexType::Unknown; // OV type
    int    mcp_OV_key             = -1;
    double mcp_OV_X               = -999.0; // OV position
    double mcp_OV_Y               = -999.0;
    double mcp_OV_Z               = -999.0;

    // Local containers to the particles information
    int mcp_id           = 99;
    int mcp_key          = -9;
    int mcp_anc_id       = 99;
    int mcp_anc_key      = -9;
    int mcp_anc_dght_id  = 99;
    int mcp_anc_dght_key = -9;

    int mcp_mom_id        = 99;
    int mcp_mom_key       = -9;
    int mcp_gd_mom_id     = 99;
    int mcp_gd_mom_key    = -9;
    int mcp_gd_gd_mom_id  = 99;
    int mcp_gd_gd_mom_key = -9;

    //-----------------------------------------
    //-----------------------------------------
    // Check if this particle is in the list of signal particles
    // if it is, go on only for other particles
    if ( m_removeSignalDaughters && isInDecay( signalDaughters, bloom_filter, ite_P ) ) continue;

    allparts_keys.push_back( thisP->key() );

    //-----------------------------------------
    //-----------------------------------------
    // Retrieve the MC information and store it
    const LHCb::MCParticle* mcp                 = nullptr;
    const LHCb::MCParticle* mcp_ancestor        = nullptr;
    const LHCb::MCParticle* mcp_ancestor_daught = nullptr;
    const LHCb::MCParticle* mcpmom              = nullptr;
    const LHCb::MCParticle* mcpmom_mom          = nullptr;
    const LHCb::MCParticle* mcpmom_mom_mom      = nullptr;

    const LHCb::MCVertex* mcTrack_OV = nullptr;

    bool hasUnexcitedBmother = false;
    for ( const auto& iMCAss : m_p2mcAssocs ) {
      mcp = iMCAss->relatedMCP( thisP );

      if ( mcp ) {
        //-----------------------------------------
        // Store the particle info
        mcp_id  = mcp->particleID().pid();
        mcp_key = mcp->key();
        // Store the particle mother info
        mcpmom = mcp->mother();

        if ( mcpmom ) {
          mcp_mom_id  = mcpmom->particleID().pid();
          mcp_mom_key = mcpmom->key();
          // Store the particle gd mother info
          mcpmom_mom = mcpmom->mother();

          if ( mcpmom_mom ) {
            mcp_gd_mom_id  = mcpmom_mom->particleID().pid();
            mcp_gd_mom_key = mcpmom_mom->key();
            // Store the particle gd gd mother info
            mcpmom_mom_mom = mcpmom_mom->mother();

            if ( mcpmom_mom_mom ) {
              mcp_gd_gd_mom_id  = mcpmom_mom_mom->particleID().pid();
              mcp_gd_gd_mom_key = mcpmom_mom_mom->key();
            }
          }
        }

        //-----------------------------------------
        // Get the particle ancestor
        mcp_ancestor = getAncestor( mcp, hasUnexcitedBmother );

        if ( mcp_ancestor ) {
          // Store the particle ancestor info
          mcp_anc_id  = mcp_ancestor->particleID().pid();
          mcp_anc_key = mcp_ancestor->key();

          // Store origin vertex info
          if ( mcp_ancestor->originVertex() ) {
            mcTrack_OV             = mcp_ancestor->originVertex();
            mcp_OV_assocToSignalPV = mcTrack_OV == mBsig_OV && mBsig_OV != nullptr && mcTrack_OV != nullptr;
            mcp_OV_isPrimary       = mcTrack_OV->isPrimary();
            mcp_OV_type            = mcTrack_OV->type();
            mcp_OV_products        = mcTrack_OV->products().size();
            mcp_OV_key             = mcTrack_OV->key();
            mcp_OV_X               = mcTrack_OV->position().x();
            mcp_OV_Y               = mcTrack_OV->position().y();
            mcp_OV_Z               = mcTrack_OV->position().z();
          }
        }

        //-----------------------------------------
        // Get the particle ancestor daughter
        mcp_ancestor_daught = getAncestorDght( mcp );

        if ( mcp_ancestor_daught ) {
          // Store the particle ancestor daughter info
          mcp_anc_dght_id  = mcp_ancestor_daught->particleID().pid();
          mcp_anc_dght_key = mcp_ancestor_daught->key();
        }
        break;
      }
    } // end of loop over the particle to mc associator

    if ( m_writeCompleteTrackAncestry > 0 ) {
      complete_ancestry_keys.emplace_back();
      complete_ancestry_ids.emplace_back();
      FullTrackAncestry( mcp, complete_ancestry_keys.back(), complete_ancestry_ids.back() );
    }

    //-----------------------------------------
    //-----------------------------------------
    // Now add the flags to ease the selection
    // of the fragmentation particles

    // Flag to tell if the particle is coming from the B (-1)
    // Should never happen if signal daughters are removed
    int                     bFlag          = 0;
    const LHCb::MCParticle* mcp_ancestor_b = nullptr;
    if ( mcp ) {
      mcp_ancestor_b = originof( mcp );
      mcp_fromSignal.push_back( mcp->fromSignal() );

      if ( mcp_ancestor_b ) {
        if ( mcp_ancestor_b->particleID().hasBottom() ) {

          bFlag = 1;
          // Check if ancestor is the B signal itself
          if ( ( mcp_ancestor_b->particleID().pid() == mBsig_id ) && ( mcp_ancestor_b->key() == mBsig_key ) ) {
            bFlag = -1;
          }
        }
      }
    } else {
      mcp_fromSignal.push_back( false );
    }

    //-----------------------------------------
    //-----------------------------------------
    // Flag the type of particle (fragmentation, underlying event, ...)
    int oriFlag = -1; // Error code = no related MC particle, could be ghost
    if ( mcp_ancestor != nullptr ) {
      if ( mcp_ancestor->particleID().hasBottom() ) {
        // Particle comes from a b quark
        if ( mcp_anc_key == mBsig_anc_key ) {
          // Particle comes from the B signal
          // (b quark is the one from the signal B quark)
          if ( isInDecay( signalDaughters, bloom_filter, ite_P ) ) {
            // Track is signal track
            oriFlag = 0;
          } else {
            // SS fragmentation
            oriFlag = 1;
          }
        } else {
          // Track comes from opposite side b hadron
          if ( mcp_ancestor_daught != nullptr ) {
            if ( mcp_ancestor_daught->particleID().hasBottom() ) {
              // Some kind of b hadron is involved in track production
              if ( hasUnexcitedBmother ) {
                // OS decay: Particle comes from decay of B0, B+, Bs or Bc+
                oriFlag = 2;
              } else {
                // Particle comes formally from a decay of a OS b hadron resonance like B1(H)0 or B*0+
                oriFlag = 3;
              }
            } else {
              // OS Fragmentation
              oriFlag = 4;
            }
          } // else error flag
        }
      } else {
        // Particle is prompt or unrelated to a b frag and decay
        oriFlag = 5;
      }
    }
    if ( !mcp_OV_assocToSignalPV && oriFlag != -1 ) {
      // The track does not come from the same PV as the signal
      // Can still come from a b decay, e.g. 102, 103, 104, although that is rare. 105 is very likely
      oriFlag += 100;
    }

    //-----------------------------------------
    //-----------------------------------------
    //-----------------------------------------
    //-----------------------------------------
    // Store everything now
    // starting by the mc info
    mcp_ids.push_back( mcp_id );
    mcp_mother_ids.push_back( mcp_mom_id );
    mcp_gd_mother_ids.push_back( mcp_gd_mom_id );
    mcp_gd_gd_mother_ids.push_back( mcp_gd_gd_mom_id );

    mcp_keys.push_back( mcp_key );
    mcp_mother_keys.push_back( mcp_mom_key );
    mcp_gd_mother_keys.push_back( mcp_gd_mom_key );
    mcp_gd_gd_mother_keys.push_back( mcp_gd_gd_mom_key );

    mcp_anc_ids.push_back( mcp_anc_id );
    mcp_anc_keys.push_back( mcp_anc_key );

    mcp_anc_dght_ids.push_back( mcp_anc_dght_id );
    mcp_anc_dght_keys.push_back( mcp_anc_dght_key );

    // Store the flags value
    bFlags.push_back( bFlag );
    originFlags.push_back( oriFlag );

    // Store origin vertex info
    mcp_OV_assocToSignal_PV_arr.push_back( mcp_OV_assocToSignalPV );
    mcp_OV_isPrimary_arr.push_back( mcp_OV_isPrimary );
    mcp_OV_type_arr.push_back( mcp_OV_type );
    mcp_OV_key_arr.push_back( mcp_OV_key );
    mcp_OV_products_arr.push_back( mcp_OV_products );
    mcp_OV_Xs.push_back( mcp_OV_X );
    mcp_OV_Ys.push_back( mcp_OV_Y );
    mcp_OV_Zs.push_back( mcp_OV_Z );

  } // end of the loop over the particles

  //----------------------------------------------------
  //----------------------------------------------------
  // Fill the tuple now
  // info related to the B signal
  bool success = true;
  success &= tuple->column( m_trackPrefix + "MC_Bsignal_ID", mBsig_id );
  success &= tuple->column( m_trackPrefix + "MC_Bsignal_Key", mBsig_key );
  success &= tuple->column( m_trackPrefix + "MC_Bsignal_OSCIL", mBsig_hasOsc );
  success &= tuple->column( m_trackPrefix + "MC_ANCESTOR_Bsignal_ID", mBsig_anc_id );
  success &= tuple->column( m_trackPrefix + "MC_ANCESTOR_Bsignal_Key", mBsig_anc_key );

  // info related to the mc particle ancestor
  success &= tuple->farray( m_trackPrefix + "ANCESTOR_ID", mcp_anc_ids, "N_MC", 500 );
  success &= tuple->farray( m_trackPrefix + "ANCESTOR_Key", mcp_anc_keys, "N_MC", 500 );
  success &= tuple->farray( m_trackPrefix + "ANCESTOR_DAUGHTER_ID", mcp_anc_dght_ids, "N_MC", 500 );
  success &= tuple->farray( m_trackPrefix + "ANCESTOR_DAUGHTER_Key", mcp_anc_dght_keys, "N_MC", 500 );

  // info related to the track flags
  success &= tuple->farray( m_trackPrefix + "ORIG_FLAGS", originFlags, "N_MC", 500 );
  success &= tuple->farray( m_trackPrefix + "B_FLAGS", bFlags, "N_MC", 500 );
  success &= tuple->farray( m_trackPrefix + "fromSignal", mcp_fromSignal, "N_MC", 500 );

  // info related to the mc particle (mother, gd mom, gd gd mom)
  success &= tuple->farray( m_trackPrefix + "MC_ID", mcp_ids, "N_MC", 500 );
  success &= tuple->farray( m_trackPrefix + "MC_Key", mcp_keys, "N_MC", 500 );
  success &= tuple->farray( m_trackPrefix + "MC_MOTHER_ID", mcp_mother_ids, "N_MC", 500 );
  success &= tuple->farray( m_trackPrefix + "MC_MOTHER_Key", mcp_mother_keys, "N_MC", 500 );
  success &= tuple->farray( m_trackPrefix + "MC_ALL_Key", allparts_keys, "N_MC", 500 );

  if ( isVerbose() ) {
    // (Grand) grand mother information
    success &= tuple->farray( m_trackPrefix + "MC_GD_MOTHER_ID", mcp_gd_mother_ids, "N_MC", 500 );
    success &= tuple->farray( m_trackPrefix + "MC_GD_MOTHER_Key", mcp_gd_mother_keys, "N_MC", 500 );
    success &= tuple->farray( m_trackPrefix + "MC_GD_GD_MOTHER_ID", mcp_gd_gd_mother_ids, "N_MC", 500 );
    success &= tuple->farray( m_trackPrefix + "MC_GD_GD_MOTHER_Key", mcp_gd_gd_mother_keys, "N_MC", 500 );
  }

  // info related to origin vertex
  success &= tuple->farray( m_trackPrefix + "MC_OV_AssocToSignalPV", mcp_OV_assocToSignal_PV_arr, "N_MC", 500 );
  success &= tuple->farray( m_trackPrefix + "MC_OV_isPrimary", mcp_OV_isPrimary_arr, "N_MC", 500 );
  success &= tuple->farray( m_trackPrefix + "MC_OV_types", mcp_OV_type_arr, "N_MC", 500 );
  success &= tuple->farray( m_trackPrefix + "MC_OV_Key", mcp_OV_key_arr, "N_MC", 500 );
  success &= tuple->farray( m_trackPrefix + "MC_OV_products", mcp_OV_products_arr, "N_MC", 500 );
  success &= tuple->farray( m_trackPrefix + "MC_OV_X", mcp_OV_Xs, "N_MC", 500 );
  success &= tuple->farray( m_trackPrefix + "MC_OV_Y", mcp_OV_Ys, "N_MC", 500 );
  success &= tuple->farray( m_trackPrefix + "MC_OV_Z", mcp_OV_Zs, "N_MC", 500 );

  if ( m_writeCompleteTrackAncestry > 0 ) {
    for ( auto& caid : complete_ancestry_ids ) { caid.resize( m_writeCompleteTrackAncestry, 99 ); }
    for ( auto& cakeys : complete_ancestry_keys ) { cakeys.resize( m_writeCompleteTrackAncestry, -1 ); }
    success &= tuple->fmatrix( m_trackPrefix + "ANCESTRY_ID", complete_ancestry_ids, complete_ancestry_ids.size(),
                               m_writeCompleteTrackAncestry, "N_MC", 500 );
    success &= tuple->fmatrix( m_trackPrefix + "ANCESTRY_Key", complete_ancestry_keys, complete_ancestry_keys.size(),
                               m_writeCompleteTrackAncestry, "N_MC", 500 );
  }

  return StatusCode( success );
}

BF MCTupleToolHierarchyOtherTracks::init_bloom_filter( const LHCb::Particle::ConstVector& parts ) const {
  BF ret;
  for ( const auto* part : parts ) {
    if ( !part->isBasicParticle() ) continue;
    const LHCb::Track* tr = part->proto()->track();
    ret.insert( tr );
  }
  return ret;
}

//==================================================================================
//==================================================================================
bool MCTupleToolHierarchyOtherTracks::isInDecay( const LHCb::Particle::ConstVector daugs, const BF& daughterFP,
                                                 const LHCb::Particle* particle ) const {
  const LHCb::Track* p_tr = particle->proto()->track();

  BF this_particle;
  this_particle.insert( p_tr );
  if ( ( daughterFP & this_particle ).empty() ) {
    // Particle is not signal
    return false;
  } // else might still not be signal, but detailed check is needed

  for ( const auto& it_part : daugs ) {
    if ( !it_part->isBasicParticle() ) continue;

    const LHCb::Track* Track_decay = it_part->proto()->track();
    if ( Track_decay && p_tr ) {
      if ( Track_decay == p_tr ) { return true; }
    }
  }
  return false;
}

//==================================================================================
//==================================================================================
const MCParticle* MCTupleToolHierarchyOtherTracks::originof( const LHCb::MCParticle* mcp ) {
  const auto* mcp_mom = mcp->mother();
  if ( ( !mcp_mom ) || mcp->particleID().hasBottom() ) {
    return mcp;
  } else {
    return originof( mcp_mom );
  }
}

//==================================================================================
const MCParticle* MCTupleToolHierarchyOtherTracks::getAncestor( const LHCb::MCParticle* mcp,
                                                                bool&                   hasUnexcitedBmother ) {
  const auto* ancestor = const_cast<LHCb::MCParticle*>( mcp );
  while ( ancestor->mother() ) {
    ancestor = const_cast<LHCb::MCParticle*>( ancestor->mother() );
    if ( int pid = std::abs( ancestor->particleID().pid() ); pid == 511 || pid == 521 || pid == 531 || pid == 541 ) {
      // can be eligible for "OS decay" instead of "OS fragmentation"
      hasUnexcitedBmother |= true;
    }
  }
  return ancestor;
}

void MCTupleToolHierarchyOtherTracks::FullTrackAncestry( const LHCb::MCParticle* mcp, std::vector<int>& keys,
                                                         std::vector<int>& ids ) {
  if ( mcp == nullptr ) {
    ids.push_back( 99 );
    keys.push_back( -1 );
  } else {
    const auto* ancestor = const_cast<LHCb::MCParticle*>( mcp );
    ids.push_back( ancestor->particleID().pid() );
    keys.push_back( ancestor->key() );
    while ( ancestor->mother() ) {
      ancestor = const_cast<LHCb::MCParticle*>( ancestor->mother() );
      ids.push_back( ancestor->particleID().pid() );
      keys.push_back( ancestor->key() );
    }
  }
}

//==================================================================================
//==================================================================================
const MCParticle* MCTupleToolHierarchyOtherTracks::getAncestorDght( const LHCb::MCParticle* mcp ) {
  MCParticle* ancestor     = const_cast<LHCb::MCParticle*>( mcp );
  MCParticle* ancestorDght = const_cast<LHCb::MCParticle*>( mcp );
  while ( ancestor->mother() ) {
    ancestorDght = ancestor;
    ancestor     = const_cast<LHCb::MCParticle*>( ancestor->mother() );
  }
  return ancestorDght;
}
