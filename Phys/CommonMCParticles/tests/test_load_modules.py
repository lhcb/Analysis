#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#$Id: test_load_modules.py,v 1.1 2010-04-22 14:44:53 jpalac Exp $
'''
Test loading various CommonMCParticles in the Analysis environment. Needs to be extended.
'''
__author__ = 'Juan Palacios juan.palacios@nikhef.nl'

import sys
sys.path.append('../python')


def test_load_all():
    import CommonMCParticles


if '__main__' == __name__:

    import sys
    import functools

    test_names = list(filter(lambda k: k.count('test_') > 0, locals().keys()))
    __tests = list(filter(lambda x: x[0] in test_names, locals().items()))

    message = ''
    summary = '\n'
    length = len(max(test_names, key=len)) + 2

    for test in __tests:
        try:
            test[1]()
            message = 'PASS'
        except:
            message = "FAIL"
        summary += test[0].ljust(length) + ':' + message.rjust(10) + '\n'

    if summary.count('FAIL') > 0:
        message = 'FAIL'
        wr = sys.stderr.write
    else:
        message = 'PASS'
        wr = sys.stdout.write

    summary += 'Global'.ljust(length) + ':' + message.rjust(10) + '\n\n'
    wr(summary)
