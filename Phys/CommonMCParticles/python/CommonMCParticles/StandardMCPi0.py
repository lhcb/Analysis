#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Configuration file for 'Standard MC Pi0'
"""
from __future__ import print_function
__author__ = "Conor Fitzpatrick"
__version__ = "1.0"
# =============================================================================
__all__ = ('StdMCMergedPi0', 'StdMCResolvedPi0', 'locations')
# =============================================================================
from Gaudi.Configuration import *
from Configurables import FilterDesktop

from CommonParticles.Utils import *
from CommonParticles import StdLooseMergedPi0, StdLooseResolvedPi0

matchpi0 = "(mcMatch('pi0'))"
locations = {}
## create the algorithm

StdMCMergedPi0 = FilterDesktop(
    'StdMCMergedPi0',
    Inputs=["Phys/StdLooseMergedPi0/Particles"],
    Code=matchpi0,
    Preambulo=[
        "from LoKiPhysMC.decorators import *", "from PartProp.Nodes import CC"
    ])
## configure Data-On-Demand service
locations.update(updateDoD(StdMCMergedPi0))

StdMCResolvedPi0 = FilterDesktop(
    'StdMCResolvedPi0',
    Inputs=["Phys/StdLooseResolvedPi0/Particles"],
    Code=matchpi0,
    Preambulo=[
        "from LoKiPhysMC.decorators import *", "from PartProp.Nodes import CC"
    ])
## configure Data-On-Demand service
locations.update(updateDoD(StdMCResolvedPi0))

## ============================================================================
if '__main__' == __name__:

    print(__doc__)
    print(__author__)
    print(__version__)
    print(locationsDoD(locations))

# =============================================================================
# The END
# =============================================================================
