#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Configuration file for 'Standard MC Photons'
"""
from __future__ import print_function
__author__ = "Conor Fitzpatrick"
__version__ = "1.0"
# =============================================================================
__all__ = ('StdMCPhotons', 'locations')
# =============================================================================
from Gaudi.Configuration import *
from Configurables import FilterDesktop

from CommonParticles.Utils import *
from CommonParticles import StdLoosePhotons
## create the algorithm
algorithm = FilterDesktop(
    'StdMCPhotons',
    Inputs=["Phys/StdLooseAllPhotons/Particles"],
    Code="(mcMatch('gamma'))",
    Preambulo=[
        "from LoKiPhysMC.decorators import *", "from PartProp.Nodes import CC"
    ])

## configure Data-On-Demand service
locations = updateDoD(algorithm)

## finally: define the symbol
StdMCPhotons = algorithm

## ============================================================================
if '__main__' == __name__:

    print(__doc__)
    print(__author__)
    print(__version__)
    print(locationsDoD(locations))

# =============================================================================
# The END
# =============================================================================
