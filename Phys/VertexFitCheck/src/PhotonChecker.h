/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: PhotonChecker.h,v 1.2 2007-10-17 07:27:55 jpalac Exp $
#ifndef PHOTONCHECKER_H
#define PHOTONCHECKER_H 1

// Include files
// from DaVinci, this is a specialized GaudiAlgorithm
#include "Kernel/DaVinciTupleAlgorithm.h"

#include "Kernel/Particle2MCLinker.h"

using namespace LHCb;

/** @class PhotonChecker PhotonChecker.h
 *
 *  PhotonChecker is an algorithm to check
 *  the resolution and pull distributions
 *  of (x, y, E) of calorimeter clusters
 *  assocaited with photon particles
 *  using MC truth.
 *
 *  @author Yuehong Xie
 *  @date   2006-5-25
 */
class PhotonChecker : public DaVinciTupleAlgorithm {

public:
  /// Standard constructor
  PhotonChecker( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~PhotonChecker(); ///< Destructor

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

private:
  class IsHypo {
  public:
    /// constructor
    IsHypo( const CaloHypo::Hypothesis& hypo ) : m_hypo( hypo ){};
    /// functor interface
    bool operator()( const CaloHypo* hypo ) const { return 0 != hypo && m_hypo == hypo->hypothesis(); }
    typedef const CaloHypo* argument_type;
    typedef bool            result_type;

  private:
    IsHypo();

  private:
    CaloHypo::Hypothesis m_hypo;
  };

private:
  Particle2MCLinker* m_pLinker;
  std::string        m_particlePath;

  StatusCode getPhotonParameter( const LHCb::Particle& part, double& zg, Gaudi::Vector3& para,
                                 Gaudi::SymMatrix3x3& cov ) const;
};
#endif
