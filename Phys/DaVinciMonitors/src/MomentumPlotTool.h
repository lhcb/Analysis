/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: MomentumPlotTool.h,v 1.2 2009-02-15 22:03:51 jonrob Exp $
#ifndef DAVINCIMONITORS_MomentumPlotTool_H
#define DAVINCIMONITORS_MomentumPlotTool_H 1

// base class
#include "BasePlotTool.h"

// boost
#include "boost/lexical_cast.hpp"

/** @class MomentumPlotTool MomentumPlotTool.h
 *
 *  Fills momentum plots
 *
 *  @author Patrick Koppenburg
 *  @date   2008-12-05
 */
class MomentumPlotTool : public BasePlotTool {

public:
  /// Standard constructor
  MomentumPlotTool( const std::string& type, const std::string& name, const IInterface* parent );

  ~MomentumPlotTool(); ///< Destructor

protected:
  /// Fill final state PID plots - overwrites BasePlotTools method
  StatusCode fillImpl( const LHCb::Particle* p, const std::string& trailer = "" ) override;
};

#endif // DAVINCIMONITORS_MomentumPlotTool_H
