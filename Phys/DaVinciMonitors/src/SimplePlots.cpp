/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "SimplePlots.h"

//-----------------------------------------------------------------------------
// Implementation file for class : SimplePlots
//
// 2004-10-22 : Patrick KOPPENBURG
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SimplePlots )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
SimplePlots::SimplePlots( const std::string& name, ISvcLocator* pSvcLocator )
    : DaVinciAlgorithm( name, pSvcLocator ), m_plots( 0 ) {
  declareProperty( "PlotTool", m_plotTool = "LoKi::Hybrid::PlotTool//Plots" );
}
//=============================================================================
// Destructor
//=============================================================================
SimplePlots::~SimplePlots() {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode SimplePlots::initialize() {
  StatusCode sc = DaVinciAlgorithm::initialize();
  if ( !sc ) return sc;

  m_plots = tool<IPlotTool>( m_plotTool, this );
  if ( !m_plots ) {
    err() << "Unable to retrieve Plot Tool " << m_plotTool << endmsg;
    return StatusCode::FAILURE;
  }

  return m_plots->setPath( name() );
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode SimplePlots::execute() {

  debug() << "==> Execute" << endmsg;

  // code goes here
  const LHCb::Particle::ConstVector PP = this->i_particles();
  StatusCode                        sc = m_plots->fillPlots( PP );
  if ( !sc ) {
    err() << "Error from PlotTool" << endmsg;
    return sc;
  }

  setFilterPassed( true ); // Mandatory. Set to true if event is accepted.
  return StatusCode::SUCCESS;
}
