/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "MomentumPlotTool.h"

using namespace Gaudi::Units;
//-----------------------------------------------------------------------------
// Implementation file for class : MomentumPlotTool
//
// 2008-12-05 : Patrick Koppenburg
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( MomentumPlotTool )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
MomentumPlotTool::MomentumPlotTool( const std::string& type, const std::string& name, const IInterface* parent )
    : BasePlotTool( type, name, parent ) {}

//=============================================================================
// Standard destructor
//=============================================================================
MomentumPlotTool::~MomentumPlotTool() {}

//=============================================================================
// Daughter plots - just mass plots
//=============================================================================
StatusCode MomentumPlotTool::fillImpl( const LHCb::Particle* p, const std::string& trailer ) {
  const LHCb::ParticleProperty* pp = particleProperty( p->particleID() );
  if ( !pp ) return StatusCode::SUCCESS;

  plot1D( p->p() / Gaudi::Units::GeV, histoName( "P", pp, trailer ), "Ptot of " + pp->name() + " in " + trailer, 0, 100,
          nBins() );
  plot1D( p->pt() / Gaudi::Units::GeV, histoName( "Pt", pp, trailer ), "Pt of " + pp->name() + " in " + trailer, 0, 10,
          nBins() );

  return StatusCode::SUCCESS;
}
