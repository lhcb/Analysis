/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: ANNPIDTool.h,v 1.2 2009-02-15 22:03:51 jonrob Exp $
#ifndef DAVINCIMONITORS_ANNPIDTool_H
#define DAVINCIMONITORS_ANNPIDTool_H 1

// base class
#include "BasePlotTool.h"

// boost
#include "boost/lexical_cast.hpp"

/** @class ANNPIDTool ANNPIDTool.h
 *
 *  Fills ANN PID Training Tuple
 *
 *  @author Chris Jones
 *  @date   09-02-2011
 */
class ANNPIDTool : public BasePlotTool {

public:
  /// Standard constructor
  ANNPIDTool( const std::string& type, const std::string& name, const IInterface* parent );

  ~ANNPIDTool(); ///< Destructor

protected:
  /// Fill final state PID plots - overwrites BasePlotTools method
  StatusCode fillImpl( const LHCb::Particle* p, const std::string& trailer = "" ) override;
};

#endif // DAVINCIMONITORS_ANNPIDTool_H
