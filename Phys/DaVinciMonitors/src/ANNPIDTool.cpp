/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "ANNPIDTool.h"

//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( ANNPIDTool )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
ANNPIDTool::ANNPIDTool( const std::string& type, const std::string& name, const IInterface* parent )
    : BasePlotTool( type, name, parent ) {}

//=============================================================================
// Standard destructor
//=============================================================================
ANNPIDTool::~ANNPIDTool() {}

//=============================================================================
// Daughter plots - just mass plots
//=============================================================================
StatusCode ANNPIDTool::fillImpl( const LHCb::Particle* p, const std::string& /* trailer */ ) {
  // skip non-stable particles
  if ( !( p->isBasicParticle() ) ) return StatusCode::SUCCESS;

  const LHCb::ParticleProperty* pp = particleProperty( p->particleID() );
  if ( !pp ) return StatusCode::SUCCESS;

  const LHCb::ProtoParticle* proto = p->proto();
  if ( !proto ) return StatusCode::SUCCESS;

  return StatusCode::SUCCESS;
}
