/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef RichPlotTool_H
#define RichPlotTool_H 1

// STL
#include <sstream>

// local
#include "BasePlotTool.h"

// Rich
#include "RichRecInterfaces/IRichPIDPlots.h"
#include "RichUtils/RichHashMap.h"

/** @class RichPlotTool RichPlotTool.h
 *
 *  Fills RICH specific plots
 *
 *  @author Chris Jones
 *  @date   17/02/2009
 */
class RichPlotTool : public BasePlotTool {

public:
  /// Standard constructor
  RichPlotTool( const std::string& type, const std::string& name, const IInterface* parent );

  /// Initialise
  StatusCode initialize() override;

protected:
  /// Fill final state PID plots - overwrites BasePlotTools method
  StatusCode fillImpl( const LHCb::Particle* p, const std::string& trailer = "" ) override;

private:
  /// Get the Rich type enum from the Particle properties object
  Rich::ParticleIDType pidType( const LHCb::ParticleProperty* prop ) const;

  /// Returns the PID tool instance for the given Particle Name
  const Rich::Rec::IPIDPlots* pidTool( const std::string& name ) const;

private:
  /// Mapping between trailer and PID tool
  typedef Rich::HashMap<std::string, const Rich::Rec::IPIDPlots*> PIDToolMap;

private:
  /// Mapping between particle names and PID plot tools
  mutable PIDToolMap m_pidTools;

  /// Fill full set of histograms
  bool m_extraHistos;

  /// Plot Tool Configuration
  Rich::Rec::IPIDPlots::Configuration m_pidConfig;
};

#endif // RichPlotTool_H
