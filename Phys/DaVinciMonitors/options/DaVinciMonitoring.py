###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
##############################################################################
# $Id: DaVinciMonitoring.py,v 1.12 2010-05-24 09:51:19 ggiacomo Exp $
#
# syntax: gaudirun.py $DAVINCIMONITORSROOT/options/DVMonitorDst.py
#
# Author: Patrick Koppenburg <patrick.koppenburg@cern.ch>
#
##############################################################################
from DaVinci.Configuration import *
from Gaudi.Configuration import *
##############################################################################
#
# Count Events
#
from Configurables import EventCountHisto
DaVinci().MoniSequence += [EventCountHisto("DaVinciMonitor")]
##############################################################################
#
# Get right selection @todo does not work yet
#
# from Configurables import LoKi__HDRFilter   as HltDecReportsFilter
# HltDecReportsFilter  ( 'myname', Code = "HLT_PASS('somedecisionname')" )
#
#
# Imported stuff
#
importOptions("$DAVINCIMONITORSROOT/options/Jpsi2MuPi.py")
importOptions("$DAVINCIMONITORSROOT/options/RichCalib.py")
#importOptions( "$DAVINCIMONITORSROOT/options/MuonPidJpCalib.py" )
#importOptions( "$DAVINCIMONITORSROOT/options/MuonPidLaCalib.py" )
importOptions("$DAVINCIMONITORSROOT/options/MuIDMonitor.py")
importOptions("$DAVINCIMONITORSROOT/options/MuEffMonitor.py")

from DaVinciMonitors.TosMonitor import tosMonitors
DaVinci().MoniSequence += tosMonitors()
