###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
##############################################################################
#
# RICH PID Monitoring from data
#
from Configurables import RichPIDQCConf, DaVinci, GaudiSequencer
richSeq = GaudiSequencer("RichPIDMoniSeq")
RichPIDQCConf().setProp("CalibSequencer", richSeq)

RichPIDQCConf().Candidates["KshortPiPi"] = []
RichPIDQCConf().Candidates["JPsiMuMu"] = []
RichPIDQCConf().Candidates["DstarD0Pi"] = []
RichPIDQCConf().Candidates["LambdaPrPi"] = []
RichPIDQCConf().Candidates["DsPhiPi"] = []

#RichPIDQCConf().MCChecks = True                  # Enable MC checking as well
#RichPIDQCConf().PIDCalibrations = [ "KshortPiPi" ]  # The PID Calibration selections to run (default is all)

DaVinci().MoniSequence += [richSeq]  # RICH sequence
