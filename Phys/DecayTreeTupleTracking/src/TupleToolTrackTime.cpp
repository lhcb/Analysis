/*****************************************************************************\
* (c) Copyright 2010-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from Gaudi
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"
// local
#include "Event/Track.h"
#include "TupleToolTrackTime.h"

#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>

#include "LHCbMath/ValueWithError.h"

//-----------------------------------------------------------------------------
// Implementation file for class : TupleToolTrackTime
//
// 2016-02-26 : Francesco Dettori
//              francesco.dettori@cern.ch
//
// Based on code from W. Hulsbergen
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( TupleToolTrackTime )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TupleToolTrackTime::TupleToolTrackTime( const std::string& type, const std::string& name, const IInterface* parent )
    : TupleToolBase( type, name, parent ) {
  declareInterface<IParticleTupleTool>( this );
}

//=============================================================================
// Destructor
//=============================================================================
TupleToolTrackTime::~TupleToolTrackTime() {}

StatusCode TupleToolTrackTime::initialize() {
  StatusCode sc = TupleToolBase::initialize();
  if ( sc.isFailure() ) return sc;

  m_trackT0Tool = tool<ITrackT0Tool>( "TrackT0Tool", "Track_time", this );

  return sc;
}

//=============================================================================
StatusCode TupleToolTrackTime::fill( const LHCb::Particle*, const LHCb::Particle* P, const std::string& head,
                                     Tuples::Tuple& tuple ) {
  if ( msgLevel( MSG::VERBOSE ) ) verbose() << " filling particle " << endmsg;
  const std::string prefix = fullName( head );
  bool              test   = true;

  if ( !P ) return StatusCode::FAILURE;

  // first just return if the particle isn't supposed to have a track
  if ( !P->isBasicParticle() ) return StatusCode::SUCCESS;

  const LHCb::ProtoParticle* protop = P->proto();
  if ( !protop ) {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Protoparticle not found for this particle" << endmsg;
    return StatusCode::SUCCESS;
  }

  const LHCb::Track* track = protop->track();
  if ( !track ) {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Track not found for this particle" << endmsg;
    return StatusCode::SUCCESS;
  }

  const auto track_t0 = m_trackT0Tool->trackT0( *track );

  // Fill tuple
  test &= tuple->column( prefix + "_TRACK_time", track_t0.value() );
  test &= tuple->column( prefix + "_TRACK_time_err", track_t0.error() );

  return StatusCode::SUCCESS;
}
