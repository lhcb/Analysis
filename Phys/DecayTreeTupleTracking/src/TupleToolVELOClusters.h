/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TUPLETOOLRECOSTATS_H
#define TUPLETOOLRECOSTATS_H 1

// Include files
// from Gaudi
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "Kernel/IEventTupleTool.h" // Interface

#include "Event/Track.h"
#include "Event/VeloCluster.h"

/** @class TupleToolVELOClusters TupleToolVELOClusters.h
 *
 *  Fills VELO information
 */
class TupleToolVELOClusters : public TupleToolBase, virtual public IEventTupleTool {

public:
  /// Standard constructor
  TupleToolVELOClusters( const std::string& type, const std::string& name, const IInterface* parent );

  /// Declare properties
  Gaudi::Property<std::string> m_locationClusters{this, "VeloClusterLocation", LHCb::VeloClusterLocation::Default,
                                                  "Location of VELO clusters"};

  StatusCode fill( Tuples::Tuple& ) override; ///< Fill tuple
  //  int unusedVelo();
};

#endif // TUPLETOOLRECOSTATS_H
