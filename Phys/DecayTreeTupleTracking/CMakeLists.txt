###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Phys/DecayTreeTupleTracking
---------------------------
#]=======================================================================]

gaudi_add_module(DecayTreeTupleTracking
    SOURCES
        src/TupleToolConeIsolation.cpp
        src/TupleToolDEDX.cpp
        src/TupleToolMCAssociatedClusters.cpp
        src/TupleToolTrackIsolation.cpp
        src/TupleToolTrackKink.cpp
        src/TupleToolTrackPosition.cpp
        src/TupleToolTrackTime.cpp
        src/TupleToolVELOClusters.cpp
        src/TupleToolVeloTrackClusterInfo.cpp
        src/TupleToolVeloTrackMatch.cpp
    LINK
        Analysis::DecayTreeTupleBaseLib
        Boost::headers
        Gaudi::GaudiAlgLib
        Gaudi::GaudiKernel
        GSL::gsl
        LHCb::DAQEventLib
        LHCb::DigiEventLib
        LHCb::LHCbKernel
        LHCb::LHCbMathLib
        LHCb::LHCbTrackInterfacesLib
        LHCb::LinkerEvent
        LHCb::MCEvent
        LHCb::MCInterfacesLib
        LHCb::PhysEvent
        LHCb::TrackEvent
        Phys::DaVinciInterfacesLib
        Phys::DaVinciMCKernelLib
        Rec::TrackFitEvent
        Rec::TrackInterfacesLib
        Rec::TrackKernel
)
