/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @class TrackParticleRefitter.cpp TrackParticleRefitter.cpp.h
 *  Make a subselection of a track list
 */

#include "Event/KalmanFitResult.h"
#include "Event/Particle.h"
#include "Event/Track.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/ToolHandle.h"
#include "Kernel/IVertexFit.h"
#include "LHCbMath/LorentzVectorWithError.h"
#include "TrackInterfaces/ITrackFitter.h"
#include "TrackInterfaces/ITrackStateProvider.h"

#include <string>

class TrackParticleRefitter : public GaudiAlgorithm {

public:
  // Constructors and destructor
  using GaudiAlgorithm::GaudiAlgorithm;
  StatusCode execute() override;

private:
  StatusCode refittree( const LHCb::Particle& p, std::set<const LHCb::Particle*>& refittedparticles );

private:
  Gaudi::Property<std::string>          m_inputLocation{this, "ParticleLocation"};
  Gaudi::Property<bool>                 m_forceTrackRefit{this, "ForceTrackRefit", false};
  ToolHandle<IVertexFit>                m_vertexfitter{this, "VeretxFitter", "ParticleVertexFitter"};
  ToolHandle<ITrackFitter>              m_trackfitter{this, "TrackFitter", "TrackMasterFitter"};
  PublicToolHandle<ITrackStateProvider> m_stateprovider{this, "TrackStateProvider", "TrackStateProvider"};
};

DECLARE_COMPONENT( TrackParticleRefitter )

StatusCode TrackParticleRefitter::refittree( const LHCb::Particle&            p,
                                             std::set<const LHCb::Particle*>& refittedparticles ) {
  StatusCode sc = StatusCode::SUCCESS;
  if ( refittedparticles.find( &p ) == refittedparticles.end() ) {
    // if we are a simple track, then refit the track
    if ( p.proto() && p.proto()->track() ) {
      auto tr = p.proto()->track();
      if ( m_forceTrackRefit || !dynamic_cast<const LHCb::KalmanFitResult*>( tr->fitResult() ) ) {
        StatusCode thissc = m_trackfitter->operator()( const_cast<LHCb::Track&>( *tr ) );
        if ( !sc.isSuccess() ) {
          warning() << "problem fitting track" << endmsg;
          sc = thissc;
        }
        m_stateprovider->clearCache( *tr ); // better safe than sorry
      }
    } else {
      // make sure to refit all daughters
      for ( const LHCb::Particle* dau : p.daughters() ) {
        StatusCode thissc = refittree( *dau, refittedparticles );
        if ( !thissc.isSuccess() ) sc = thissc;
      }
      // if we have an endvertex, do a vertex fit
      if ( p.endVertex() ) {
        LHCb::Particle& ncp    = const_cast<LHCb::Particle&>( p );
        StatusCode      thissc = m_vertexfitter->reFit( ncp );
        // never quite understood why we need to set this separately
        ncp.setMeasuredMass( ncp.momentum().M() );
        ncp.setMeasuredMassErr( Gaudi::Math::LorentzVectorWithError( ncp.momentum(), ncp.momCovMatrix() ).m().error() );
        if ( !thissc.isSuccess() ) sc = thissc;
      }
    }
    // add this particle to the list of particles that we already treated
    refittedparticles.insert( &p );
  }
  return sc;
}

StatusCode TrackParticleRefitter::execute() {

  LHCb::Particle::Range inputparticles = get<LHCb::Particle::Range>( m_inputLocation );
  // we keep track of which particles were treated, so we don't do them more than once.
  std::set<const LHCb::Particle*> refittedparticles;
  // now do the vertex fits
  for ( const LHCb::Particle* p : inputparticles ) {
    StatusCode sc = refittree( *p, refittedparticles );
    if ( !sc.isSuccess() ) warning() << "Vertex fit failed" << endmsg;
  }
  return StatusCode::SUCCESS;
}
