/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TracksFromParticles_H_
#define TracksFromParticles_H_

#include "Event/TrackTypes.h"
#include "Kernel/DaVinciAlgorithm.h"

/** @class TracksFromParticles TracksFromParticles.h
 *
 *  Algorithm to create Tracks from Particles object
 *
 *  @author M. Needham
 *  @date   2012-04-27
 */

namespace LHCb {
  class Track;
}

class TracksFromParticles : public DaVinciAlgorithm {

public:
  /// destructor
  virtual ~TracksFromParticles();

  /// constructor
  TracksFromParticles( const std::string& name, ISvcLocator* pSvc );

  /// execute
  StatusCode execute() override;

private:
  std::string m_outputLocation;
};

#endif
