###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import DaVinci
##### Stream CONFIGURATION
MyStream = "Leptonic"
is2015 = True
fitasin = "2015"  # "2015","EM2015","Reco14"
DaVinci().InputType = "MDST"
inputs = ['Phys/B2XMuMu_Line/Particles']

DaVinci().DDDBtag = "dddb-20150724"
DaVinci().CondDBtag = "cond-20150828"

##### DAVINCI CONFIGURATION

from Configurables import DaVinci, RefitParticleTracks
DaVinci().EvtMax = 10000
DaVinci().PrintFreq = 1
if is2015:
    DaVinci().DataType = "2015"
else:
    DaVinci().DataType = "2012"
if DaVinci().InputType == "MDST":
    DaVinci().RootInTES = MyStream
DaVinci().Simulation = False
DaVinci().SkipEvents = 0

#### the actual algorithm
from DaVinciTrackRefitting import TrackRefitting
from Configurables import GaudiSequencer
refitter = TrackRefitting.TrackRefitting(
    "refittingalg",
    fitAsIn=fitasin,
    Stripping23=is2015,
    RootInTES=MyStream,  # Also needed on FullDST!
    Inputs=inputs,
    ValidationMode=True,
    UpdateProbability=True,
    DoProbability=True,
    Manipulator="Run2GhostId").fitter()
DaVinci().UserAlgorithms += [refitter]
