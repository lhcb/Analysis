/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// STD & STL
// ============================================================================
#include <sstream>
// ============================================================================
// ROOT/RooFit
// ============================================================================
#include "RooPrintable.h"
// ============================================================================
// Local
// ============================================================================
#include "Analysis/Printable.h"
// ============================================================================
/*  helper function to print printable object into string (needed for python)
 *  @param object the object
 *  @param verbose flag
 *  @param indent indent
 *  @return string
 *  @see RooPrintable::printMultiline
 */
// ==========================================================================
std::string Analysis::print_printable1( const RooPrintable& object, const int content, const bool verbose,
                                        std::string indent ) {
  std::ostringstream s;
  object.printMultiline( s, content, verbose, indent );
  return s.str();
}
// ==========================================================================
/*  helper function to print printable object into string (needed for python)
 *  @param object  the object
 *  @param content the content
 *  @param style  the style
 *  @param indent indent
 *  @return string
 *  @see RooPrritable::printSstream
 */
// ==========================================================================
std::string Analysis::print_printable2( const RooPrintable& object, const int content, const short style,
                                        std::string indent ) {
  std::ostringstream s;
  object.printStream( s, content,
                      style == RooPrintable::kInline
                          ? RooPrintable::kInline
                          : style == RooPrintable::kSingleLine
                                ? RooPrintable::kSingleLine
                                : style == RooPrintable::kStandard
                                      ? RooPrintable::kStandard
                                      : style == RooPrintable::kVerbose
                                            ? RooPrintable::kVerbose
                                            : style == RooPrintable::kTreeStructure ? RooPrintable::kTreeStructure
                                                                                    : RooPrintable::kStandard,
                      indent );
  return s.str();
}
// ==========================================================================
/*  helper function to print printable object into tree
 *  @param object  the object
 *  @param indent indent
 *  @return string
 */
// ==========================================================================
std::string Analysis::print_printable_tree( const RooPrintable& object, std::string indent ) {
  std::ostringstream s;
  object.printTree( s, indent );
  return s.str();
}

// ============================================================================
//                                                                      The END
// ============================================================================
