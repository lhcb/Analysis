/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id$
// ==================================================================
// Include files
// ==================================================================
// ROOT
// ==================================================================
#include "RooAbsData.h"
#include "RooCmdArg.h"
// ==================================================================
// local
// ==================================================================
#include "Analysis/RooMakeHistos.h"
// ==================================================================
/** @file
 *  Implementation file for class Analysis::RooMakeHistos
 *  @see Analysis::RooMakeHistos
 *  @see RooAbsData::createHistos
 *  @date   2011-07-16
 *  @author Vanya Belyaev
 */
// ==================================================================
// the only one method
// ==================================================================
TH1* Analysis::RooMakeHistos::create_histo( const RooAbsData& dataset, const std::string& name,
                                            const RooAbsRealLValue& xvar, const RooCmdArg& arg1, const RooCmdArg& arg2,
                                            const RooCmdArg& arg3, const RooCmdArg& arg4, const RooCmdArg& arg5,
                                            const RooCmdArg& arg6, const RooCmdArg& arg7, const RooCmdArg& arg8 ) {
  return dataset.createHistogram( name.c_str(), xvar, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8 );
}
// ==================================================================
// The END
// ==================================================================
