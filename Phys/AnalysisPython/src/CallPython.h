/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef CALLPYTHON_H
#define CALLPYTHON_H 1
// ============================================================================
// Incldue files
// ============================================================================
// Python
// ============================================================================
#include "Python.h"
// ============================================================================
// Ostap
// ============================================================================
#include "GaudiKernel/StatusCode.h"
// ============================================================================
// Local
// ============================================================================
#include "OstapFixes.h"
// ============================================================================
namespace {
  // ==========================================================================
  /// void -> double
  double call_python( PyObject* self, char* method ) {
    // check arguments
    Ostap::Assert( self, "CallPython:invalid ``self''", "CallPython:call_python", Ostap::StatusCode( 400 ) );
    // call Python
    PyObject* r = PyObject_CallMethod( self, method, nullptr );
    // error/exception ?
    if ( !r ) {
      PyErr_Print();
      Ostap::throwException( "CallPython:invalid ``result''", "CallPython:call_python", Ostap::StatusCode( 500 ) )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }
// float or integer ?
#if PY_MAJOR_VERSION >= 3
    if ( PyFloat_Check( r ) ) // floating value?
    {
      const double result = PyFloat_AS_DOUBLE( r );
      Py_DECREF( r );
      return result;                // RETURN
    } else if ( PyLong_Check( r ) ) // integer value ?
    {
      const double result = PyLong_AS_LONG( r );
      Py_DECREF( r );
      return result; //  RETURN
    }
#else
    if ( PyFloat_Check( r ) ) // floating value?
    {
      const double result = PyFloat_AS_DOUBLE( r );
      Py_DECREF( r );
      return result;               // RETURN
    } else if ( PyInt_Check( r ) ) // integer value ?
    {
      const double result = PyInt_AS_LONG( r );
      Py_DECREF( r );
      return result; //  RETURN
    }
#endif
    // ?
    const double result = PyFloat_AsDouble( r );
    if ( PyErr_Occurred() ) {
      PyErr_Print();
      Py_DECREF( r );
      Ostap::throwException( "CallPython:invalid conversion", "CallPython:call_python", Ostap::StatusCode( 600 ) )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }
    //
    Py_DECREF( r );
    return result; // RETURN
  }
  // ==========================================================================
} // namespace
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // CALLPYTHON_H
