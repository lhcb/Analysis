/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/StatusCode.h"
// ============================================================================
// LHCbMath
// ============================================================================
#include "LHCbMath/HistoInterpolation.h"
// ============================================================================
// AnalysisPython
// ============================================================================
#include "Analysis/Formula.h"
#include "Analysis/Funcs.h"
#include "Analysis/Iterator.h"
// ============================================================================
// ROOT&RooFit
// ============================================================================
#include "RooAbsData.h"
#include "RooArgList.h"
#include "RooArgSet.h"
#include "RooFormulaVar.h"
// ============================================================================
#include "TH1D.h"
#include "TH1F.h"
// ============================================================================
//  Local
// ============================================================================
/** @file
 *  Implementation file for classes from namespace Analysis::Functions
 *  @date 2018-03-31
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 */
// ============================================================================
// clang-format off
ClassImp(Analysis::Functions::FuncFormula)
ClassImp(Analysis::Functions::FuncTH1)
ClassImp(Analysis::Functions::FuncTH2)
ClassImp(Analysis::Functions::FuncTH3)
    // clang-format on
    // ============================================================================
    /*  constructor from the formula expression
     *  @param expression the  formula expression
     *  @param tree       the tree
     *  @param name       the name for the formula
     */
    // ============================================================================
    Analysis::Functions::FuncFormula::FuncFormula( const std::string& expression, const TTree* tree,
                                                   const std::string& name )
    : Analysis::IFuncTree()
    , TObject()
    , m_tree( tree )
    , m_formula( nullptr )
    , m_expression( expression )
    , m_name( name ) {
  if ( nullptr != m_tree && !make_formula() ) {
    throw GaudiException( "Invalid Formula '" + m_expression + "'", "Analysis::Function::FuncFormula",
                          StatusCode( 702 ) );
  }
}
// ============================================================================
Analysis::Functions::FuncFormula::~FuncFormula() {}
// ============================================================================
// notify
// ============================================================================
bool Analysis::Functions::FuncFormula::notify() const {
  return ( m_formula && m_formula->ok() ) ? m_formula->Notify() : false;
}
// ============================================================================
// make the formula
// ============================================================================
bool Analysis::Functions::FuncFormula::make_formula() const {
  m_formula.reset( nullptr );
  if ( nullptr == m_tree ) { return false; }
  m_formula.reset( nullptr );
  TTree* t  = const_cast<TTree*>( m_tree );
  m_formula = std::make_unique<Analysis::Formula>( m_name, m_expression, t );
  return m_formula && m_formula->ok();
}
// ============================================================================
//  evaluate the formula for  TTree
// ============================================================================
double Analysis::Functions::FuncFormula::operator()( const TTree* tree ) const {
  //
  if ( nullptr != tree && tree != m_tree ) {
    m_tree = tree;
    m_formula.reset( nullptr );
  }
  //
  if ( nullptr == m_tree ) {
    throw GaudiException( "Invalid Tree", "Analysis::Function::FuncFormula", StatusCode( 701 ) );
  }
  //
  if ( !m_formula || !m_formula->ok() ) { make_formula(); }
  if ( !m_formula || !m_formula->ok() ) {
    throw GaudiException( "Invalid Formula '" + m_expression + "'", "Analysis::Function::FuncFormula",
                          StatusCode( 702 ) );
  }
  //
  return m_formula->evaluate();
}
// ===========================================================================
// FuncTH
// ===========================================================================
/*  constructor
 *  @param tree          (INPUT) the tree
 *  @param interpolation (INPUT) interpolation type
 *  @param edges         (INPUT) special tretament of edges?
 *  @param extrapolate   (INPUT) use extrapolation?
 *  @param density       (INPUT) use  density?
 */
// ===========================================================================
Analysis::Functions::FuncTH::FuncTH( const TTree* tree, const bool edges, const bool extrapolate, const bool density )
    : Analysis::IFuncTree()
    , TObject()
    , m_tree( tree )
    , m_edges( edges )
    , m_extrapolate( extrapolate )
    , m_density( density ) {}
// ======================================================================
// destructor
// ======================================================================
Analysis::Functions::FuncTH::~FuncTH() {}
// ======================================================================
/*  (protected) constructor without the historgam
 *  @param xvar          (INPUT) the expression/variable
 *  @param tree          (INPUT) the tree
 *  @param interpolation (INPUT) interpolation type
 *  @param edges         (INPUT) special tretament of edges?
 *  @param extrapolate   (INPUT) use extrapolation?
 *  @param density       (INPUT) use  density?
 */
// ===========================================================================
Analysis::Functions::FuncTH1::FuncTH1( const std::string& xvar, const TTree* tree,
                                       const Gaudi::Math::HistoInterpolation::Type tx, const bool edges,
                                       const bool extrapolate, const bool density )
    : FuncTH( tree, edges, extrapolate, density ), m_xvar( nullptr ), m_xvar_exp( xvar ), m_tx( tx ) {
  if ( nullptr != m_tree && !make_xvar() ) {
    throw GaudiException( "Invalid Formula '" + m_xvar_exp + "'", "Analysis::Function::FuncTH1", StatusCode( 702 ) );
  }
}
// ======================================================================
/*  constructor from the histogram
 *  @param histo         (INPUT) the historgam
 *  @param xvar          (INPUT) the expression/variable
 *  @param tree          (INPUT) the tree
 *  @param interpolation (INPUT) interpolation type
 *  @param edges         (INPUT) special tretament of edges?
 *  @param extrapolate   (INPUT) use extrapolation?
 *  @param density       (INPUT) use  density?
 */
// ===========================================================================
Analysis::Functions::FuncTH1::FuncTH1( const TH1F& histo, const std::string& xvar, const TTree* tree,
                                       const Gaudi::Math::HistoInterpolation::Type tx, const bool edges,
                                       const bool extrapolate, const bool density )
    : FuncTH1( xvar, tree, tx, edges, extrapolate, density ) {
  /// copy the histogram
  histo.Copy( m_h1 );
  m_h1.SetDirectory( nullptr );
  ///
}
// ===========================================================================
/*  constructor from the formula expression
 *  @param histo         (INPUT) the historgam
 *  @param xvar          (INPUT) the expression/variable
 *  @param tree          (INPUT) the tree
 *  @param interpolation (INPUT) interpolation type
 *  @param edges         (INPUT) special tretament of edges?
 *  @param extrapolate   (INPUT) use extrapolation?
 *  @param density       (INPUT) use  density?
 */
// ===========================================================================
Analysis::Functions::FuncTH1::FuncTH1( const TH1D& histo, const std::string& xvar, const TTree* tree,
                                       const Gaudi::Math::HistoInterpolation::Type tx, const bool edges,
                                       const bool extrapolate, const bool density )
    : FuncTH1( xvar, tree, tx, edges, extrapolate, density ) {
  /// copy the histogram
  histo.Copy( m_h1 );
  m_h1.SetDirectory( nullptr );
  ///
}
// ============================================================================
Analysis::Functions::FuncTH1::~FuncTH1() {}
// ============================================================================
// notify
// ============================================================================
bool Analysis::Functions::FuncTH1::notify() const { return ( m_xvar && m_xvar->ok() ) ? m_xvar->Notify() : false; }
// ============================================================================
// make the formula
// ============================================================================
bool Analysis::Functions::FuncTH1::make_xvar() const {
  m_xvar.reset( nullptr );
  if ( nullptr == m_tree ) { return false; }
  m_xvar.reset( nullptr );
  TTree* t = const_cast<TTree*>( m_tree );
  m_xvar   = std::make_unique<Analysis::Formula>( "", m_xvar_exp, t );
  return m_xvar && m_xvar->ok();
}
// ============================================================================
//  evaluate the formula for  TTree
// ============================================================================
double Analysis::Functions::FuncTH1::operator()( const TTree* tree ) const {
  //
  if ( nullptr != tree && tree != m_tree ) {
    m_tree = tree;
    m_xvar.reset( nullptr );
  }
  //
  if ( nullptr == m_tree ) { throw GaudiException( "Invalid Tree", "Analysis::Function::FuncTH1", StatusCode( 701 ) ); }
  //
  if ( !m_xvar || !m_xvar->ok() ) { make_xvar(); }
  //
  if ( !m_xvar || !m_xvar->ok() ) {
    throw GaudiException( "Invalid Formula '" + m_xvar_exp + "'", "Analysis::Function::FuncTH1", StatusCode( 702 ) );
  }
  //
  const double xvar = m_xvar->evaluate();
  //
  return Gaudi::Math::HistoInterpolation::interpolate_1D( m_h1, xvar, m_tx, m_edges, m_extrapolate, m_density ).value();
}
// ===========================================================================
/*  (protected) constructor without histogram
 *  @param histo         (INPUT) the historgam
 *  @param xvar          (INPUT) the expression/variable
 *  @param yvar          (INPUT) the expression/variable
 *  @param tree          (INPUT) the tree
 *  @param tx            (INPUT) interpolation type
 *  @param ty            (INPUT) interpolation type
 *  @param edges         (INPUT) special tretament of edges?
 *  @param extrapolate   (INPUT) use extrapolation?
 *  @param density       (INPUT) use  density?
 */
// ===========================================================================
Analysis::Functions::FuncTH2::FuncTH2( const std::string& xvar, const std::string& yvar, const TTree* tree,
                                       const Gaudi::Math::HistoInterpolation::Type tx,
                                       const Gaudi::Math::HistoInterpolation::Type ty, const bool edges,
                                       const bool extrapolate, const bool density )
    : FuncTH( tree, edges, extrapolate, density )
    , m_xvar( nullptr )
    , m_yvar( nullptr )
    , m_xvar_exp( xvar )
    , m_yvar_exp( yvar )
    , m_tx( tx )
    , m_ty( ty ) {
  if ( nullptr != m_tree && !make_xvar() ) {
    throw GaudiException( "Invalid Formula '" + m_xvar_exp + "'", "Analysis::Function::FuncTH2", StatusCode( 702 ) );
  }
  if ( nullptr != m_tree && !make_yvar() ) {
    throw GaudiException( "Invalid Formula '" + m_yvar_exp + "'", "Analysis::Function::FuncTH2", StatusCode( 702 ) );
  }
}
// ======================================================================
/*  constructor from the histogram
 *  @param histo         (INPUT) the historgam
 *  @param xvar          (INPUT) the expression/variable
 *  @param tree          (INPUT) the tree
 *  @param interpolation (INPUT) interpolation type
 *  @param edges         (INPUT) special tretament of edges?
 *  @param extrapolate   (INPUT) use extrapolation?
 *  @param density       (INPUT) use  density?
 */
// ======================================================================
Analysis::Functions::FuncTH2::FuncTH2( const TH2F& histo, const std::string& xvar, const std::string& yvar,
                                       const TTree* tree, const Gaudi::Math::HistoInterpolation::Type tx,
                                       const Gaudi::Math::HistoInterpolation::Type ty, const bool edges,
                                       const bool extrapolate, const bool density )
    : FuncTH2( xvar, yvar, tree, tx, ty, edges, extrapolate, density ) {
  /// copy the histogram
  histo.Copy( m_h2 );
  m_h2.SetDirectory( nullptr );
  ///
}
// ======================================================================
/*  constructor from the histogram
 *  @param histo         (INPUT) the historgam
 *  @param xvar          (INPUT) the expression/variable
 *  @param tree          (INPUT) the tree
 *  @param interpolation (INPUT) interpolation type
 *  @param edges         (INPUT) special tretament of edges?
 *  @param extrapolate   (INPUT) use extrapolation?
 *  @param density       (INPUT) use  density?
 */
// ======================================================================
Analysis::Functions::FuncTH2::FuncTH2( const TH2D& histo, const std::string& xvar, const std::string& yvar,
                                       const TTree* tree, const Gaudi::Math::HistoInterpolation::Type tx,
                                       const Gaudi::Math::HistoInterpolation::Type ty, const bool edges,
                                       const bool extrapolate, const bool density )
    : FuncTH2( xvar, yvar, tree, tx, ty, edges, extrapolate, density ) {
  /// copy the histogram
  histo.Copy( m_h2 );
  m_h2.SetDirectory( nullptr );
  ///
}
// ============================================================================
//  destructor
// ============================================================================
Analysis::Functions::FuncTH2::~FuncTH2() {}
// ============================================================================
// notify
// ============================================================================
bool Analysis::Functions::FuncTH2::notify() const {
  const bool b1 = ( m_xvar && m_xvar->ok() ) ? m_xvar->Notify() : false;
  const bool b2 = ( m_yvar && m_yvar->ok() ) ? m_yvar->Notify() : false;
  return b1 && b2;
}
// ============================================================================
// make the formula
// ============================================================================
bool Analysis::Functions::FuncTH2::make_xvar() const {
  m_xvar.reset( nullptr );
  if ( nullptr == m_tree ) { return false; }
  m_xvar.reset( nullptr );
  TTree* t = const_cast<TTree*>( m_tree );
  m_xvar   = std::make_unique<Analysis::Formula>( "", m_xvar_exp, t );
  return m_xvar && m_xvar->ok();
}
// ============================================================================
// make the formula
// ============================================================================
bool Analysis::Functions::FuncTH2::make_yvar() const {
  m_yvar.reset( nullptr );
  if ( nullptr == m_tree ) { return false; }
  m_yvar.reset( nullptr );
  TTree* t = const_cast<TTree*>( m_tree );
  m_yvar   = std::make_unique<Analysis::Formula>( "", m_yvar_exp, t );
  return m_yvar && m_yvar->ok();
}
// ============================================================================
//  evaluate the formula for  TTree
// ============================================================================
double Analysis::Functions::FuncTH2::operator()( const TTree* tree ) const {
  //
  if ( nullptr != tree && tree != m_tree ) {
    m_tree = tree;
    m_xvar.reset( nullptr );
    m_yvar.reset( nullptr );
  }
  //
  if ( nullptr == m_tree ) { throw GaudiException( "Invalid Tree", "Analysis::Function::FuncTH2", StatusCode( 701 ) ); }
  //
  if ( !m_xvar || !m_xvar->ok() ) { make_xvar(); }
  if ( !m_yvar || !m_yvar->ok() ) { make_yvar(); }
  //
  if ( !m_xvar || !m_xvar->ok() ) {
    throw GaudiException( "Invalid Formula '" + m_xvar_exp + "'", "Analysis::Function::FuncTH2", StatusCode( 702 ) );
  }
  if ( !m_yvar || !m_yvar->ok() ) {
    throw GaudiException( "Invalid Formula '" + m_yvar_exp + "'", "Analysis::Function::FuncTH2", StatusCode( 702 ) );
  }
  //
  const double xvar = m_xvar->evaluate();
  const double yvar = m_yvar->evaluate();
  //
  return Gaudi::Math::HistoInterpolation::interpolate_2D( m_h2, xvar, yvar, m_tx, m_ty, m_edges, m_extrapolate,
                                                          m_density )
      .value();
}
// ===========================================================================
/*  (protected) constructor without histogram
 *  @param xvar          (INPUT) the expression/variable
 *  @param yvar          (INPUT) the expression/variable
 *  @param tree          (INPUT) the tree
 *  @param tx            (INPUT) interpolation type
 *  @param ty            (INPUT) interpolation type
 *  @param edges         (INPUT) special tretament of edges?
 *  @param extrapolate   (INPUT) use extrapolation?
 *  @param density       (INPUT) use  density?
 */
// ===========================================================================
Analysis::Functions::FuncTH3::FuncTH3( const std::string& xvar, const std::string& yvar, const std::string& zvar,
                                       const TTree* tree, const Gaudi::Math::HistoInterpolation::Type tx,
                                       const Gaudi::Math::HistoInterpolation::Type ty,
                                       const Gaudi::Math::HistoInterpolation::Type tz, const bool edges,
                                       const bool extrapolate, const bool density )
    : FuncTH( tree, edges, extrapolate, density )
    , m_xvar( nullptr )
    , m_yvar( nullptr )
    , m_zvar( nullptr )
    , m_xvar_exp( xvar )
    , m_yvar_exp( yvar )
    , m_zvar_exp( zvar )
    , m_tx( tx )
    , m_ty( ty )
    , m_tz( tz ) {
  if ( nullptr != m_tree && !make_xvar() ) {
    throw GaudiException( "Invalid Formula '" + m_xvar_exp + "'", "Analysis::Function::FuncTH3", StatusCode( 702 ) );
  }
  if ( nullptr != m_tree && !make_yvar() ) {
    throw GaudiException( "Invalid Formula '" + m_yvar_exp + "'", "Analysis::Function::FuncTH3", StatusCode( 702 ) );
  }
  if ( nullptr != m_tree && !make_zvar() ) {
    throw GaudiException( "Invalid Formula '" + m_zvar_exp + "'", "Analysis::Function::FuncTH3", StatusCode( 702 ) );
  }
}
// ======================================================================
/*  constructor from the histogram
 *  @param histo         (INPUT) the historgam
 *  @param xvar          (INPUT) the expression/variable
 *  @param yvar          (INPUT) the expression/variable
 *  @param zvar          (INPUT) the expression/variable
 *  @param tree          (INPUT) the tree
 *  @param tx            (INPUT) interpolation type
 *  @param ty            (INPUT) interpolation type
 *  @param tz            (INPUT) interpolation type
 *  @param edges         (INPUT) special tretament of edges?
 *  @param extrapolate   (INPUT) use extrapolation?
 *  @param density       (INPUT) use  density?
 */
// ======================================================================
Analysis::Functions::FuncTH3::FuncTH3( const TH3F& histo, const std::string& xvar, const std::string& yvar,
                                       const std::string& zvar, const TTree* tree,
                                       const Gaudi::Math::HistoInterpolation::Type tx,
                                       const Gaudi::Math::HistoInterpolation::Type ty,
                                       const Gaudi::Math::HistoInterpolation::Type tz, const bool edges,
                                       const bool extrapolate, const bool density )
    : FuncTH3( xvar, yvar, zvar, tree, tx, ty, tz, edges, extrapolate, density ) {
  /// copy the histogram
  histo.Copy( m_h3 );
  m_h3.SetDirectory( nullptr );
  ///
}
// ======================================================================
/*  constructor from the histogram
 *  @param histo         (INPUT) the historgam
 *  @param xvar          (INPUT) the expression/variable
 *  @param tree          (INPUT) the tree
 *  @param interpolation (INPUT) interpolation type
 *  @param edges         (INPUT) special tretament of edges?
 *  @param extrapolate   (INPUT) use extrapolation?
 *  @param density       (INPUT) use  density?
 */
// ======================================================================
Analysis::Functions::FuncTH3::FuncTH3( const TH3D& histo, const std::string& xvar, const std::string& yvar,
                                       const std::string& zvar, const TTree* tree,
                                       const Gaudi::Math::HistoInterpolation::Type tx,
                                       const Gaudi::Math::HistoInterpolation::Type ty,
                                       const Gaudi::Math::HistoInterpolation::Type tz, const bool edges,
                                       const bool extrapolate, const bool density )
    : FuncTH3( xvar, yvar, zvar, tree, tx, ty, tz, edges, extrapolate, density ) {
  /// copy the histogram
  histo.Copy( m_h3 );
  m_h3.SetDirectory( nullptr );
  ///
}
// ============================================================================
//  destructor
// ============================================================================
Analysis::Functions::FuncTH3::~FuncTH3() {}
// ============================================================================
// notify
// ============================================================================
bool Analysis::Functions::FuncTH3::notify() const {
  const bool b1 = ( m_xvar && m_xvar->ok() ) ? m_xvar->Notify() : false;
  const bool b2 = ( m_yvar && m_yvar->ok() ) ? m_yvar->Notify() : false;
  const bool b3 = ( m_zvar && m_zvar->ok() ) ? m_zvar->Notify() : false;
  return b1 && b2 && b3;
}
// ============================================================================
// make the formula
// ============================================================================
bool Analysis::Functions::FuncTH3::make_xvar() const {
  m_xvar.reset( nullptr );
  if ( nullptr == m_tree ) { return false; }
  m_xvar.reset( nullptr );
  TTree* t = const_cast<TTree*>( m_tree );
  m_xvar   = std::make_unique<Analysis::Formula>( "", m_xvar_exp, t );
  return m_xvar && m_xvar->ok();
}
// ============================================================================
// make the formula
// ============================================================================
bool Analysis::Functions::FuncTH3::make_yvar() const {
  m_yvar.reset( nullptr );
  if ( nullptr == m_tree ) { return false; }
  m_yvar.reset( nullptr );
  TTree* t = const_cast<TTree*>( m_tree );
  m_yvar   = std::make_unique<Analysis::Formula>( "", m_yvar_exp, t );
  return m_yvar && m_yvar->ok();
}
// ============================================================================
// make the formula
// ============================================================================
bool Analysis::Functions::FuncTH3::make_zvar() const {
  m_zvar.reset( nullptr );
  if ( nullptr == m_tree ) { return false; }
  m_zvar.reset( nullptr );
  TTree* t = const_cast<TTree*>( m_tree );
  m_zvar   = std::make_unique<Analysis::Formula>( "", m_zvar_exp, t );
  return m_zvar && m_zvar->ok();
}
// ============================================================================
//  evaluate the formula for  TTree
// ============================================================================
double Analysis::Functions::FuncTH3::operator()( const TTree* tree ) const {
  //
  if ( nullptr != tree && tree != m_tree ) {
    m_tree = tree;
    m_xvar.reset( nullptr );
    m_yvar.reset( nullptr );
    m_zvar.reset( nullptr );
  }
  //
  if ( nullptr == m_tree ) { throw GaudiException( "Invalid Tree", "Analysis::Function::FuncTH3", StatusCode( 701 ) ); }
  //
  if ( !m_xvar || !m_xvar->ok() ) { make_xvar(); }
  if ( !m_yvar || !m_yvar->ok() ) { make_yvar(); }
  if ( !m_zvar || !m_zvar->ok() ) { make_zvar(); }
  //
  if ( !m_xvar || !m_xvar->ok() ) {
    throw GaudiException( "Invalid Formula '" + m_xvar_exp + "'", "Analysis::Function::FuncTH3", StatusCode( 702 ) );
  }
  if ( !m_yvar || !m_yvar->ok() ) {
    throw GaudiException( "Invalid Formula '" + m_yvar_exp + "'", "Analysis::Function::FuncTH3", StatusCode( 702 ) );
  }
  if ( !m_zvar || !m_zvar->ok() ) {
    throw GaudiException( "Invalid Formula '" + m_zvar_exp + "'", "Analysis::Function::FuncTH3", StatusCode( 702 ) );
  }
  //
  const double xvar = m_xvar->evaluate();
  const double yvar = m_yvar->evaluate();
  const double zvar = m_yvar->evaluate();
  //
  return Gaudi::Math::HistoInterpolation::interpolate_3D( m_h3, xvar, yvar, zvar, m_tx, m_ty, m_tz, m_edges,
                                                          m_extrapolate, m_density )
      .value();
}

// ===========================================================================
/* constructor from the formula expression
 *  @param expression the formula expression
 *  @param data       the data
 *  @param name       the name for the formula
 */
// ===========================================================================
Analysis::Functions::FuncRooFormula::FuncRooFormula( const std::string& expression, const RooAbsData* data,
                                                     const std::string& name )
    : Analysis::IFuncData(), m_data( data ), m_formula( nullptr ), m_expression( expression ), m_name( name ) {}
// ============================================================================
Analysis::Functions::FuncRooFormula::~FuncRooFormula() {}
// ============================================================================
// make the formula
// ============================================================================
bool Analysis::Functions::FuncRooFormula::make_formula() const {
  m_formula.reset( nullptr );
  if ( nullptr == m_data ) { return false; }
  const RooArgSet* varset = m_data->get();
  if ( nullptr == varset ) {
    throw GaudiException( "Invalid RooArgSet", "Analysis::Function::FuncRooFormula", StatusCode( 707 ) );
  }
  RooArgList         varlst;
  Analysis::Iterator iter( *varset );
  while ( RooAbsArg* a = iter.static_next<RooAbsArg>() ) { varlst.add( *a ); }
  //
  m_formula = std::make_unique<RooFormulaVar>( m_name.c_str(), m_expression.c_str(), varlst );
  //
  return m_formula && m_formula->ok();
}
// ============================================================================
//  evaluate the formula for  Data
// ============================================================================
double Analysis::Functions::FuncRooFormula::operator()( const RooAbsData* data ) const {
  //
  if ( nullptr != data && data != m_data ) {
    m_data = data;
    m_formula.reset( nullptr );
  }
  //
  if ( nullptr == m_data ) {
    throw GaudiException( "Invalid RooAbsData", "Analysis::Function::FuncRooFormula", StatusCode( 709 ) );
  }
  //
  if ( !m_formula || !m_formula->ok() ) { make_formula(); }
  if ( !m_formula || !m_formula->ok() ) {
    throw GaudiException( "Invalid RooFormula '" + m_expression + "'", "Analysis::Function::FuncRooFormula",
                          StatusCode( 710 ) );
  }
  //
  return m_formula->getVal();
}
// ============================================================================
// The END
// ============================================================================
