/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
//  STD & STL
// ============================================================================

// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/StatusCode.h"
// ============================================================================
// AnalysisPythoin
// ============================================================================
#include "Analysis/ErrorHandlers.h"
// ============================================================================
// GSL
// ============================================================================
#include "gsl/gsl_errno.h"
// ============================================================================
// ROOT
// ============================================================================
#include "TError.h"
// ============================================================================
// Python
// ============================================================================
// #include "Python.h"
// ============================================================================
namespace {
  // ==========================================================================
  /// (previous) error  handler
  ErrorHandlerFunc_t s_handler = nullptr;
  // ==========================================================================
  void rootErrorHandler( int level, Bool_t abort, const char* location, const char* message ) {
    //
    if ( gErrorIgnoreLevel == kUnset ) { ::DefaultErrorHandler( kUnset - 1, kFALSE, "", "" ); }
    // silently ignore ...
    if ( level < gErrorIgnoreLevel ) { return; }
    // error: throw exception
    if ( kError <= level ) {
      std::string msg = location && location[0] ? message + std::string( " in " ) + location : std::string( message );
      std::string tag =
          kError == level
              ? "ROOT/Error"
              : kBreak == level ? "ROOT/Break"
                                : kSysError == level ? "ROOT/SysError" : kFatal == level ? "ROOT/Fatal" : "ROOT/error";
      throw GaudiException( tag + ": " + msg, tag, StatusCode( 10000 + level ) );
    }
    // else if ( kWarning <= level )
    // {
    //  // python warning here
    //  PyErr_WarnExplicit( NULL, (char*)msg, (char*)location, 0, (char*)"ROOT", NULL );
    // }
    else if ( nullptr != s_handler ) {
      ( *s_handler )( level, abort, location, message );
    } else {
      ::DefaultErrorHandler( level, abort, location, message );
    }
  }
  // ==========================================================================
  // GSL
  // ==========================================================================
  void GSL_local_error( const char* reason, const char* file, int line, int gsl_errno ) {
    std::cerr << " GSL_ERROR : " << gsl_errno << "/'" << gsl_strerror( gsl_errno ) << "'"
              << "\t reason '" << reason << "' "
              << "\t file/line '" << file << "'/" << line << std::endl;
  }
  // ==========================================================================
  void GSL_ignore_error( const char* /* reason    */, const char* /* file      */, int /* line      */,
                         int /* gsl_errno */ ) {}
  // ==========================================================================
  void GSL_exception_error( const char* reason, const char* file, int line, int gsl_errno ) {
    std::string        tag = "GSL/Error";
    std::ostringstream ss;
    ss << gsl_strerror( gsl_errno ) << "(" << gsl_errno << ") " << reason << " in " << file << " at line " << line;
    throw GaudiException( tag + ": " + ss.str(), tag, StatusCode( 20000 + gsl_errno ) );
  }
  // ==========================================================================
} // namespace

// ============================================================================
// use local error handler for ROOT
// ============================================================================
bool Analysis::Utils::useRootErrorHandler( const bool use ) {
  if ( !use && GetErrorHandler() == &rootErrorHandler ) {
    if ( nullptr != s_handler && s_handler != &rootErrorHandler ) {
      SetErrorHandler( s_handler );
      return true;
    } // RETURN
  } else if ( use && GetErrorHandler() != &rootErrorHandler ) {
    s_handler = GetErrorHandler();
    SetErrorHandler( &rootErrorHandler );
    return true; // RETURN
  }
  return false;
}
// ============================================================================
// constructor: make use of local error handler
// ============================================================================
Analysis::Utils::RootError::RootError() : m_previous( Analysis::Utils::useRootErrorHandler( true ) ) {}
// ============================================================================
// destructor: stop local error handler
// ============================================================================
Analysis::Utils::RootError::~RootError() {
  if ( m_previous ) { Analysis::Utils::useRootErrorHandler( false ); }
}

// ============================================================================
// constructor: make use of Gsl Error Handler: print error to stderr
// ============================================================================
Analysis::Utils::GslError::GslError( Analysis::Utils::GslError::handler* h )
    : m_previous( gsl_set_error_handler( h ) ) {
  static_assert( std::is_same<handler, gsl_error_handler_t>::value, "``handler'' type is not ``gsl_error_handler_t''" );
}
// ============================================================================
// constructor: make use of Gsl Error Handler: print error to stderr
// ============================================================================
Analysis::Utils::GslError::GslError() : GslError( &GSL_local_error ) {}
// ============================================================================
// destructor: stop using the error  handler
// ============================================================================
Analysis::Utils::GslError::~GslError() { gsl_set_error_handler( m_previous ); }
// ============================================================================
// constructor: make use of Gsl Error Handler: print error to stderr
// ============================================================================
Analysis::Utils::GslIgnore::GslIgnore() : GslError( &GSL_ignore_error ) {}
// ============================================================================
// constructor: make use of Gsl Error Handler: print error to stderr
// ============================================================================
Analysis::Utils::GslException::GslException() : GslError( &GSL_exception_error ) {}
// ============================================================================

// ============================================================================
// The END
// ============================================================================
