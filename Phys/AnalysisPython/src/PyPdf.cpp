/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// Python
// ============================================================================
#include "Python.h"
// ============================================================================
// ROOT
// ============================================================================
#include "RVersion.h"
#include "TClass.h"
#include "TPython.h"
// ============================================================================
// Ostap
// ============================================================================
#include "Analysis/Iterator.h"
#include "Analysis/PyPdf.h"
// ============================================================================
// Local
// ============================================================================
#include "CallPython.h"
#include "OstapFixes.h"
// ============================================================================
/** @file
 *  Implementation file for class Ostap::Models::PyPdf
 *  @date 2018-06-06
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 */
// ============================================================================
namespace {
  // ==========================================================================
  static char s_evaluate[] = "evaluate";
  static char s_clone[]    = "clone";
  static char s_getAI[]    = "get_analytical_integral";
  static char s_AI[]       = "analytical_integral";
  // ==========================================================================
} // namespace
// ============================================================================
/*  Standard constructor
 *  @param self      python-partner for this C++ instance
 *  @param name      the name of PDF
 *  @param title     the title  of PDF
 *  @param variables all variables
 */
// ============================================================================
Analysis::Models::PyPdf::PyPdf( PyObject* self, const char* name, const char* title, const RooAbsCollection& variables )
    : RooAbsPdf( name, title )
    , m_self( self )
    , m_varlist( "varlist", "All variables(list)", this )
    , m_varset( "varset", "All variables(set)", this ) {
  //
  Ostap::Assert( m_self, "self* points to NULL", "PyPdf::consructor", Ostap::StatusCode( 400 ) );
  //
  Analysis::Iterator it( variables );
  while ( RooAbsReal* v = it.static_next<RooAbsReal>() ) {
    m_varlist.add( *v );
    m_varset.add( *v );
  }
  Py_XINCREF( m_self );
}
// ============================================================================
// copy constructor
// ============================================================================
Analysis::Models::PyPdf::PyPdf( const Analysis::Models::PyPdf& right, const char* name )
    : RooAbsPdf( right, name )
    //
    , m_self( right.m_self )
    , m_varlist( "!varlist", this, right.m_varlist )
    , m_varset( "!varset", this, right.m_varset ) {
  Py_XINCREF( m_self );
}
// ============================================================================
// virtual destructor
// ============================================================================
Analysis::Models::PyPdf::~PyPdf() {
  if ( m_self ) { Py_DECREF( m_self ); }
}
// ============================================================================
Analysis::Models::PyPdf* Analysis::Models::PyPdf::clone( const char* name ) const {
  /// create the python clone
  PyObject* method = PyObject_GetAttrString( m_self, s_clone );
  if ( !method ) {
    PyErr_Print();
    Ostap::throwException( "No method ``clone'' is found", "PyPdf::clone", Ostap::StatusCode( 500 ) )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }
  if ( !PyCallable_Check( method ) ) {
    PyErr_Print();
    Py_DECREF( method );
    Ostap::throwException( "Attribute ``clone'' is not callable", "PyPdf::clone", Ostap::StatusCode( 500 ) )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }
  /// create C++ clone
  PyPdf* cl = new Analysis::Models::PyPdf( *this, name );
  /// create kwargs
  PyObject* kwargs = PyDict_New();
///
/// set "name"-item
#if PY_MAJOR_VERSION >= 3
  if ( 0 != PyDict_SetItem( kwargs, PyUnicode_FromString( "name" ), PyUnicode_FromString( ( name ? name : "" ) ) ) ) {
#else
  if ( 0 != PyDict_SetItem( kwargs, PyString_FromString( "name" ), PyString_FromString( ( name ? name : "" ) ) ) ) {
#endif
    PyErr_Print();
    Py_DECREF( method );
    Py_DECREF( kwargs );
    Ostap::throwException( "Can't set ``name'' item", "PyPdf::clone", Ostap::StatusCode( 500 ) )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }
  /// create "pdf"-item

#if ROOT_VERSION_CODE >= ROOT_VERSION( 6, 22, 0 )
  PyObject* pycl = TPython::CPPInstance_FromVoidPtr( cl, cl->IsA()->GetName(), false );
#else
  PyObject* pycl = TPython::ObjectProxy_FromVoidPtr( cl, cl->IsA()->GetName(), false );
#endif
  if ( !pycl ) {
    PyErr_Print();
    Py_DECREF( method );
    Py_DECREF( kwargs );
    Ostap::throwException( "Can't pythonize PyPdf instance", "PyPdf::clone", Ostap::StatusCode( 500 ) )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }
#if PY_MAJOR_VERSION >= 3
  if ( 0 != PyDict_SetItem( kwargs, PyUnicode_FromString( "pdf" ), pycl ) ) {
#else
  if ( 0 != PyDict_SetItem( kwargs, PyString_FromString( "pdf" ), pycl ) ) {
#endif
    PyErr_Print();
    Py_DECREF( method );
    Py_DECREF( kwargs );
    Py_DECREF( pycl );
    Ostap::throwException( "Can't set ``pdf'' item", "PyPdf::clone", Ostap::StatusCode( 500 ) )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }
  /// create args
  PyObject* args = PyTuple_New( 0 );
  //
  // create python clone!
  PyObject* pyclone = PyObject_Call( method, args, kwargs );
  if ( !pyclone ) {
    PyErr_Print();
    Py_DECREF( method );
    Ostap::throwException( "Can't create  python ``clone''", "PyPdf::clone", Ostap::StatusCode( 500 ) )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }
  //
  Py_INCREF( pyclone );
  ///
  PyObject* old = cl->m_self;
  cl->m_self    = pyclone; // the most important line!!!
  //
  Py_DECREF( method );
  if ( old ) { Py_DECREF( old ); }
  //
  return cl;
}
// ============================================================================
// declare the analysitical integrals
// ============================================================================
Int_t Analysis::Models::PyPdf::getAnalyticalIntegral( RooArgSet& allVars, RooArgSet& analVars,
                                                      const char* rangeName ) const {
  //
  Ostap::Assert( m_self, "self* points to NULL", "PyPdf::getAnalyticalIntegral", Ostap::StatusCode( 400 ) );
  ///
  m_allDeps   = &allVars;
  m_analDeps  = &analVars;
  m_rangeName = rangeName;
  m_intCode   = 0;
  //
  if ( 1 != PyObject_HasAttrString( m_self, s_getAI ) || 1 != PyObject_HasAttrString( m_self, s_AI ) ) {
    //
    m_allDeps   = nullptr;
    m_analDeps  = nullptr;
    m_rangeName = nullptr;
    m_intCode   = 0;
    //
    return 0;
  }
  //
  /// create the python method
  PyObject* getai = PyObject_GetAttrString( m_self, s_getAI );
  if ( !getai || !PyCallable_Check( getai ) ) {
    if ( nullptr == getai ) {
      PyErr_Print();
    } else {
      Py_DECREF( getai );
    }
    //
    m_allDeps   = nullptr;
    m_analDeps  = nullptr;
    m_rangeName = nullptr;
    m_intCode   = 0;
    //
    return 0; // RETURN
  }
  /// create args
  PyObject* args = PyTuple_New( 0 );
  //
  // call python method
  PyObject* icode = PyObject_Call( getai, args, nullptr );
#if PY_MAJOR_VERSION >= 3
  if ( !icode || !PyLong_Check( icode ) ) // integer value ? )
#else
  if ( !icode || !PyInt_Check( icode ) ) // integer value ? )
#endif
  {
    if ( nullptr == icode ) {
      PyErr_Print();
    } else {
      Py_DECREF( icode );
    }
    //
    Py_DECREF( getai );
    //
    Ostap::throwException( "Can't get proper code", "PyPdf::getAnalyticalIntegral", Ostap::StatusCode( 500 ) )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }
//
#if PY_MAJOR_VERSION >= 3
  const Int_t code = PyLong_AS_LONG( icode );
#else
  const Int_t code = PyInt_AS_LONG( icode );
#endif
  Py_DECREF( icode );
  ///
  m_allDeps   = nullptr;
  m_analDeps  = nullptr;
  m_rangeName = nullptr;
  m_intCode   = 0;
  //
  return code;
}
// ============================================================================
// get the integral
// ============================================================================
Double_t Analysis::Models::PyPdf::analyticalIntegral( Int_t code, const char* rangeName ) const {
  //
  m_intCode   = code;
  m_rangeName = rangeName;
  ///
  const double result = call_python( m_self, s_AI );
  //
  m_intCode   = 0;
  m_rangeName = nullptr;
  //
  return result;
}
// ============================================================================
/// move the function from protected to public interface
Bool_t Analysis::Models::PyPdf::matchArgs( const RooArgSet& refVars ) const {
  return nullptr != m_allDeps && nullptr != m_analDeps && RooAbsReal::matchArgs( *m_allDeps, *m_analDeps, refVars );
}
// ============================================================================
// the actual evaluation of function
// ============================================================================
Double_t Analysis::Models::PyPdf::evaluate() const { return call_python( m_self, s_evaluate ); }
// ============================================================================
// clang-format off
ClassImp(Analysis::Models::PyPdf)
    // clang-format on
    // ============================================================================
    //                                                                      The END
    // ============================================================================
