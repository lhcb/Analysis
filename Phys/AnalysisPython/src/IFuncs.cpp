/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// local
// ============================================================================
#include "Analysis/IFuncs.h"
// ============================================================================
/** @file
 *  Implementation file for classes
 *  @date 2018-03-31
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 */
// ============================================================================
// desructor
// ============================================================================
Analysis::IFuncTree::~IFuncTree() {}
// ============================================================================
// desructor
// ============================================================================
Analysis::IFuncData::~IFuncData() {}
// ============================================================================
// The END
// ============================================================================
