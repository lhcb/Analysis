/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id$
// ============================================================================
#ifndef ANALYSIS_TEE_H
#define ANALYSIS_TEE_H 1
// ============================================================================
// Include files
// ============================================================================
// STD& STDL
// ============================================================================
#include <memory>
#include <ostream>
#include <streambuf>
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/Kernel.h"
// ============================================================================
namespace Gaudi {
  // ==========================================================================
  namespace Utils {
    // ========================================================================
    /** @class Tee Tee.h Analysis/Tee.h
     *  Helper utility for "tee"
     *  @see Gaudi::Utils::Mute
     *  @author Vanya Belyaev
     *  @date   2013-07-07
     */
    class GAUDI_API Tee {
    public:
      // ======================================================================
      /// constructor from filename
      Tee( const std::string& filename = "tee.out" );
      /// constructor from the stream
      Tee( std::ostream& filestream );
      // destructor
      ~Tee();
      // ======================================================================
    private:
      // ======================================================================
      /// copy     constructor is disabled
      Tee( const Tee& ); // copy    constructor is disabled
      /// assignement operator is disabled
      Tee& operator=( const Tee& ); // assignement operator is disabled
      // ======================================================================
    public: // helper stuff to use it in python as Context Manager
      // ======================================================================
      /** helper function to implement python's __enter__
       *  the action is performed in constructor
       */
      void enter();
      /// helper function to implement python's __exit__
      void exit();
      // ======================================================================
    private:
      // ======================================================================
      /// the file itself
      std::unique_ptr<std::ostream> m_file{nullptr}; // the file itself
      /// is the file owned?
      bool                            m_own{true}; // is the file owned?
      std::unique_ptr<std::streambuf> m_buffer{nullptr};
      std::streambuf*                 m_keep{nullptr}; // keep the standard buffer
      // ======================================================================
    };
    // ========================================================================
  } // namespace Utils
  // ==========================================================================
} //                                                     end of namespace Gaudi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // ANALYSIS_TEE_H
