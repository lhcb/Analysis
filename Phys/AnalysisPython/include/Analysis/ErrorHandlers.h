/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef ANALYSIS_ERRORHANDLERS_H
#define ANALYSIS_ERRORHANDLERS_H 1
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/Kernel.h"
// ============================================================================
namespace Analysis {
  // ==========================================================================
  namespace Utils {
    // ========================================================================
    /// use local error handler for ROOT
    GAUDI_API
    bool useRootErrorHandler( const bool use = true );
    // ========================================================================
    /** @class RootError
     *  Simple error handler for ROOT that converts error messages into exceptions
     *  @author Vanya Belyaev Ivan.Belayev@itep.ru
     *  @date   2016-12-10
     */
    class GAUDI_API RootError {
    public:
      // =======================================================================
      /// constructor: make use of local error handler
      RootError();
      /// destructor: stop using local error handler
      ~RootError();
      // =======================================================================
    private:
      // =======================================================================
      /// is actually used?
      bool m_previous; // is actually used?
      // ======================================================================
    };
    // ========================================================================
    /** @class GslError
     *  helper class to manipulate with GSL error handlers
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     */
    class GAUDI_API GslError {
      //=======================================================================
    public:
      //=======================================================================
      /// constructor: make use of Gsl Error Handler: print error to stderr
      GslError();
      /// destructor: stop using the error  handler
      ~GslError();
      // ======================================================================
    protected:
      // ======================================================================
      typedef void handler( const char*, const char*, int, int );
      GslError( handler* h );
      handler* m_previous;
      // ======================================================================
    };
    // ========================================================================
    /** @class GslIgnore
     *  helper class to manipulate with GSL error handlers
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     */
    class GAUDI_API GslIgnore : public GslError {
      //=======================================================================
    public:
      //=======================================================================
      /// constructor: make use of Gsl Error Handler: ignore error
      GslIgnore(); // constructor: make use of Gsl Error Handler: ignore error
      // ======================================================================
    };
    // ========================================================================
    /** @class GslException
     *  helper class to manipulate with GSL error handlers :
     *  throw exception on error
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     */
    class GslException : public GslError {
      //=======================================================================
    public:
      //=======================================================================
      /// constructor: make use of Gsl Error Handler: throw Exception
      GslException(); // constructor: make use of Gsl Error Handler: throw Exception
      // ======================================================================
    };
    // ========================================================================
  } // namespace Utils
  // ==========================================================================
} //                                                  end of namespace Analysis
// ============================================================================
#endif // ANALYSIS_ERRORHANDLERS_H
