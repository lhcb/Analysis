/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef ANALYSIS_PRINTABLE_H
#define ANALYSIS_PRINTABLE_H 1
// ============================================================================
// Include files
// ============================================================================
// STD & STL
// ============================================================================
#include <ostream>
// ============================================================================
// ROOT/RooFit
// ============================================================================
class RooPrintable;
// ============================================================================
namespace Analysis {
  // ==========================================================================
  /** helper function to print printable object into string (needed for python)
   *  @param object the object
   *  @param verbose flag
   *  @param indent indent
   *  @return string
   *  @see RooPrintable::printMultiline
   */
  std::string print_printable1( const RooPrintable& object, const int content, const bool verbose = false,
                                std::string indent = "" );
  // ==========================================================================
  /** helper function to prin=t printable object into string (needed for python)
   *  @param object  the object
   *  @param content the content
   *  @param style  the style
   *  @param indent indent
   *  @return string
   *  @see RooPrritable::printSstream
   */
  std::string print_printable2( const RooPrintable& objbect, const int content, const short style,
                                std::string indent = "" );
  // ==========================================================================
  /** helper function to print printable object into tree
   *  @param object  the object
   *  @param indent indent
   *  @return string
   */
  std::string print_printable_tree( const RooPrintable& object, std::string indent = "" );
  // ==========================================================================
} // namespace Analysis
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // ANALYSIS_PRINTABLE_H
