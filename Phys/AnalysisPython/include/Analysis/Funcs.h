/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef OSTAP_FUNCS_H
#define OSTAP_FUNCS_H 1
// ============================================================================
// Include files
// ============================================================================
// STD&STL
// ============================================================================
#include <memory>
#include <string>
// ============================================================================
// LHCbMath
// ============================================================================
#include "LHCbMath/HistoInterpolation.h"
// ============================================================================
// Ostap
// ============================================================================
#include "Analysis/IFuncs.h"
// ============================================================================
// ROOT
// ============================================================================
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TObject.h"
// ============================================================================
class RooFormulaVar; // from RooFit
class TH1F;          // ROOT
class TH1D;          // ROOT
class TH2F;          // ROOT
class TH2D;          // ROOT
class TH3F;          // ROOT
class TH3D;          // ROOT
// ============================================================================
namespace Analysis {
  // ==========================================================================
  /// from Ostap
  class Formula;
  // ==========================================================================
  namespace Functions {
    // ========================================================================
    /** @class FuncFormula
     *  simple implementation of TTree-function based on Ostap::Formula
     */
    class FuncFormula : public Analysis::IFuncTree, public TObject {
    public:
      // ======================================================================
      ClassDef( Analysis::Functions::FuncFormula, 1 );
      // ======================================================================
    public:
      // ======================================================================
      /** constructor from the formula expression
       *  @param expression the  formula expression
       *  @param tree       the tree
       *  @param name       the name for the formula
       */
      FuncFormula( const std::string& expression, const TTree* tree = nullptr, const std::string& name = "" );
      // ======================================================================
      /// default constructor, needed for serialization
      FuncFormula() = default;
      /// destructor
      virtual ~FuncFormula();
      // ======================================================================
    public:
      // ======================================================================
      ///  evaluate the formula for  TTree
      double operator()( const TTree* tree = nullptr ) const override;
      // ======================================================================
    public:
      // ======================================================================
      Bool_t Notify() override { return notify(); }
      bool   notify() const;
      // ======================================================================
    private: // make the formula
      // ======================================================================
      bool make_formula() const;
      // ======================================================================
    private:
      // ======================================================================
      mutable const TTree*                       m_tree{nullptr};    //!
      mutable std::unique_ptr<Analysis::Formula> m_formula{nullptr}; //!
      // ======================================================================
      /// the  expression itself
      std::string m_expression{}; // the  expression itself
      /// the name
      std::string m_name{}; // the name
      // ======================================================================
    };
    // ========================================================================
    /** @class FuncTH
     *  simple implementation of TTree-function based on TH
     */
    class FuncTH : public Analysis::IFuncTree, public TObject {
    public:
      // ======================================================================
      ClassDef( Analysis::Functions::FuncTH, 1 );
      // ======================================================================
    public:
      // ======================================================================
      /** constructor
       *  @param tree          (INPUT) the tree
       *  @param edges         (INPUT) special tretament of edges?
       *  @param extrapolate   (INPUT) use extrapolation?
       *  @param density       (INPUT) use  density?
       */
      FuncTH( const TTree* tree = nullptr, const bool edges = true, const bool extrapolate = false,
              const bool density = false );
      // ======================================================================
      /// destructor
      virtual ~FuncTH();
      // ======================================================================
    protected:
      // ======================================================================
      mutable const TTree* m_tree{nullptr}; //!
      /// special treatment of edges?
      bool m_edges{true}; // special treatment for edges?
      /// extrapolate?
      bool m_extrapolate{false}; // extrapolate ?
      /// density?
      bool m_density{false}; // density ?
      // ======================================================================
    };
    // ========================================================================
    /** @class FuncTH1
     *  simple implementation of TTree-function based on TH1
     */
    class FuncTH1 : public FuncTH {
    public:
      // ======================================================================
      ClassDef( Analysis::Functions::FuncTH1, 1 );
      // ======================================================================
    public:
      // ======================================================================
      /** constructor from the histogram
       *  @param histo         (INPUT) the historgam
       *  @param xvar          (INPUT) the expression/variable
       *  @param tree          (INPUT) the tree
       *  @param tx            (INPUT) interpolation type
       *  @param edges         (INPUT) special tretament of edges?
       *  @param extrapolate   (INPUT) use extrapolation?
       *  @param density       (INPUT) use  density?
       */
      FuncTH1( const TH1F& histo, const std::string& xvar, const TTree* tree = nullptr,
               const Gaudi::Math::HistoInterpolation::Type tx = Gaudi::Math::HistoInterpolation::Cubic,
               const bool edges = true, const bool extrapolate = false, const bool density = false );
      // ======================================================================
      /** constructor from the histogram
       *  @param histo         (INPUT) the historgam
       *  @param xvar          (INPUT) the expression/variable
       *  @param tree          (INPUT) the tree
       *  @param tx            (INPUT) interpolation type
       *  @param edges         (INPUT) special tretament of edges?
       *  @param extrapolate   (INPUT) use extrapolation?
       *  @param density       (INPUT) use  density?
       */
      FuncTH1( const TH1D& histo, const std::string& xvar, const TTree* tree = nullptr,
               const Gaudi::Math::HistoInterpolation::Type tx = Gaudi::Math::HistoInterpolation::Cubic,
               const bool edges = true, const bool extrapolate = false, const bool density = false );
      // ======================================================================
      /// default constructor, needed for serialization
      FuncTH1() = default;
      /// destructor
      virtual ~FuncTH1();
      // ======================================================================
    protected: // private constructor without histogram
      // ======================================================================
      /** constructor without histogram
       *  @param xvar          (INPUT) the expression/variable
       *  @param tree          (INPUT) the tree
       *  @param tx            (INPUT) interpolation type
       *  @param edges         (INPUT) special tretament of edges?
       *  @param extrapolate   (INPUT) use extrapolation?
       *  @param density       (INPUT) use  density?
       */
      FuncTH1( const std::string& xvar, const TTree* tree = nullptr,
               const Gaudi::Math::HistoInterpolation::Type tx = Gaudi::Math::HistoInterpolation::Cubic,
               const bool edges = true, const bool extrapolate = false, const bool density = false );
      // ======================================================================
    public:
      // ======================================================================
      ///  evaluate the formula for  TTree
      double operator()( const TTree* tree = nullptr ) const override;
      // ======================================================================
    public:
      // ======================================================================
      Bool_t Notify() override { return notify(); }
      bool   notify() const;
      // ======================================================================
    private:
      // ======================================================================
      /// make x-var
      bool make_xvar() const;
      // ======================================================================
    protected:
      // ======================================================================
      mutable std::unique_ptr<Analysis::Formula> m_xvar{nullptr}; //!
      // ======================================================================
      /// the  expression for x-variable
      std::string m_xvar_exp{}; // the  expression for x-variable
      /// interpolation type
      Gaudi::Math::HistoInterpolation::Type m_tx{Gaudi::Math::HistoInterpolation::Cubic}; // the interpolation type
      // ======================================================================
    private:
      // ======================================================================
      ///  the histogram
      TH1D m_h1{}; // the histogram
      // ======================================================================
    };
    // ========================================================================
    /** @class FuncTH2
     *  simple implementation of TTree-function based on TH2
     */
    class FuncTH2 : public FuncTH {
    public:
      // ======================================================================
      ClassDef( Analysis::Functions::FuncTH2, 1 );
      // ======================================================================
    public:
      // ======================================================================
      /** constructor from the histogram
       *  @param histo         (INPUT) the historgam
       *  @param xvar          (INPUT) the expression/variable
       *  @param yvar          (INPUT) the expression/variable
       *  @param tree          (INPUT) the tree
       *  @param tx            (INPUT) interpolation type
       *  @param ty            (INPUT) interpolation type
       *  @param edges         (INPUT) special tretament of edges?
       *  @param extrapolate   (INPUT) use extrapolation?
       *  @param density       (INPUT) use  density?
       */
      FuncTH2( const TH2F& histo, const std::string& xvar, const std::string& yvar, const TTree* tree = nullptr,
               const Gaudi::Math::HistoInterpolation::Type tx = Gaudi::Math::HistoInterpolation::Cubic,
               const Gaudi::Math::HistoInterpolation::Type ty = Gaudi::Math::HistoInterpolation::Cubic,
               const bool edges = true, const bool extrapolate = false, const bool density = false );
      // ======================================================================
      /** constructor from the histogram
       *  @param histo         (INPUT) the historgam
       *  @param xvar          (INPUT) the expression/variable
       *  @param yvar          (INPUT) the expression/variable
       *  @param tree          (INPUT) the tree
       *  @param tx            (INPUT) interpolation type
       *  @param ty            (INPUT) interpolation type
       *  @param edges         (INPUT) special tretament of edges?
       *  @param extrapolate   (INPUT) use extrapolation?
       *  @param density       (INPUT) use  density?
       */
      FuncTH2( const TH2D& histo, const std::string& xvar, const std::string& yvar, const TTree* tree = nullptr,
               const Gaudi::Math::HistoInterpolation::Type tx = Gaudi::Math::HistoInterpolation::Cubic,
               const Gaudi::Math::HistoInterpolation::Type ty = Gaudi::Math::HistoInterpolation::Cubic,
               const bool edges = true, const bool extrapolate = false, const bool density = false );
      // ======================================================================
      /// default constructor, needed for serialization
      FuncTH2() = default;
      /// destructor
      virtual ~FuncTH2();
      // ======================================================================
    protected: // private constructor withtou histogram
      // ======================================================================
      /** constructor without histogram
       *  @param histo         (INPUT) the historgam
       *  @param xvar          (INPUT) the expression/variable
       *  @param yvar          (INPUT) the expression/variable
       *  @param tree          (INPUT) the tree
       *  @param tx            (INPUT) interpolation type
       *  @param ty            (INPUT) interpolation type
       *  @param edges         (INPUT) special tretament of edges?
       *  @param extrapolate   (INPUT) use extrapolation?
       *  @param density       (INPUT) use  density?
       */
      FuncTH2( const std::string& xvar, const std::string& yvar, const TTree* tree = nullptr,
               const Gaudi::Math::HistoInterpolation::Type tx = Gaudi::Math::HistoInterpolation::Cubic,
               const Gaudi::Math::HistoInterpolation::Type ty = Gaudi::Math::HistoInterpolation::Cubic,
               const bool edges = true, const bool extrapolate = false, const bool density = false );
      // ======================================================================
    public:
      // ======================================================================
      ///  evaluate the formula for  TTree
      double operator()( const TTree* tree = nullptr ) const override;
      // ======================================================================
    public:
      // ======================================================================
      Bool_t Notify() override { return notify(); }
      bool   notify() const;
      // ======================================================================
    protected:
      // ======================================================================
      /// make x-var
      bool make_xvar() const;
      /// make y-var
      bool make_yvar() const;
      // ======================================================================
    protected:
      // ======================================================================
      mutable std::unique_ptr<Analysis::Formula> m_xvar{nullptr}; //!
      mutable std::unique_ptr<Analysis::Formula> m_yvar{nullptr}; //!
      // ======================================================================
      /// the  expression for x-variable
      std::string m_xvar_exp{}; // the  expression for x-variable
      /// the  expression for y-variable
      std::string m_yvar_exp{}; // the  expression for y-variable
      /// interpolation type
      Gaudi::Math::HistoInterpolation::Type m_tx{Gaudi::Math::HistoInterpolation::Cubic}; // the interpolation type
      /// interpolation type
      Gaudi::Math::HistoInterpolation::Type m_ty{Gaudi::Math::HistoInterpolation::Cubic}; // the interpolation type
      // ======================================================================
    private:
      // ======================================================================
      ///  the histogram
      TH2D m_h2{}; // the histogram
      // ======================================================================
    };
    // ========================================================================
    /** @class FuncTH3
     *  simple implementation of TTree-function based on TH3
     */
    class FuncTH3 : public FuncTH {
    public:
      // ======================================================================
      ClassDef( Analysis::Functions::FuncTH3, 1 );
      // ======================================================================
    public:
      // ======================================================================
      /** constructor from the histogram
       *  @param histo         (INPUT) the historgam
       *  @param xvar          (INPUT) the expression/variable
       *  @param yvar          (INPUT) the expression/variable
       *  @param zvar          (INPUT) the expression/variable
       *  @param tree          (INPUT) the tree
       *  @param tx            (INPUT) interpolation type
       *  @param ty            (INPUT) interpolation type
       *  @param tz            (INPUT) interpolation type
       *  @param edges         (INPUT) special tretament of edges?
       *  @param extrapolate   (INPUT) use extrapolation?
       *  @param density       (INPUT) use  density?
       */
      FuncTH3( const TH3F& histo, const std::string& xvar, const std::string& yvar, const std::string& zvar,
               const TTree*                                tree = nullptr,
               const Gaudi::Math::HistoInterpolation::Type tx   = Gaudi::Math::HistoInterpolation::Cubic,
               const Gaudi::Math::HistoInterpolation::Type ty   = Gaudi::Math::HistoInterpolation::Cubic,
               const Gaudi::Math::HistoInterpolation::Type tz   = Gaudi::Math::HistoInterpolation::Cubic,
               const bool edges = true, const bool extrapolate = false, const bool density = false );
      // ======================================================================
      /** constructor from the histogram
       *  @param histo         (INPUT) the historgam
       *  @param xvar          (INPUT) the expression/variable
       *  @param yvar          (INPUT) the expression/variable
       *  @param zvar          (INPUT) the expression/variable
       *  @param tree          (INPUT) the tree
       *  @param tx            (INPUT) interpolation type
       *  @param ty            (INPUT) interpolation type
       *  @param tz            (INPUT) interpolation type
       *  @param edges         (INPUT) special tretament of edges?
       *  @param extrapolate   (INPUT) use extrapolation?
       *  @param density       (INPUT) use  density?
       */
      FuncTH3( const TH3D& histo, const std::string& xvar, const std::string& yvar, const std::string& zvar,
               const TTree*                                tree = nullptr,
               const Gaudi::Math::HistoInterpolation::Type tx   = Gaudi::Math::HistoInterpolation::Cubic,
               const Gaudi::Math::HistoInterpolation::Type ty   = Gaudi::Math::HistoInterpolation::Cubic,
               const Gaudi::Math::HistoInterpolation::Type tz   = Gaudi::Math::HistoInterpolation::Cubic,
               const bool edges = true, const bool extrapolate = false, const bool density = false );
      // ======================================================================
      /// default constructor, needed for serialization
      FuncTH3() = default;
      /// destructor
      virtual ~FuncTH3();
      // ======================================================================
    protected: // private constructor withtou histogram
      // ======================================================================
      /** constructor without histogram
       *  @param xvar          (INPUT) the expression/variable
       *  @param yvar          (INPUT) the expression/variable
       *  @param zvar          (INPUT) the expression/variable
       *  @param tree          (INPUT) the tree
       *  @param tx            (INPUT) interpolation type
       *  @param ty            (INPUT) interpolation type
       *  @param tz            (INPUT) interpolation type
       *  @param edges         (INPUT) special tretament of edges?
       *  @param extrapolate   (INPUT) use extrapolation?
       *  @param density       (INPUT) use  density?
       */
      FuncTH3( const std::string& xvar, const std::string& yvar, const std::string& zvar, const TTree* tree = nullptr,
               const Gaudi::Math::HistoInterpolation::Type tx = Gaudi::Math::HistoInterpolation::Cubic,
               const Gaudi::Math::HistoInterpolation::Type ty = Gaudi::Math::HistoInterpolation::Cubic,
               const Gaudi::Math::HistoInterpolation::Type tz = Gaudi::Math::HistoInterpolation::Cubic,
               const bool edges = true, const bool extrapolate = false, const bool density = false );
      // ======================================================================
    public:
      // ======================================================================
      ///  evaluate the formula for  TTree
      double operator()( const TTree* tree = nullptr ) const override;
      // ======================================================================
    public:
      // ======================================================================
      Bool_t Notify() override { return notify(); }
      bool   notify() const;
      // ======================================================================
    protected:
      // ======================================================================
      /// make x-var
      bool make_xvar() const;
      /// make y-var
      bool make_yvar() const;
      /// make z-var
      bool make_zvar() const;
      // ======================================================================
    protected:
      // ======================================================================
      mutable std::unique_ptr<Analysis::Formula> m_xvar{nullptr}; //!
      mutable std::unique_ptr<Analysis::Formula> m_yvar{nullptr}; //!
      mutable std::unique_ptr<Analysis::Formula> m_zvar{nullptr}; //!
      // ======================================================================
      /// the  expression for x-variable
      std::string m_xvar_exp{}; // the  expression for x-variable
      /// the  expression for y-variable
      std::string m_yvar_exp{}; // the  expression for y-variable
      /// the  expression for z-variable
      std::string m_zvar_exp{}; // the  expression for z-variable
      /// interpolation type
      Gaudi::Math::HistoInterpolation::Type m_tx{Gaudi::Math::HistoInterpolation::Cubic}; // the interpolation type
      /// interpolation type
      Gaudi::Math::HistoInterpolation::Type m_ty{Gaudi::Math::HistoInterpolation::Cubic}; // the interpolation type
      /// interpolation type
      Gaudi::Math::HistoInterpolation::Type m_tz{Gaudi::Math::HistoInterpolation::Cubic}; // the interpolation type
      // ======================================================================
    private:
      // ======================================================================
      ///  the histogram
      TH3D m_h3{}; // the histogram
      // ======================================================================
    };
    // ========================================================================
    /** @class FuncRooFormula
     *  simple implementation of 'RooAbsData'-function based on RooFormulaVar
     */
    class FuncRooFormula : public Analysis::IFuncData {
    public:
      // ======================================================================
      /** constructor from the formula expression
       *  @param expression the formula expression
       *  @param data       the data
       *  @param name       the name for the formula
       */
      FuncRooFormula( const std::string& expression, const RooAbsData* data = nullptr, const std::string& name = "" );
      // ======================================================================
      /// default constructor, needed for serialization
      FuncRooFormula() = default;
      /// destructor
      virtual ~FuncRooFormula();
      // ======================================================================
    public:
      // ======================================================================
      ///  evaluate the formula for  data
      double operator()( const RooAbsData* data = nullptr ) const override;
      // ======================================================================
    private: // make the formula
      // ======================================================================
      bool make_formula() const;
      // ======================================================================
    private:
      // ======================================================================
      mutable const RooAbsData*              m_data{nullptr};    //!
      mutable std::unique_ptr<RooFormulaVar> m_formula{nullptr}; //!
      // ======================================================================
      /// the  expression itself
      std::string m_expression{}; // the  expression itself
      /// the name
      std::string m_name{}; // the name
      // ======================================================================
    };
    // ========================================================================
  } // namespace Functions
  // ==========================================================================
} // namespace Analysis
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // OSTAP_FUNCS_H
