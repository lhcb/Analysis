/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id$
// ============================================================================
#ifndef ANALYSIS_ITERATOR_H
#define ANALYSIS_ITERATOR_H 1
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/Kernel.h"
// ============================================================================
// Forward deslaration
// ============================================================================
class TObject;          // ROOT
class TCollection;      // ROOT
class TIterator;        // ROOT
class RooLinkedList;    // RooFit
class RooAbsCollection; // RooFit
// ============================================================================
namespace Analysis {
  // ==========================================================================
  /** @class Iterator
   *  helper class to simplify manipulations with ROOT-iterator
   *  @see TIterator
   *  @author Vanay BELYAEV  Ivan.Belyaev@=itep.ru
   *  @date 2015-02-015
   */
  class GAUDI_API Iterator {
  public:
    // =========================================================================
    /// standard constructor: create and keep the ietrator
    Iterator( const RooAbsCollection& collection );
    /// standard constructor: create and keep the ietrator
    Iterator( const TCollection& collection );
    /// standard constructor: create and keep the ietrator
    Iterator( const RooLinkedList& collection );
    /// default constructor
    Iterator();
    /// destructor
    ~Iterator();
    // =========================================================================
  public:
    // ========================================================================
    /// invoke TIterator::Next
    TObject* next() const; // invoke TIterator::Next
    /// invoke TIterator::Reset
    bool reset() const; // invoke TIterator::Reset
    // =========================================================================
  public: // aliases
    // ========================================================================
    // alias:
    TObject* Next() const { return next(); } // invoke TIterator::Next
    // alias:
    bool Reset() const { return reset(); } // invoke TIterator::Reset
    // =========================================================================
  public: // some pointer alchemistry
    // ========================================================================
    /// valid iterator
    bool valid() const { return 0 != m_iterator; }
    /// invalid ?
    bool operator!() const { return !valid(); }
    /// conversion to underlying type
    inline TIterator* operator->() const { return m_iterator; }
    // =========================================================================
  public:
    // =========================================================================
    /** iterate and cast to specific type
     */
    template <class T>
    T* dynamic_next() const {
      TObject* r = this->Next();
      return ( nullptr == r ) ? nullptr : dynamic_cast<T*>( r ); // RETURN
    }
    // ========================================================================
    /** iterate and cast to specific type
     */
    template <class T>
    T* static_next() const {
      TObject* r = this->Next();
      return ( nullptr == r ) ? nullptr : static_cast<T*>( r ); // RETURN
    }
    // =========================================================================
  private:
    // ========================================================================
    /// copy constructor is disabled
    Iterator( const Iterator& ); // copy constructor is disabled
    /// assignement is disabled
    Iterator& operator=( const Iterator& ); // assignement is disabled
    // =========================================================================
  private:
    // ========================================================================
    /// iterator itself
    TIterator* m_iterator; // iterator itself
    // ========================================================================
  };
  // ==========================================================================
} // end of namespace Analysis
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // ANALYSIS_ITERATOR_H
