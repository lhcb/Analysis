/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef OSTAP_PYFUNCS_H
#define OSTAP_PYFUNCS_H 1
// ============================================================================
// Include files
// ============================================================================
// Ostap
// ============================================================================
#include "Analysis/IFuncs.h"
// ============================================================================
// Python
// ============================================================================
#ifdef _POSIX_C_SOURCE
#  undef _POSIX_C_SOURCE
#endif

#ifdef _XOPEN_SOURCE
#  undef _XOPEN_SOURCE
#endif

#include "Python.h"
// ============================================================================
namespace Analysis {
  // ==========================================================================
  namespace Functions {
    // ========================================================================
    /** @class PyFuncTree
     *  Helper class to implement "TTree-functions" in python
     *  @see Analysis::IFuncTree
     */
    class PyFuncTree : public Analysis::IFuncTree {
    public:
      // ======================================================================
      /** constructor
       *  @param self python object
       *  @param tree pointer to the tree
       */
      PyFuncTree( PyObject* self, const TTree* tree = 0 );
      // ======================================================================
      /// destructor
      virtual ~PyFuncTree();
      // ======================================================================
    public:
      // ======================================================================
      /// the basic
      double operator()( const TTree* tree = 0 ) const override;
      // ======================================================================
    public:
      // ======================================================================
      /// get the pointer to TTree
      const TTree* tree() const { return m_tree; }
      // ======================================================================
    private:
      // ======================================================================
      /// potentially cached pointer to the tree
      mutable const TTree* m_tree;
      // ======================================================================
    private:
      // ======================================================================
      // self reference for python instance
      PyObject* m_self; // self reference for python instance
      // ======================================================================
    };
    // ========================================================================
    /** @class PyFuncData
     *  Helper class to implement "RooAbsData-functions" in python
     *  @see Analysis::IFuncData
     */
    class PyFuncData : public Analysis::IFuncData {
    public:
      // ======================================================================
      /** constructor
       *  @param self python object
       *  @param data pointer to the data
       */
      PyFuncData( PyObject* self, const RooAbsData* data = 0 );
      // ======================================================================
      /// destructor
      virtual ~PyFuncData();
      // ======================================================================
    public:
      // ======================================================================
      /// the basic
      double operator()( const RooAbsData* data = 0 ) const override;
      // ======================================================================
    public:
      // ======================================================================
      /// get the pointer to TTree
      const RooAbsData* data() const { return m_data; }
      // ======================================================================
    private:
      // ======================================================================
      /// potentially cached pointer to the tree
      mutable const RooAbsData* m_data;
      // ======================================================================
    private:
      // ======================================================================
      // self reference for python instance
      PyObject* m_self; // self-reference for python instance
                        // ======================================================================
    };
    // ========================================================================
  } // namespace Functions
  // ==========================================================================
} // namespace Analysis
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // OSTAP_PYFUNCS_H
