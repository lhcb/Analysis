/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef OSTAP_IFUNCS_H
#  define OSTAP_IFUNCS_H 1
// ============================================================================
// Include files
// ============================================================================
// Forward declarations
// ============================================================================
class TTree;      // From ROOT
class RooAbsData; // From RooFit
// ============================================================================
namespace Analysis {
  // ==========================================================================
  /** @class ITreeFunc
   *  Helper abstract class to evaluate certain TTree-functions
   */
  class IFuncTree {
  public:
    // ========================================================================
    /// evaluate the function from TTree
    virtual double operator()( const TTree* tree = nullptr ) const = 0;
    /// virtual destructor
    virtual ~IFuncTree();
    // ========================================================================
  };
  // ==========================================================================
  /** @class IFuncData
   *  Helper abstract class to evaluate certain RooAbsData-functions
   */
  class IFuncData {
  public:
    // ========================================================================
    /// evaluate the function from TTree
    virtual double operator()( const RooAbsData* tree = nullptr ) const = 0;
    /// virtual destructor
    virtual ~IFuncData();
    // ========================================================================
  };
  // ==========================================================================
} // namespace Analysis
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // OSTAP_IFUNCS_H
// ============================================================================
