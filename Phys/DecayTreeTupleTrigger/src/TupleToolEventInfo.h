/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: TupleToolEventInfo.h,v 1.5 2010-06-02 12:01:44 pkoppenb Exp $
#ifndef JBOREL_TUPLETOOLEVENTINFO_H
#define JBOREL_TUPLETOOLEVENTINFO_H 1

// Include files
// from Gaudi
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "Kernel/IEventTupleTool.h" // Interface

class ITupleTool;
struct ILHCbMagnetSvc;

/** @class TupleToolEventInfo TupleToolEventInfo.h jborel/TupleToolEventInfo.h
 *
 * \brief Event and Run number for DecayTreeTuple
 *
 * Tuple columns:
 * - "runNumber" : "well, the run number"
 * - "eventNumber" : "well, the event number"
 * - "BCID" : "bunch crossing id"
 * - "BCType" : "Bunch Crossing type: 0 = NoBeam,  1 = Beam1,  2 = Beam2,   3 = Beam Crossing"
 * - "OdinTCK" : "Odin Trigger Configuration Keys"
 * - "L0DUTCK" : "L0DU Trigger Configuration Keys"
 * - "HLT1TCK" : "HLT1 Trigger Configuration Keys"
 * - "HLT2TCK" : "HLT2 Trigger Configuration Keys"
 * - "GPSTime" : "GPS time in microseconds"
 * - "Polarity" : "Magnet polarity, +1 = Up, -1 = Down"
 * - "Verbose":
 *   - "TriggerType" : "type of trigger broadcasted by ODIN, LHCb::ODIN::TriggerType"
 *   - "GpsYear" : "GPS time in years"
 *   - "GpsMonth" : "GPS time in months"
 *   - "GpsDay" : "GPS time in days"
 *   - "GpsHour" : "GPS time in hours"
 *   - "GpsMinute" : "GPS time in minutes"
 *   - "GpsSecond" : "GPS time in seconds"
 *
 * Note that months are numbered [0-11]. That's a convention. Sorry.
 *
 * \sa DecayTreeTuple
 *  @author Jeremie Borel
 *  @date   2007-11-07
 */
class TupleToolEventInfo : public TupleToolBase, virtual public IEventTupleTool {

public:
  /// Standard constructor
  TupleToolEventInfo( const std::string& type, const std::string& name, const IInterface* parent );

  ~TupleToolEventInfo(){}; ///< Destructor

  StatusCode initialize() override;
  StatusCode fill( Tuples::Tuple& ) override;

private:
  ILHCbMagnetSvc*                  m_magSvc;
  typedef std::vector<std::string> AlgorithmNames;
  Gaudi::Property<AlgorithmNames>  m_algNames{this, "Algorithms"}; ///< List of algorithm(s) to include in timing
};
#endif // JBOREL_TUPLETOOLEVENTINFO_H
