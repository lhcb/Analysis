/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: TupleToolStripping.h,v 1.1 2010-04-26 12:49:59 pkoppenb Exp $
#ifndef TUPLETOOLSTRIPPING_H
#define TUPLETOOLSTRIPPING_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"

#include "Kernel/IEventTupleTool.h" // Interface
#include "TupleToolTriggerBase.h"

/** @class TupleToolStripping TupleToolStripping.h
 *\brief Add stripping line decision to the tuple
 *
 *  @author Patrick Koppenburg
 *  @date   2010-04-23
 */
class TupleToolStripping : public TupleToolTriggerBase, virtual public IEventTupleTool {

public:
  /// Standard constructor
  TupleToolStripping( const std::string& type, const std::string& name, const IInterface* parent );

  ~TupleToolStripping(); ///< Destructor

  StatusCode initialize() override;

  using TupleToolTriggerBase::fill;
  StatusCode fill( Tuples::Tuple& tuple ) override;

private:
  std::string m_location; ///< HDR location

  /// property: list of stripping lines to specifically look at
  std::vector<std::string> m_strippingList;
};

#endif // TUPLETOOLSTRIPPING_H
