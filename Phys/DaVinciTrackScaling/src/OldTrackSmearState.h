/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
#ifndef _OldTrackSmearState_H_
#define _OldTrackSmearState_H_

/** @class TrackSmearState TrackSmearState.h
 *
 * Smear the state
 *
 *  @author M.Needham
 *  @date   30/05/2006
 */

#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/IRndmGen.h"
#include "GaudiKernel/SmartIF.h"

#include <string>
#include <vector>

class TGraphErrors;

class OldTrackSmearState : public GaudiAlgorithm {

public:
  // Constructors and destructor
  OldTrackSmearState( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~OldTrackSmearState();

  StatusCode initialize() override;

  StatusCode execute() override;

private:
  std::string m_inputRootFile;
  std::string m_inputLocation;
  std::string m_year;

  double        m_smearAngle;
  double        m_fudgeFactor;
  TGraphErrors* m_graph;

  IRndmGen* m_gaussDist = nullptr;
};

#endif
