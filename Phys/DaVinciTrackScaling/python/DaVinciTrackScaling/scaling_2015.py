#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
# @file scaling_2015.py
#
# Momentum scaling for 2015
#
# @author Matt  NEEDHAM
# @author Vanya BELYAEV Ivan.Belyaev@itep.ru
# @date   2016-01-11
# ============================================================================
"""Momentum scaling fro 2015

Input data are specified close to the start of the script

- input ROOT file with historams
the file should have two 2D-histograms, named as 'idp-plus' and 'idp-minus'

- global delta
- list of run-dependent offsets as list of triplets:  [ (start,stop,offset) ],
where (start,stop) represent the run range (both edges are inclusive)

As the output xml-file MomentumScale.xml is generated in cwd

"""
# ============================================================================
__author__ = "Vanya BELYAEV Ivan.Belyaev@itep.ru"
__date__ = "2016-01-11"
__version__ = ""
# ============================================================================
import ROOT, Ostap.PyRoUts
# ============================================================================
from momentum_scale import buildXML, logger
# ============================================================================
logger.info(100 * '*')
logger.info(__doc__)
logger.info(100 * '*')
# ============================================================================

# ============================================================================
## the histograms
with ROOT.TFile('../../data/BFitResults-fine-2015-new.root',
                'READ') as the_file:
    idp_plus = the_file.Get('idp-plus').clone()
    idp_minus = the_file.Get('idp-minus').clone()
    logger.info('HISTOS are read from %s' % the_file)

# =============================================================================
## global delta
## delta =  -0.165063e-3
delta = -0.075e-3
logger.info('Global delta: %s' % delta)

## //down
## if (run >= 162200   && run <= 163200 ) alpha = 0.0;
## //gap
## else if (run >= 163250  && run <= 164000 )  alpha = 0.0406160e-3 ;
## else if (run >= 164001  && run <= 164500 )  alpha = -0.0405840e-3;
## else if (run >= 164501  && run <= 164900 )  alpha = -0.0751180e-3;
## else if (run >= 164901  && run <= 165200 )  alpha =  -0.0706750e-3;
## //gap
## else if (run >= 165201  && run <= 165400 ) alpha =  -0.0708270e-3  ;
## else if (run >= 165401  && run <= 165600 ) alpha =  -0.0670200e-3 ;
## // up
## else if (run >= 166250  && run <= 166400 ) alpha = 0.0940130e-3 ;
## else if (run >= 166401  && run <= 166550 ) alpha = 0.0539590e-3 ;
## // gap
## else if (run >= 166600  && run <= 166700 ) alpha = 0.0157780e-3  ;
## else if (run >= 166700  && run <= 166800 ) alpha = 0.0421130e-3 ;
## // gap
## else if (run >= 166850  && run <= 167000 ) alpha =  -0.0553680e-3 ;
## else if (run >= 167000  && run <= 167150 ) alpha =  -0.0373790e-3;

## Run-dependent offsets
offsets = [
    ## start stop offset
    (163250, 164000, 0.0406160e-3),
    (164001, 164500, -0.0405840e-3),
    (164501, 164900, -0.0751180e-3),
    (164901, 165200, -0.0706750e-3),
    ##
    (165201, 165400, -0.0708270e-3),
    (165401, 165600, -0.0670200e-3),
    ##
    (166250, 166400, 0.0940130e-3),
    (166401, 166550, 0.0539590e-3),
    ##
    (166600, 166700, 0.0157780e-3),
    (166700, 166800, 0.0421130e-3),
    ##
    (166850, 167000, -0.0553680e-3),
    (167000, 167150, -0.0373790e-3)
]

# =============================================================================
# Build XML-document
# =============================================================================
buildXML(
    year='2k+15',
    reco='Reco16',
    idp_plus=idp_plus,
    idp_minus=idp_minus,
    offsets=offsets,
    delta=delta)

# ============================================================================
# The END
# ============================================================================
