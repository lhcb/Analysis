#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
# @file scaling_2018a.py
# Momentum scaling for 2018 (before the technical stop) IOV < 2018-06-14 10:00
# @author Matt  NEEDHAM
# @author Vanya BELYAEV Ivan.Belyaev@itep.ru
# @date   2018-11-22
# ============================================================================
"""Momentum scaling for 2018 (before the technical stop) IOV < 2018-06-14 10:00

Input data are specified close to the start of the script

- input ROOT file with historams
the file should have two 2D-histograms, named as 'idp-plus' and 'idp-minus'

- global delta
- list of run-dependent offsets as list of triplets:  [ (start,stop,offset) ],
where (start,stop) represent the run range (both edges are inclusive)

As the output xml-file MomentumScale.xml is generated in cwd

"""
# ============================================================================
__author__ = "Vanya BELYAEV Ivan.Belyaev@itep.ru"
__date__ = "2018-11-22"
__version__ = ""
# ============================================================================
import ROOT, Ostap.PyRoUts
# ============================================================================
from momentum_scale import buildXML, logger
# ============================================================================
logger.info(100 * '*')
logger.info(__doc__)
logger.info(100 * '*')
# ============================================================================

# ============================================================================
## the histograms
with ROOT.TFile('../../data/BFitResults-fine-2018-both.root',
                'READ') as the_file:
    ## KEY: TH2D     idp-plus-post;1 idp-plus
    ## KEY: TH2D     idp-minus-post;1        idp-minus
    ## KEY: TH2D     idp-plus-pre;1  idp-plus
    ## KEY: TH2D     idp-minus-pre;1 idp-minus
    logger.info("use ``pre''-histograms!")
    idp_plus = the_file.Get('idp-plus-pre').clone()
    idp_minus = the_file.Get('idp-minus-pre').clone()
    logger.info('HISTOS are read from %s' % the_file)

# =============================================================================
## global delta
delta = -0.115e-3
logger.info('Global delta: %s' % delta)

## ==========================================================================
## CalScale2018both.h
## ==========================================================================

## double alpha = 0;
## if (run >= 205860   && run <=  206450 ) alpha= -0.151e-3;

## // up
## if (run >= 206500   && run <=  207100 ) alpha= 0.112e-3 ;
## if (run >= 207101   && run <=  207350 ) alpha= 0.175e-3;
## if (run >= 207600   && run <=  208540 ) alpha= 0.148e-3;

## //down
## if (run >= 208560  && run <= 209200) alpha= 0.180e-3;
## if (run >= 209201  && run <= 209550) alpha=0.013e-3;
## if (run >= 209551  && run <= 209850) alpha= 0.0018e-3;
## if (run >= 209851  && run <= 210230) alpha= 0.0295e-3;

## //down TS
## if (run >= 210600  && run <= 211100) alpha= 0.0567e-3;

## // up
## if (run >= 211200  && run <= 212500) alpha=0.354875e-3;
## if (run >= 212501  && run <= 212750) alpha=0.42e-3;
## if (run >= 212751  && run <= 213000) alpha= 0.4677e-3;
## if (run >= 213001  && run <= 213200) alpha=0.513e-3 ;
## if (run >= 213201  && run <= 213423) alpha= 0.5675e-3;

## // down
## if (run >= 213425  && run <= 213800) alpha=  0.5844e-3;
## if (run >= 213801  && run <= 214300) alpha=  0.4088e-3;
## if (run >= 214301  && run <= 214745) alpha= 0.4728e-3;

## // down
## if (run >= 215500  && run <= 215948) alpha= 0.3977e-3 ;

## // up
## if (run >= 215949  && run <= 216629)  alpha= 0.4177e-3;
## if (run >= 216630  && run <= 217120) alpha= 0.;

## return alpha;

offsets = [
    (205860, 206450, -0.151e-3),
    ## up
    (206500, 207100, 0.112e-3),
    (207101, 207350, 0.175e-3),
    (207600, 208540, 0.148e-3),
    ##  down
    (208560, 209200, 0.180e-3),
    (209201, 209550, 0.013e-3),
    (209551, 209850, 0.0018e-3),
    (209851, 210230, 0.0295e-3),
    ## down TS
    (210600, 211100, 0.0567e-3),
    ## up
    (211200, 212500, 0.354875e-3),
    (212501, 212750, 0.42e-3),
    (212751, 213000, 0.4677e-3),
    (213001, 213200, 0.513e-3),
    (213201, 213423, 0.5675e-3),
    ## down
    (213425, 213800, 0.5844e-3),
    (213801, 214300, 0.4088e-3),
    (214301, 214745, 0.4728e-3),
    ## down
    (215500, 215948, 0.3977e-3),
    ## up
    (215949, 216629, 0.4177e-3),
    (216630, 217120, 0.)
]

# =============================================================================
# Build XML-document
# =============================================================================
buildXML(
    year='2k+18(pre)',
    reco='Reco18',
    idp_plus=idp_plus,
    idp_minus=idp_minus,
    offsets=offsets,
    delta=delta)

# ============================================================================
# The END
# ============================================================================
