#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
# @file momentum_scale.py
#
# Helper script to convert input data (histograms + offsets) to XML-format
#
# Input data are specified close to the start of the script:
#
#  - input ROOT file with historams
#     the file should have two 2D-histograms, named as 'idp-plus' and 'idp-minus'
#
#  - global delta
#  - list of run-dependent offsets as list of triplets:  [ (start,stop,offset) ],
#     where (start,stop) represent the run range (both edges are inclusive)
#
#  As the output xml-file MomentumScale.xml is generated in cwd
#
# @author Vanya BELYAEV Ivan.Belyaev@itep.ru
# @date   2013-04-20
# @see TrackScaleState
#
# ============================================================================
"""
Helper script to convert input data (histograms + offsets) to XML-format

Input data are specified close to the start of the script

  - input ROOT file with historams
     the file should have two 2D-histograms, named as 'idp-plus' and 'idp-minus'

  - global delta
  - list of run-dependent offsets as list of triplets:  [ (start,stop,offset) ],
     where (start,stop) represent the run range (both edges are inclusive)

  As the output xml-file MomentumScale.xml is generated in cwd


"""
from __future__ import print_function
# ============================================================================
__author__ = "Vanya BELYAEV Ivan.Belyaev@itep.ru"
__date__ = "2013-04-20"
__version__ = "$Revision:$"
__all__ = ('logger', 'buildXML', 'offsets_to_histo')
# ============================================================================
import ROOT
from Ostap.PyRoUts import h1_axis, VE
# ============================================================================
from Ostap.Logger import getLogger
logger = getLogger('MomentumScale')

# ============================================================================


# ============================================================================
## convert run offsets into the histogram
#  @param offset the list of run-dependes offsets: [ (start,stop,offset) ]
#  @param title (optional) the histogram title
#  @code
#   offsets_2011 = [
#     ## start   stop     offset
#     ( 89333  , 90256  , 9.97629e-05 ) ,
#     ( 90257  , 90763  , 6.01683e-06 ) ,
#     ( 91556  , 93282  , 5.33486e-05 ) ,
#     ( 93398  , 94386  , 0.000183575 ) ,
#     ]
#   histo = offsets_to_histo ( offets_2011 )
# @endcode
# @author Vanya BELYAEV Ivan.Belyaev@itep.ru
# @date   2013-04-20
#
def offsets_to_histo(offsets, title=''):
    """Helper function to convert the offset list into histogram

    offsets_2011 = [
    ## start   stop     offset
    ( 89333  , 90256  , 9.97629e-05 ) ,
    ( 90257  , 90763  , 6.01683e-06 ) ,
    ( 91556  , 93282  , 5.33486e-05 ) ,
    ( 93398  , 94386  , 0.000183575 ) ,
    ]

    histo = offsets_to_histo ( offets_2011 )

    """
    #
    # make the axis ticks
    ticks = set()
    for i in offsets:
        ticks.add(i[0])
        ticks.add(i[1] + 1)  ## take care about equality

    ## create the histogram with appropriate binnings
    h = h1_axis(list(ticks), double=True, title=title)

    for i in offsets:
        mid = 0.5 * (i[0] + i[1])
        bin = h.findBin(mid)
        h[bin] = VE(i[2], 0)

    return h


line1 = '<?xml version="1.0" encoding="ISO-8859-1"?>'
line2 = '<!DOCTYPE DDDB SYSTEM "../../../DTD/structure.dtd">'


# =============================================================================
# Build XML-document
# =============================================================================
def buildXML(reco, year, idp_plus, idp_minus, offsets, delta):

    import lxml.builder

    E = lxml.builder.ElementMaker()
    DDDB = E.DDDB
    CONDITION = E.condition
    PARAM = E.param

    xml_delta = PARAM(
        str(delta),
        name='Delta',
        type='double',
        comment='Global delta scale         %s %s' % (reco, year))

    xml_idplus = PARAM(
        idp_plus.toString(),
        name="IdpPlus",
        type="Histo2D",
        comment="Calibration for IdpPlus    %s %s" % (reco, year))

    xml_idpminus = PARAM(
        idp_minus.toString(),
        name="IdpMinus",
        type="Histo2D",
        comment="Calibration for IdpMinus   %s %s" % (reco, year))

    xml_offsets = PARAM(
        offsets_to_histo(offsets, 'Run-dependent offsets').toString(),
        name="Offsets",
        type="Histo1D",
        comment="Run-dependent offsets     %s %s" % (reco, year))

    document = DDDB(
        CONDITION(
            xml_delta,
            xml_idplus,
            xml_idpminus,
            xml_offsets,
            ## comment = "Momentum scale            %s %s" % ( reco , year ) ,
            name='MomentumScale'), )

    xml_name = 'MomentumScale.xml'
    xml_file = open(xml_name, 'w')

    print(
        line1 + '\n' + line2 + '\n\n\n' + lxml.etree.tostring(
            document, pretty_print=True),
        file=xml_file)

    xml_file.close()
    logger.info('XML file "%s" is generated in CWD ' % xml_name)


# =============================================================================
# The END
# =============================================================================
