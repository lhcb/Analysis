/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from Gaudi
#include "GaudiKernel/PhysicalConstants.h"
#include "Kernel/ParticleProperty.h"

#include "Event/MCParticle.h"
// local
#include "DaVinciP2VVAngles.h"
#include "MCBu2KstarMuMuAngleCalculator.h"

#include <boost/assign/list_of.hpp>

//-----------------------------------------------------------------------------
// Implementation file for class : MCBu2KstarMuMuAngleCalculator
//
// 2007-08-13 : Thomas Blake
// 2017-12-07 : David Gerick
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory

DECLARE_COMPONENT( MCBu2KstarMuMuAngleCalculator )

using namespace DaVinci::P2VVAngles;

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
MCBu2KstarMuMuAngleCalculator::MCBu2KstarMuMuAngleCalculator( const std::string& type, const std::string& name,
                                                              const IInterface* parent )
    : GaudiTool( type, name, parent ), m_KPiOrigin( 1, "K*(892)+" ) {
  declareInterface<IP2VVMCPartAngleCalculator>( this );
  declareProperty( "KaonPionOrigin", m_KPiOrigin );
  const std::vector<std::string> tmp = boost::assign::list_of( "J/psi(1S)" )( "psi(2S)" )( "B+" );
  declareProperty( "MuonOrigin", m_MuOrigin = tmp );
}

//=============================================================================
// Destructor
//=============================================================================

MCBu2KstarMuMuAngleCalculator::~MCBu2KstarMuMuAngleCalculator() {}

//=============================================================================

StatusCode MCBu2KstarMuMuAngleCalculator::initialize() {

  StatusCode sc = GaudiTool::initialize();
  if ( !sc ) return sc;

  info() << "Initializing Angle Calculator Tool" << endmsg;

  m_mcDecayFinder = tool<IMCDecayFinder>( "MCDecayFinder", this );
  m_ppSvc         = svc<LHCb::IParticlePropertySvc>( "LHCb::ParticlePropertySvc", true );

  std::vector<std::string>::const_iterator istr;

  for ( istr = m_KPiOrigin.begin(); istr != m_KPiOrigin.end(); ++istr ) {
    const LHCb::ParticleProperty* pp = m_ppSvc->find( *istr );
    if ( nullptr != pp ) m_KPiOriginID.push_back( pp->particleID().abspid() );
  }

  for ( istr = m_MuOrigin.begin(); istr != m_MuOrigin.end(); ++istr ) {
    const LHCb::ParticleProperty* pp = m_ppSvc->find( *istr );
    if ( nullptr != pp ) m_MuOriginID.push_back( pp->particleID().abspid() );
  }

  return sc;
}

bool MCBu2KstarMuMuAngleCalculator::hasMother( const LHCb::MCParticle*          particle,
                                               const std::vector<unsigned int>& ids ) const {
  const LHCb::MCParticle* mother = particle->mother();

  if ( nullptr == mother ) return false;
  std::vector<unsigned int>::const_iterator it;

  for ( it = ids.begin(); it != ids.end(); ++it ) {
    if ( ( *it ) == ( mother->particleID().abspid() ) ) return true;
  }

  return false;
}

StatusCode MCBu2KstarMuMuAngleCalculator::daughters( const LHCb::MCParticle* mother ) {
  LHCb::MCParticle::ConstVector                 descendants;
  LHCb::MCParticle::ConstVector::const_iterator iter;

  m_mcDecayFinder->descendants( mother, descendants );

  m_pMuMinus = nullptr;
  m_pMuPlus  = nullptr;
  m_pK       = nullptr;
  m_pPi      = nullptr;

  int absidK = 0, absidPi = 0;

  for ( iter = descendants.begin(); iter != descendants.end(); ++iter ) {

    int pid   = ( *iter )->particleID().pid();
    int absid = ( *iter )->particleID().abspid();

    if ( 13 == pid || 11 == pid ) {
      if ( hasMother( *iter, m_MuOriginID ) ) m_pMuMinus = ( *iter );
    } else if ( -13 == pid || -11 == pid ) {
      if ( hasMother( *iter, m_MuOriginID ) ) m_pMuPlus = ( *iter );
    } else if ( 321 == absid || 310 == absid ) {
      if ( hasMother( *iter, m_KPiOriginID ) ) {
        m_pK   = ( *iter );
        absidK = absid;
      }
    } else if ( 211 == absid || 111 == absid ) {
      if ( hasMother( *iter, m_KPiOriginID ) ) {
        m_pPi   = ( *iter );
        absidPi = absid;
      }
    }
  }

  if ( nullptr == m_pK || nullptr == m_pMuPlus || nullptr == m_pMuMinus || nullptr == m_pPi ) {
    return StatusCode::FAILURE;
  }

  if ( 321 == absidK && 111 != absidPi ) {
    error() << "Cannot use K+ and pi+ as K*+ daughters for angle calculation!" << endmsg;
    return StatusCode::FAILURE; // allow only K+ and pi0...
  }
  if ( 310 == absidK && 211 != absidPi ) {
    error() << "Cannot use KS0 and pi0 as K*+ daughters for angle calculation!" << endmsg;
    return StatusCode::FAILURE; //... or KS0 and pi+ combination
  }

  return StatusCode::SUCCESS;
}

StatusCode MCBu2KstarMuMuAngleCalculator::calculateAngles( const LHCb::MCParticle* particle, double& thetal,
                                                           double& thetak, double& phi ) {
  StatusCode sc = daughters( particle );

  if ( sc.isFailure() ) {
    error() << "Could not find required particles !" << endmsg;
    return sc;
  }

  int motherID = particle->particleID().pid();
  if ( particle->hasOscillated() ) motherID = -motherID;
  bool isBplus = ( motherID > 0 );

  if ( isBplus ) {
    thetal = calculateHelicityPolarAngle( m_pK->momentum(), m_pPi->momentum(), m_pMuPlus->momentum(),
                                          m_pMuMinus->momentum() );

    thetak = calculateHelicityPolarAngle( m_pMuPlus->momentum(), m_pMuMinus->momentum(), m_pK->momentum(),
                                          m_pPi->momentum() );

    phi = calculatePlaneAngle( m_pMuPlus->momentum(), m_pMuMinus->momentum(), m_pK->momentum(), m_pPi->momentum() );
  } else {
    thetal = calculateHelicityPolarAngle( m_pK->momentum(), m_pPi->momentum(), m_pMuMinus->momentum(),
                                          m_pMuPlus->momentum() );

    thetak = calculateHelicityPolarAngle( m_pMuMinus->momentum(), m_pMuPlus->momentum(), m_pK->momentum(),
                                          m_pPi->momentum() );

    phi = calculatePlaneAngle( m_pMuMinus->momentum(), m_pMuPlus->momentum(), m_pK->momentum(), m_pPi->momentum() );
  }

  return sc;
}

StatusCode MCBu2KstarMuMuAngleCalculator::calculateTransversityAngles( const LHCb::MCParticle* particle,
                                                                       double& Theta_tr, double& Phi_tr,
                                                                       double& Theta_V ) {

  StatusCode sc = daughters( particle );

  if ( sc.isFailure() ) {
    error() << "Could not find required particles !" << endmsg;
    return sc;
  }

  int motherID = particle->particleID().pid();
  if ( particle->hasOscillated() ) motherID = -motherID;
  bool isBplus = ( motherID > 0 );

  if ( isBplus ) {
    Theta_V = calculateHelicityPolarAngle( m_pMuPlus->momentum(), m_pMuMinus->momentum(), m_pK->momentum(),
                                           m_pPi->momentum() );

    Theta_tr = calculateThetaTr( m_pMuPlus->momentum(), m_pMuMinus->momentum(), m_pK->momentum(), m_pPi->momentum() );

    Phi_tr = calculatePhiTr( m_pMuPlus->momentum(), m_pMuMinus->momentum(), m_pK->momentum(), m_pPi->momentum() );

  } else {

    Theta_V = calculateHelicityPolarAngle( m_pMuMinus->momentum(), m_pMuPlus->momentum(), m_pK->momentum(),
                                           m_pPi->momentum() );

    Theta_tr = calculateThetaTr( m_pMuMinus->momentum(), m_pMuPlus->momentum(), m_pK->momentum(), m_pPi->momentum() );

    Phi_tr = calculatePhiTr( m_pMuMinus->momentum(), m_pMuPlus->momentum(), m_pK->momentum(), m_pPi->momentum() );
  }

  return StatusCode::SUCCESS;
}

double MCBu2KstarMuMuAngleCalculator::calculatePhi( const LHCb::MCParticle* particle ) {

  StatusCode sc = daughters( particle );

  if ( sc.isFailure() ) { Exception( "Could not find required particles !" ); }

  int motherID = particle->particleID().pid();
  if ( particle->hasOscillated() ) motherID = -motherID;
  bool isBplus = ( motherID > 0 );

  double phi = -999;

  if ( isBplus ) {
    phi = calculatePlaneAngle( m_pMuPlus->momentum(), m_pMuMinus->momentum(), m_pK->momentum(), m_pPi->momentum() );
  } else {

    // flip sign for phi in Bminus case
    phi = -calculatePlaneAngle( m_pMuMinus->momentum(), m_pMuPlus->momentum(), m_pK->momentum(), m_pPi->momentum() );
  }

  return phi;
}

double MCBu2KstarMuMuAngleCalculator::calculateThetaL( const LHCb::MCParticle* particle ) {

  StatusCode sc = daughters( particle );

  if ( sc.isFailure() ) { Exception( "Could not find required particles !" ); }

  int motherID = particle->particleID().pid();
  if ( particle->hasOscillated() ) motherID = -motherID;
  bool isBplus = ( motherID > 0 );

  double theta = -999;

  if ( isBplus ) {
    theta = calculateHelicityPolarAngle( m_pK->momentum(), m_pPi->momentum(), m_pMuPlus->momentum(),
                                         m_pMuMinus->momentum() );
  } else {
    theta = calculateHelicityPolarAngle( m_pK->momentum(), m_pPi->momentum(), m_pMuMinus->momentum(),
                                         m_pMuPlus->momentum() );
  }
  return theta;
}

double MCBu2KstarMuMuAngleCalculator::calculateThetaK( const LHCb::MCParticle* particle ) {
  StatusCode sc = daughters( particle );

  if ( sc.isFailure() ) { Exception( "Could not find required particles !" ); }

  double theta =
      calculateHelicityPolarAngle( m_pK->momentum(), m_pPi->momentum(), m_pMuPlus->momentum(), m_pMuMinus->momentum() );
  return theta;
}

double MCBu2KstarMuMuAngleCalculator::calculateTransThetaTr( const LHCb::MCParticle* particle ) {
  StatusCode sc = daughters( particle );

  if ( sc.isFailure() ) { Exception( "Could not find required particles !" ); }

  int motherID = particle->particleID().pid();
  if ( particle->hasOscillated() ) motherID = -motherID;
  bool isBplus = ( motherID > 0 );

  double Theta_tr = -999.;

  if ( isBplus ) {
    Theta_tr = calculateThetaTr( m_pMuPlus->momentum(), m_pMuMinus->momentum(), m_pK->momentum(), m_pPi->momentum() );
  } else {
    Theta_tr = calculateThetaTr( m_pMuMinus->momentum(), m_pMuPlus->momentum(), m_pK->momentum(), m_pPi->momentum() );
  }

  return Theta_tr;
}

double MCBu2KstarMuMuAngleCalculator::calculateTransPhiTr( const LHCb::MCParticle* particle ) {
  StatusCode sc = daughters( particle );

  if ( sc.isFailure() ) { Exception( "Could not find required particles !" ); }

  int motherID = particle->particleID().pid();
  if ( particle->hasOscillated() ) motherID = -motherID;
  bool isBplus = ( motherID > 0 );

  double Phi_tr = -999.;

  if ( isBplus ) {
    Phi_tr = calculatePhiTr( m_pMuPlus->momentum(), m_pMuMinus->momentum(), m_pK->momentum(), m_pPi->momentum() );
  } else {
    Phi_tr = calculatePhiTr( m_pMuMinus->momentum(), m_pMuPlus->momentum(), m_pK->momentum(), m_pPi->momentum() );
  }

  return Phi_tr;
}

double MCBu2KstarMuMuAngleCalculator::calculateTransThetaV( const LHCb::MCParticle* particle ) {
  return calculateThetaK( particle );
}

double MCBu2KstarMuMuAngleCalculator::calculateMass( const LHCb::MCParticle* particle ) {
  StatusCode sc = daughters( particle );

  if ( sc.isFailure() ) { Exception( "Could not find required particles !" ); }

  const Gaudi::LorentzVector& vecMuPlus  = m_pMuPlus->momentum();
  const Gaudi::LorentzVector& vecMuMinus = m_pMuPlus->momentum();

  return ( vecMuPlus + vecMuMinus ).M();
}
