/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id$
#ifndef BS2JPSIPHIANGLECALCULATOR_H
#define BS2JPSIPHIANGLECALCULATOR_H 1

// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/IP2VVPartAngleCalculator.h" // Interface

/** @class Bs2JpsiPhiAngleCalculator Bs2JpsiPhiAngleCalculator.h
 *
 *  Calculates the three angles in a B->J/psi(ll)phi(KK) decay.
 *  Angles given in both helicity and transversity bases.
 *
 *  @author Greig Cowan
 *  @date   2008-05-12
 */

class Bs2JpsiPhiAngleCalculator : public GaudiTool, virtual public IP2VVPartAngleCalculator {

public:
  /// Standard constructor
  Bs2JpsiPhiAngleCalculator( const std::string& type, const std::string& name, const IInterface* parent );

  ~Bs2JpsiPhiAngleCalculator(); ///< Destructor

  StatusCode initialize() override;

public:
  double calculateThetaL( const LHCb::Particle* particle ) override;
  double calculateThetaK( const LHCb::Particle* particle ) override;
  double calculatePhi( const LHCb::Particle* particle ) override;
  double calculateTransThetaTr( const LHCb::Particle* particle ) override;
  double calculateTransPhiTr( const LHCb::Particle* particle ) override;
  double calculateTransThetaV( const LHCb::Particle* particle ) override;

  StatusCode calculateAngles( const LHCb::Particle* particle, double& thetaL, double& thetaK, double& phi ) override;

  StatusCode calculateTransversityAngles( const LHCb::Particle* particle, double& Theta_tr, double& Phi_tr,
                                          double& Theta_V ) override;

private:
  IParticleDescendants* m_descendants;
};

#endif // BS2JPSIPHIANGLECALCULATOR_H
