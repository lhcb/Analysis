###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Phys/DaVinciAngleCalculators
----------------------------
#]=======================================================================]

gaudi_add_module(DaVinciAngleCalculators
    SOURCES
        src/Bd2KstarMuMuAngleCalculator.cpp
        src/Bs2JpsiPhiAngleCalculator.cpp
        src/Bu2KstarMuMuAngleCalculator.cpp
        src/DaVinciP2VVAngles.cpp
        src/MCBd2KstarMuMuAngleCalculator.cpp
        src/MCBs2JpsiPhiAngleCalculator.cpp
        src/MCBu2KstarMuMuAngleCalculator.cpp
        src/P2VVAngleCalculator.cpp
    LINK
        Boost::headers
        Gaudi::GaudiAlgLib
        Gaudi::GaudiKernel
        LHCb::MCEvent
        LHCb::MCInterfacesLib
        LHCb::PartPropLib
        LHCb::PhysEvent
        Phys::DaVinciInterfacesLib
        Phys::DaVinciMCKernelLib
        ROOT::GenVector
)
