#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
# $Id$
# =============================================================================
# CVS tag $Name: not supported by cvs2svn $, version $Revision$
# =============================================================================
# $Log: not supported by cvs2svn $
# =============================================================================
## @file   LoKiAlgoMC/tests.py
#  @author Vanya BELYAEV ibelyaev@physics.syr.edu
#  @date 2007-05-29
# =============================================================================
from __future__ import print_function
""" The basic tests for LoKiAlgoMC package """
_author_ = "Vanya BELYAEV ibelyaev@physics.syr.edu"


# =============================================================================
## The most trivial test function
def test():
    """ The most trivial test function """
    from LoKiAlgoMC.decorators import _decorated
    print('LoKiAlgoMCTest: decorated objects %s' % len(_decorated))
    pass


# =============================================================================
## Perform all known tests
def testAll():
    """ Perform all known tests """
    test()


# =============================================================================
if '__main__' == __name__:
    testAll()
    # =============================================================================

    # =============================================================================
    # The END
    # =============================================================================
