###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# $Id$
# =============================================================================
# CVS tag $Name: not supported by cvs2svn $, version $Revision$
# =============================================================================
# $Log: not supported by cvs2svn $
# =============================================================================
## @file  LoKiAlgoMC/__init__.py
#  Helper file to manage LoKiAlgoMC package
#  @author Vanya BELYAEV ibelyaev@physics.syr.edu
#  @date 2007-05-29
# =============================================================================
""" Helper file to manage LoKiAlgoMC package """
_author_ = "Vanya BELYAEV ibelyaev@physics.syr.edu"
# =============================================================================
##import LoKiAlgo.decorators
##import LoKiPhysMC.decorators
# =============================================================================
# The END
# =============================================================================
