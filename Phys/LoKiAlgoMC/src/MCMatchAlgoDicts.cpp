/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id$
// ============================================================================
// Include files
// ============================================================================
// Event
// ============================================================================
#include "Event/MCParticle.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/Keeper.h"
#include "LoKi/Loop.h"
#include "LoKi/MCMatch.h"
#include "LoKi/MCMatchAlgoDicts.h"
#include "LoKi/MCMatchDicts.h"
#include "LoKi/MCTypes.h"
#include "LoKi/UniqueKeeper.h"
// ============================================================================
/** @file
 *  implementation file for functionf from namespace LOKi:Dicts::MCMatchDicts
 *  @author Vanya BELAYEV ibelyaev@physics.syr.edu
 *  @date 2007-07-05
 */
// ============================================================================
// do these MC particles make contribution to this RC particle?
// ============================================================================
bool LoKi::Dicts::MCMatchDicts::match( const LoKi::MCMatch& m, const LoKi::Loop& l, const LHCb::MCParticle* mc ) {
  return match( m, l.particle(), mc );
}
// ============================================================================
// do these MC particles make contribution to this RC particle?
// ============================================================================
bool LoKi::Dicts::MCMatchDicts::match( const LoKi::MCMatch& m, const LoKi::Loop& l, const LoKi::Types::MCRange& mc ) {
  return match( m, l.particle(), mc );
}
// ============================================================================
// do these MC particles make contribution to this RC particle?
// ============================================================================
bool LoKi::Dicts::MCMatchDicts::match( const LoKi::MCMatch& m, const LoKi::Loop& l,
                                       const LHCb::MCParticle::Vector& mc ) {
  return match( m, l.particle(), mc );
}
// ============================================================================
// do these MC particles make contribution to this RC particle?
// ============================================================================
bool LoKi::Dicts::MCMatchDicts::match( const LoKi::MCMatch& m, const LoKi::Loop& l,
                                       const LHCb::MCParticle::ConstVector& mc ) {
  return match( m, l.particle(), mc );
}
// ============================================================================
// do these MC particles make contribution to this RC particle?
// ============================================================================
bool LoKi::Dicts::MCMatchDicts::match( const LoKi::MCMatch& m, const LoKi::Loop& l,
                                       const LoKi::Keeper<LHCb::MCParticle>& mc ) {
  return match( m, l.particle(), mc );
}
// ============================================================================
// do these MC particles make contribution to this RC particle?
// ============================================================================
bool LoKi::Dicts::MCMatchDicts::match( const LoKi::MCMatch& m, const LoKi::Loop& l,
                                       const LoKi::UniqueKeeper<LHCb::MCParticle>& mc ) {
  return match( m, l.particle(), mc );
}
// ============================================================================

// ============================================================================
// The END
// ============================================================================
