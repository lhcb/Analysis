#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
"""
High level configuration tools for AnalysisConf
"""
from __future__ import print_function
# =============================================================================
__version__ = "$Id: Configuration.py,v 1.23 2010-01-22 13:11:30 pkoppenb Exp $"
__author__ = "Patrick Koppenburg <Patrick.Koppenburg@cern.ch>"
# =============================================================================

from LHCbKernel.Configuration import *
import GaudiKernel.ProcessJobOptions


class AnalysisConf(LHCbConfigurableUser):

    __slots__ = {
        "DataType":
        ''  # Data type, can be ['2008','2009','MC09','2010','2011']
        ,
        "InputType":
        ''  #  'DST', 'DIGI', 'RDST', 'MDST', 'XDST' or 'LDST'. Nothing means the input type is compatible with being a DST.
        ,
        "Simulation":
        False  # set to True for MC
        ,
        "RedoMCLinks":
        False  # On some stripped DST one needs to redo the Track<->MC link table. Set to true if problems with association.
        ,
        "OutputLevel":
        INFO  # The global output level
        ,
        "RootInTES":
        ''  # Root-In-TES
    }

    _propertyDocDct = {
        "DataType":
        """ Data type, can be ['2008','2009','MC09','2010','2011'] """,
        "InputType":
        """ 'DST', 'DIGI', 'RDST', 'MDST', 'XDST' or 'LDST'. Nothing means the input type is compatible with being a DST. """,
        "Simulation":
        """ set to True to use SimCond """,
        "RedoMCLinks":
        """ On some stripped DST one needs to redo the Track<->MC link table. Set to true if problems with association """,
        "OutputLevel":
        """ The global output level """,
        "RootInTES":
        """ Root-In-TES"""
    }

    __used_configurables__ = ('CaloAssociatorsConf', 'FlavourTaggingConf')

    #
    # configure reconstruction to be redone
    #
    def initSequence(self):
        """
        Init Sequence. Called by master application.
        """
        # only one initialisiation do far
        from Configurables import GaudiSequencer
        init = GaudiSequencer("AnalysisInitSeq")
        self.redoMCLinks(init)
        return init

    #
    # BTaggingTool configuration
    #
    def tagging(self):
        from Configurables import FlavourTaggingConf
        # Just create an instance. Nothing to configure as of yet
        ftConf = FlavourTaggingConf()

    #
    # Set MC
    #
    def redoMCLinks(self, init):
        """
        Redo MC links.
        """

        if (self.getProp("Simulation")):
            redo = self.getProp("RedoMCLinks")
            if (redo):
                from Configurables import (GaudiSequencer, TESCheck,
                                           EventNodeKiller, TrackAssociator)
                mcKillSeq = GaudiSequencer(
                    "KillMCLinks"
                )  # The sequence killing the node of it exists
                tescheck = TESCheck(
                    "DaVinciEvtCheck")  # Check for presence of node ...
                tescheck.Inputs = ["Link/Rec/Track/Best"]  #
                tescheck.Stop = False  # But don't stop
                tescheck.OutputLevel = 5  # don't print warnings
                evtnodekiller = EventNodeKiller(
                    "DaVinciEvtNodeKiller")  # kill nodes
                evtnodekiller.Nodes = ["Link/Rec/Track"]  # Kill that
                mcKillSeq.Members = [
                    tescheck, evtnodekiller,
                    TrackAssociator()
                ]
                mcLinkSeq = GaudiSequencer(
                    "RedoMCLinks")  # The sequence redoing the links
                mcLinkSeq.IgnoreFilterPassed = True  # Run it always
                mcLinkSeq.Members = [mcKillSeq, TrackAssociator()]
                init.Members += [mcLinkSeq]

#
# Set MC
#

    def configureMC(self):
        """
        Define DaVinciAssociators. Do MC unpacking.
        """
        if not self.getProp("InputType") in ["MDST"]:
            from Configurables import CaloAssociatorsConf
            CaloAssociatorsConf(
                EnableMCOnDemand=True, OutputLevel=self.getProp("OutputLevel"))

        from CaloKernel.ConfUtils import getAlgo
        from Configurables import NeutralPP2MC, ChargedPP2MC

        root_ = '/Event'
        if self.getProp("InputType") == 'MDST' and self.getProp('RootInTES'):
            root_ = self.getProp('RootInTES')

        self.unpackMC(root_)

        ## offline neutral protoparticles
        alg1 = getAlgo(
            NeutralPP2MC,  ## type
            'NeutralPP2MC',  ## base-name
            'Offline',  ## context
            root_ + '/Relations/Rec/ProtoP/Neutrals',  ## base-location
            True)  ## on-demand

        ## hlt neutral protoparticles
        alg2 = getAlgo(
            NeutralPP2MC,  ## type
            'NeutralPP2MC',  ## base-name
            'Hlt',  ## context
            root_ + '/Relations/Rec/ProtoP/Neutrals',  ## base-location
            True)  ## on-demand

        from Configurables import DataOnDemandSvc, ChargedPP2MC

        if root_ != '/Event':
            alg1.RootInTES = root_
            ChargedPP2MC(RootInTES=root_)

        dod = DataOnDemandSvc()
        dod.AlgMap[root_ + '/Relations/Rec/ProtoP/Charged'] = ChargedPP2MC()
        dod.AlgMap[root_ + '/Relations/Rec/ProtoP/Upstream'] = ChargedPP2MC()

#
# Standard Particles
#

    def standardParticles(self):
        """
        define standard particles on DoD service
        """
        import CommonParticles.StandardBasic
        import CommonParticles.StandardIntermediate


#
# Unpack MC
#

    def unpackMC(self, rootInTES):
        """
        Do MC unpacking for objects under rootInTES
        """
        import os
        from Configurables import UnpackMCParticle, UnpackMCVertex

        mcRoot = os.path.join(rootInTES, "MC")

        log.info("Will unpack MC objects to {0}".format(mcRoot))

        particlesOutput = os.path.join(mcRoot, "Particles")
        verticesOutput = os.path.join(mcRoot, "Vertices")

        dod = DataOnDemandSvc()

        dod.NodeMap[mcRoot] = "DataObject"

        dod.AlgMap[particlesOutput] = UnpackMCParticle(RootInTES=rootInTES)
        dod.AlgMap[verticesOutput] = UnpackMCVertex(RootInTES=rootInTES)

        ## for uDST
        if mcRoot != '/Event/MC':
            ## create the directory (if missing)
            dod.NodeMap[rootInTES + '/pSim'] = 'DataObject'
            from Configurables import Gaudi__DataLink as Link
            for name, target, what in [
                    ## process ``pMC'' and ``pSim'' variants of uDST coherently
                ('LINK:MC2Sim4Particles', rootInTES + '/pSim/MCParticles',
                 rootInTES + '/pMC/Particles'),
                ('LINK:MC2Sim4Vertices', rootInTES + '/pSim/MCVertices',
                 rootInTES + '/pMC/Vertices'),
                    # In case any algorithm has a hard-coded dependency on /Event/MC/...,
                    # creates links from the location under RootInTES to /Event/MC
                ('LINK:MCParticles', '/Event/MC/Particles',
                 rootInTES + '/MC/Particles'),
                ('LINK:MCVertices', '/Event/MC/Vertices',
                 rootInTES + '/MC/Vertices'),
            ]:
                dod.AlgMap[target] = Link(
                    name, Target=target, What=what, OutputLevel=5)
                log.debug(
                    'Set additional MC->SIM links %s -> %s ' % (what, target))

            ## some non-trivial action for non-trivial settings
            from Configurables import GaudiSequencer
            dod.AlgMap[particlesOutput] = GaudiSequencer(
                "SEQ:MCParticles",
                Members=[
                    dod.AlgMap[particlesOutput],
                    Link(
                        'LINK:MCParticles2MC',
                        Target=particlesOutput,
                        What='/Event/MC/Particles',
                        OutputLevel=5)
                ])
            dod.AlgMap[verticesOutput] = GaudiSequencer(
                "SEQ:MCVertices",
                Members=[
                    dod.AlgMap[verticesOutput],
                    Link(
                        'LINK:MCVertices2MC',
                        Target=verticesOutput,
                        What='/Event/MC/Vertices',
                        OutputLevel=5)
                ])

    #
    # Apply configuration
    #
    def __apply_configuration__(self):
        """
        Apply configuration for Analysis
        """
        log.info("Applying Analysis configuration")
        log.info(self)
        GaudiKernel.ProcessJobOptions.PrintOff()

        if (self.getProp("Simulation")):
            self.configureMC()

        # Setup DataOnDemand, and make sure ToolSvc is done before hand
        ApplicationMgr().ExtSvc += [ToolSvc(), DataOnDemandSvc()]

        self.tagging()
        self.standardParticles()

if '__main__' == __name__:
    print(__doc__)
    print(__author__)
    print(__version__)

# =============================================================================
# The END
# =============================================================================
