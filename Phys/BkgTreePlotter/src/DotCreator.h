/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
#ifndef INCLUDE_DOTCREATOR_H
#define INCLUDE_DOTCREATOR_H 1

#include "Event/MCParticle.h"
#include "Kernel/DaVinciAlgorithm.h"
#include "Kernel/Particle2MCLinker.h"

class IDaVinciAssociatorsWrapper;

/** @class DotCreator DotCreator.h
 *
 *
 * @author Paul Seyfert <pseyfert@cern.ch>
 * @date   2015-04-07
 */
class DotCreator : public DaVinciAlgorithm {

public:
  /// Standard Constructor
  DotCreator( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~DotCreator(); ///< Destructor

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm event execution
  StatusCode finalize() override;   ///< Algorithm finalize

protected:
  std::vector<const LHCb::Particle*> stables( const LHCb::Particle* );
  void                               stables( std::vector<const LHCb::Particle*>& target, const LHCb::Particle* p );
  std::set<const LHCb::MCParticle*>  ancestors( std::vector<const LHCb::MCParticle*> associates );
  int        do_tree( std::vector<std::pair<int, int>>& edges, int& uid, const LHCb::MCParticle* ancestor );
  StatusCode processCandidate( const LHCb::Particle* p );

private:
  std::map<const LHCb::MCParticle*, size_t> m_mapping;
  std::set<const LHCb::MCVertex*>           m_exceptions; // avoid supression of MCVertices if needed
  std::vector<std::string>                  m_labels;
  IDaVinciAssociatorsWrapper*               m_linkerTool_Links;
  Particle2MCLinker*                        m_mcLink;
  int                                       m_uuid;

  bool m_nofragmentation;
};
#endif // INCLUDE_DOTCREATOR_H
