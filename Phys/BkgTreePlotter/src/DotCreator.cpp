/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DotCreator.h"
#include "Event/ODIN.h"
#include "Kernel/IDaVinciAssociatorsWrapper.h"
#include <fstream>
#include <string>

//-----------------------------------------------------------------------------
// Implementation file for class : DotCreator
// see also https://twiki.cern.ch/twiki/bin/view/LHCb/BkgTreePlotter
//
// 2015-04-07 : Paul Seyfert <pseyfert@cern.ch>
//-----------------------------------------------------------------------------

DECLARE_COMPONENT( DotCreator )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
DotCreator::DotCreator( const std::string& name, ISvcLocator* pSvcLocator )
    : DaVinciAlgorithm( name, pSvcLocator ), m_uuid( 0 ) {
  declareProperty( "NoFragmentation", m_nofragmentation = true );
}

//=============================================================================
// Destructor
//=============================================================================
DotCreator::~DotCreator() { ; }

//=============================================================================
// Initialization
//=============================================================================
StatusCode DotCreator::initialize() {
  StatusCode sc = DaVinciAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;                // error printed already by DaVinciAlgorithm

  m_linkerTool_Links = tool<IDaVinciAssociatorsWrapper>( "DaVinciAssociatorsWrapper", "Wrapper_Links", this );

  debug() << "==> Initialize" << endmsg;

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode DotCreator::execute() {
  m_uuid = 0;
  debug() << "==> Execute" << endmsg;

  m_mcLink = m_linkerTool_Links->linker( Particle2MCMethod::Links, inputLocations() );

  const LHCb::Particle::Range inputParticles = particles();

  for ( LHCb::Particle::Range::iterator Piter = inputParticles.begin(); inputParticles.end() != Piter; ++Piter ) {
    m_exceptions.clear();
    processCandidate( *Piter ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }

  setFilterPassed( true );
  return StatusCode::SUCCESS;
}

void DotCreator::stables( std::vector<const LHCb::Particle*>& target, const LHCb::Particle* p ) {
  if ( p->isBasicParticle() ) {
    target.push_back( p );
  } else {
    for ( size_t d = 0; p->daughters().size() > d; ++d ) { stables( target, p->daughters()[d] ); }
  }
}

std::set<const LHCb::MCParticle*> DotCreator::ancestors( std::vector<const LHCb::MCParticle*> associates ) {
  std::set<const LHCb::MCParticle*> retval;
  /// go through 'associates'
  for ( auto decendant : associates ) {
    /// get the mother of the mother of the ... until the root of the tree
    while ( decendant->mother() ) {
      if ( m_nofragmentation && ( decendant->originVertex()->type() == LHCb::MCVertex::StringFragmentation ||
                                  decendant->originVertex()->type() == LHCb::MCVertex::ppCollision ) )
        break;
      m_exceptions.insert( decendant->originVertex() );
      decendant = decendant->mother();
    }
    retval.insert( decendant );
  }
  return retval;
}

int DotCreator::do_tree( std::vector<std::pair<int, int>>& edges, int& uid, const LHCb::MCParticle* ancestor ) {
  m_mapping[ancestor] = uid++;
  while ( m_labels.size() <= m_mapping[ancestor] ) m_labels.push_back( "" );
  m_labels[m_mapping[ancestor]] = ( ppSvc()->find( ancestor->particleID() )->name() );
  for ( auto ev : ancestor->endVertices() ) {
    if ( ( m_exceptions.end() == m_exceptions.find( ev ) ) /// do not continue if this MCVertex is an exception
         && (                                              // ev->type() == LHCb::MCVertex::Unknown
                                                           //|| ev->type() == LHCb::MCVertex::ppCollision
                                                           //|| ev->type() == LHCb::MCVertex::DecayVertex
                                                           //|| ev->type() == LHCb::MCVertex::OscillatedAndDecay
                                                           //|| ev->type() == LHCb::MCVertex::StringFragmentation
                ev->type() == LHCb::MCVertex::HadronicInteraction || ev->type() == LHCb::MCVertex::Bremsstrahlung ||
                ev->type() == LHCb::MCVertex::PairProduction || ev->type() == LHCb::MCVertex::Compton ||
                ev->type() == LHCb::MCVertex::DeltaRay || ev->type() == LHCb::MCVertex::PhotoElectric ||
                ev->type() == LHCb::MCVertex::Annihilation || ev->type() == LHCb::MCVertex::RICHPhotoElectric ||
                ev->type() == LHCb::MCVertex::Cerenkov || ev->type() == LHCb::MCVertex::RichHpdBackScat ||
                ev->type() == LHCb::MCVertex::GenericInteraction || ev->type() == LHCb::MCVertex::LHCHalo ||
                ev->type() == LHCb::MCVertex::MuonBackground || ev->type() == LHCb::MCVertex::MuonBackgroundFlat ||
                ev->type() == LHCb::MCVertex::MuonBackgroundSpillover || ev->type() == LHCb::MCVertex::WorldLimit ||
                ev->type() == LHCb::MCVertex::KinematicLimit ) )
      continue;
    for ( auto prod : ev->products() ) {
      int child_node = do_tree( edges, uid, prod );
      edges.push_back( std::pair<int, int>( m_mapping[ancestor], child_node ) );
    }
  }
  return m_mapping[ancestor];
}

StatusCode DotCreator::processCandidate( const LHCb::Particle* p ) {
  typedef std::pair<int, int> Edge;
  std::vector<Edge>           edges;
  edges.clear();
  m_labels.clear();
  int                                  uid( 0 );
  std::vector<const LHCb::Particle*>   stable_reconstructed_decayproducts;
  std::vector<const LHCb::MCParticle*> associates;
  stables( stable_reconstructed_decayproducts, p );
  for ( size_t d = 0; d < stable_reconstructed_decayproducts.size(); ++d ) {
    const LHCb::Particle*   rec_child = stable_reconstructed_decayproducts[d];
    const LHCb::MCParticle* mcp       = m_mcLink->firstMCP( rec_child );
    if ( mcp ) associates.push_back( mcp );
  }
  auto the_ancestors = ancestors( associates );
  for ( auto ancestor : the_ancestors ) { do_tree( edges, uid, ancestor ); }

  std::vector<std::string> colors( m_labels.size(), "white" );
  for ( size_t d = 0; d < stable_reconstructed_decayproducts.size(); ++d ) {
    const LHCb::Particle* rec_child = stable_reconstructed_decayproducts[d];

    const LHCb::MCParticle* mcp = m_mcLink->firstMCP( rec_child );
    if ( mcp ) {
      m_labels[m_mapping[mcp]] += " reconstructed as " + ppSvc()->find( rec_child->particleID() )->name();
      colors[m_mapping[mcp]] = "lightblue";
    } else {
      m_labels.push_back( std::string( "ghost " ) + ppSvc()->find( rec_child->particleID() )->name() );
      colors.push_back( "lightblue" );
    }
  }

  {
    const LHCb::ODIN* odin = get<LHCb::ODIN>( LHCb::ODINLocation::Default, false );
    std::ofstream     f( ( std::to_string( odin->runNumber() ) + "." + std::to_string( odin->eventNumber() ) + "." +
                       std::to_string( m_uuid++ ) + ".dot" )
                         .c_str(),
                     std::ofstream::out );
    f << "digraph G {\n";
    for ( size_t n = 0; n < m_labels.size(); ++n ) {
      f << n << "[label=\"" << m_labels[n] << "\", style=\"filled\", fillcolor=\"" << colors[n] << "\"];\n";
    }
    for ( auto e : edges ) { f << e.first << "->" << e.second << ";\n"; }
    f << "}\n";
    f.close();
  }
  return StatusCode::SUCCESS;
}

//=============================================================================
// Finalize
//=============================================================================
StatusCode DotCreator::finalize() {
  debug() << "==> Finalize" << endmsg;
  debug() << "Thanks for using the DotCreator!" << endmsg;
  debug() << "Please have a look at https://twiki.cern.ch/twiki/bin/view/LHCb/BkgTreePlotter" << endmsg;

  return DaVinciAlgorithm::finalize(); // must be called after all other actions
}
