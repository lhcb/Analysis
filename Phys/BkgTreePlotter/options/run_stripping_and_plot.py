###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import print_function
from Configurables import CondDB, DaVinci

DaVinci().DDDBtag = "dddb-20120831"
DaVinci().CondDBtag = "sim-20121025-vc-mu100"
year = "2012"
from Configurables import DaVinci, DotCreator

####################
# import stripping to make TES locations and decisions right

from StrippingConf.Configuration import StrippingConf
from StrippingConf.StrippingStream import StrippingStream
stream = StrippingStream("Test")

from Configurables import Gaudi__IODataManager as IODataManager
IODataManager("IODataManager").UseGFAL = False

from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from Configurables import EventNodeKiller
eventNodeKiller = EventNodeKiller('Stripkiller')
eventNodeKiller.Nodes = [
    '/Event/AllStreams', '/Event/Strip', '/Event/Phys/DecReports'
]
eventNodeKiller.Nodes += ['Link/Raw/Velo/Clusters']

from StrippingArchive.Stripping20 import StrippingTau23MuLines, StrippingTau2PMuMu
from Configurables import TCKPrescaleEmulator

conf = StrippingTau23MuLines.Tau23MuLinesConf(
    "Tau23Mu", StrippingTau23MuLines.Tau23MuLinesConf.config_default)
stream.appendLines(conf.lines())

from STTools import STOfflineConf
STOfflineConf.DefaultConf().configureTools()

# nnpidseq is deprecated in recent DV versions (comment from June 25, 2013)
#from Configurables import ChargedProtoANNPIDConf,
from Configurables import GaudiSequencer

from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()
sc = StrippingConf(
    Streams=[stream],
    TESPrefix='Strip',
    MaxCandidates=2000,
    AcceptBadEvents=False,
    HDRLocation="Phys/DecReports",
    BadEventSelection=filterBadEvents)
from Configurables import CondDB

#
#######

###################
# not needed at all

from Configurables import EventTuple, TupleToolSelResults
from Configurables import TupleToolStripping

# Remove the microbias and beam gas etc events before doing the tagging step
regexp = "HLT_PASS_RE('Hlt1(?!ODIN)(?!L0)(?!Lumi)(?!Tell1)(?!MB)(?!NZS)(?!Velo)(?!BeamGas)(?!Incident).*Decision')"
from Configurables import LoKi__HDRFilter
filterHLT = LoKi__HDRFilter("FilterHLT", Code=regexp)
DaVinci().EventPreFilters += [filterHLT]

# not needed at all
###################

# selections
from Configurables import FilterDesktop
from PhysSelPython.Wrappers import Selection, DataOnDemand
from Configurables import CombineParticles, GaudiSequencer

from Configurables import AuditorSvc, ChronoAuditor
AuditorSvc().Auditors.append(ChronoAuditor("Chrono"))

from Configurables import CondDB

###################
# write your ntuple

# write your ntuple
###################

########################
# which events to run on
from Configurables import LoKi__HDRFilter as Filter

cond = " "
for i in range(len(stream.lines) - 1):
    #for i in range(1) :
    cond += "HLT_PASS ( '" + stream.lines[i].name() + "Decision')"
    cond += " | "
cond += "HLT_PASS ( '" + stream.lines[len(stream.lines) -
                                      1].name() + "Decision')"

print(cond)
fltr = Filter('collect', Code=cond, Location="/Event/Strip/Phys/DecReports")

#####################
# some DaVinci config

DaVinci().EvtMax = 2000
DaVinci().DataType = year

#############################
# putting the things together

from Configurables import GaudiSequencer

wrap = GaudiSequencer("wrapper")
MyFilterSeq = GaudiSequencer("filter")
MyFilterSeq.ModeOR = True
MyFilterSeq.ShortCircuit = False
MyFilterSeq.IgnoreFilterPassed = False
MyFilterSeq.Members = [fltr, GaudiSequencer("SeqDs")]

DaVinci().appendToMainSequence([eventNodeKiller])
DaVinci().appendToMainSequence([sc.sequence()])

DotCreator().Inputs = []
for line in stream.lines:
    DotCreator().Inputs += [line.outputLocation()]
DaVinci().appendToMainSequence([DotCreator()])

#############################
# job specific DaVinci config

DaVinci().Simulation = True
DaVinci().InputType = 'DST'

from Gaudi.Configuration import *
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00026299/0000/00026299_00000006_1.allstreams.dst'
])
