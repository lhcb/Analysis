2020-08-12 Analysis v21r5
===

This version uses Gaudi v33r2, LHCb v45r4, Lbcom v23r0p4, Rec v24r4 and Phys v26r5, and uses LCG_97a with ROOT 6.20.06.

Release for use with Run 1 or Run 2 data, prepared on the run2-patches branch
Built relative to Analysis [v21r4](../-/tags/v21r4), with the following changes:


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Build | Remove obsolete makefile and test, !632 (@cattanem)
- Remove Tutorial package: run2-patches, !640 (@masmith)
- Remove Ostap, !631 (@cattanem)


### Other

- ~Tuples | Centrality tuple tool, !664 !669 (@fgarciar) [INT-2020]
- ~Tuples | Converted Bs2MuMu isolation refinfo tool to tupletool, !662 (@bdey)
