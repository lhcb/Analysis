2021-11-30 Analysis v22r0
===

This version uses Gaudi v36r2, LHCb v46r0, Lbcom v24r0, Rec v25r0, Phys v27r0 and uses LCG101 with ROOT 6.24.06.

This version is released on `run2-patches` branch and is intended for use with Run1 or Run2 data. For Run3, use a version released on `master` branch
Built relative to Analysis [v21r8](../-/tags/v21r8), with the following changes:


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Build | (run2) Rewrite CMake configuration in "modern CMake", !837 (@clemenci) [LBCOMP-23]


### Other

- ~Tuples | Add check for unique branch decay descriptors, !833 (@masmith) [#19]
