2019-07-22 Analysis v21r1
=========================

Release for use with Run 1 or Run 2 data, prepared on the run2-patches branch.
It is based on Gaudi v32r1, LHCb v45r1, Lbcom v23r0p1, Rec v24r1 and Phys v26r1,
and uses LCG_96 with ROOT 6.18.00.

- Fixes for yet more clang8 warnings
  - See merge request lhcb/Analysis!533
- Fix many clang8 warnings
  - See merge request lhcb/Analysis!532
- Add sanity check to TupleToolHOP
  - See merge request lhcb/Analysis!513
- Fix Ostap test for ROOT 6.18
  - See merge request lhcb/Analysis!528
- Remove unused Analysis/PyAna package
  - See merge request lhcb/Analysis!529
- Add TupleToolDOCA tool
  - See merge request lhcb/Analysis!524
