#!/usr/bin/env gaudirun.py
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
## @file
#  The configuration file to run LoKi_CCTest example
#
#   This file is a part of LoKi project -
#     "C++ ToolKit  for Smart and Friendly Physics Analysis"
#
#  The package has been designed with the kind help from
#  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
#  contributions and advices from G.Raven, J.van Tilburg,
#  A.Golutvin, P.Koppenburg have been used in the design.
#
#  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
#  @date   2008-08-04
# =============================================================================
"""
The configuration file to run LoKi_CCTest example

This file is a part of LoKi project -
'C++ ToolKit  for Smart and Friendly Physics Analysis'

The package has been designed with the kind help from
Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
contributions and advices from G.Raven, J.van Tilburg,
A.Golutvin, P.Koppenburg have been used in the design.
"""
# =============================================================================
__author__ = "Vanya BELYAEV Ivan.Belyaev@nikhef.nl"
# =============================================================================
from Gaudi.Configuration import *

## configure our own algorithm:
from Configurables import LoKi__CCTest as CCTest
alg = CCTest(
    'CCTest',
    Decays=[
        "pi+", "K+pi+++", "cc nosos ",
        "some line which contains B0/B~0 and other K*(892)0mesons Meson"
    ])

## get input data:
from LoKiExample.Bs2Jpsiphi_mm_data import Inputs as INPUT

## confgure the application itself:
from Configurables import DaVinci
DaVinci(
    DataType='DC06',  ## Data type
    Simulation=True,  ## Monte Carlo
    Hlt=False,
    #
    UserAlgorithms=[alg],  ## let DaVinci know about local algorithm
    # delegate this properties to Event Selector
    EvtMax=10,
    SkipEvents=0,
    Input=INPUT  ## the list of input data files
)

# =============================================================================
# The END
# =============================================================================
