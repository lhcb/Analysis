/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// PartProp
// ============================================================================
#include "Kernel/IParticlePropertySvc.h"
// ============================================================================
// GaudiAlg
// ============================================================================
#include "GaudiAlg/GaudiAlgorithm.h"
// ============================================================================
/** @file
 *  Implementation file for class LoKi::CCTest
 *  simple test for CC-operation
 *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
 *  @date 2008-08-04
 */
// ============================================================================
namespace LoKi {
  // ==========================================================================
  /** @class CCTest   CCTest.cpp
   *  Simple test for LoKi::cc utility
   *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
   *  @date 2008-08-04
   */
  class CCTest : public GaudiAlgorithm {
  public:
    // ========================================================================
    /// initialization of the algorithm
    StatusCode initialize() override {
      // initialize the base
      StatusCode sc = GaudiAlgorithm::initialize();
      if ( sc.isFailure() ) { return sc; }
      // load LoKi
      svc<IService>( "LoKiSvc", true ); // load LoKi
      return StatusCode::SUCCESS;
    }
    // ========================================================================
    /// the actual execution of the algorithm
    StatusCode execute() override {
      //
      for ( const auto& d : m_decays ) {
        always() << " The decay: '" << ( d ) << "'" << endmsg << " CC-decay : '" << ppSvc()->cc( d ) << "'" << endmsg;
      }
      //
      return StatusCode::SUCCESS;
    }
    /** standard constrcutor
     *  @param name the algorithm instance name
     *  @param pSvc pointer to Service Locator
     */
    CCTest( const std::string& name, // Algorithm instance name
            ISvcLocator*       pSvc )      // Service Locator
        : GaudiAlgorithm( name, pSvc ), m_decays(), m_ppSvc( 0 ) {
      declareProperty( "Decays", m_decays, "List of decays to be 'cc'-fied" );
    }
    // ========================================================================
  protected:
    // ========================================================================
    // get the particle property service
    inline const LHCb::IParticlePropertySvc* ppSvc() const {
      if ( !m_ppSvc ) m_ppSvc = svc<LHCb::IParticlePropertySvc>( "LHCb::ParticlePropertySvc", true );
      return m_ppSvc;
    }
    // ========================================================================
  private:
    // ========================================================================
    /// the list of decays to be "cc-fied":
    std::vector<std::string> m_decays; // the list of decays to be "cc-fied"
    /// the particle properties service
    mutable const LHCb::IParticlePropertySvc* m_ppSvc; // the service
    // ========================================================================
  };
  // ==========================================================================
} // end of namespace LoKi
// ============================================================================
DECLARE_COMPONENT( LoKi::CCTest )
// ============================================================================
// The END
// ============================================================================
