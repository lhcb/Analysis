/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// Event
// ============================================================================
#include "Event/Track.h"
// ============================================================================
#include "TrackInterfaces/ITrackSelector.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/Algo.h"
#include "LoKi/ParticleCuts.h"
#include "LoKi/TrackCuts.h"
// ============================================================================
/** @file
 *  Simple algorithm to illustrate the
 *  selection of tracks
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2007-10-04
 */
// ============================================================================
namespace LoKi {
  class SelectTracks : public LoKi::Algo {
  public:
    /// the main method
    StatusCode analyse() override;

    SelectTracks( const std::string& name, ISvcLocator* pSvc ) : LoKi::Algo( name, pSvc ) {
      declareProperty( "TrackSelector", m_trSelectorName );
    }

  private:
    ITrackSelector* m_trSelector     = nullptr;
    std::string     m_trSelectorName = "LoKi::Hybrid::TrackSelector";
  };
} // namespace LoKi
// ============================================================================
DECLARE_COMPONENT( LoKi::SelectTracks )
// ============================================================================
// ============================================================================
// the main method
// ============================================================================
StatusCode LoKi::SelectTracks::analyse() {

  // locate tool (if not done already)
  if ( !m_trSelector ) { m_trSelector = tool<ITrackSelector>( m_trSelectorName, this ); }

  // locate the tracks
  auto tracks = get<LHCb::Track::Container>( LHCb::TrackLocation::Default );

  /// count "good tracks"
  size_t nTracks = std::count_if( tracks->begin(), tracks->end(),
                                  [&]( const LHCb::Track* track ) { return track && m_trSelector->accept( *track ); } );

  always() << " # of \"good\"tracks is " << nTracks << " from " << tracks->size() << endmsg;

  counter( "#good" ) += nTracks;

  setFilterPassed( true );

  return StatusCode::SUCCESS;
}
// ============================================================================
// The END
// ============================================================================
