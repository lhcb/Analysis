###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
if(NOT COMMAND lhcb_find_package)
  # Look for LHCb find_package wrapper
  find_file(LHCbFindPackage_FILE LHCbFindPackage.cmake)
  if(LHCbFindPackage_FILE)
      include(${LHCbFindPackage_FILE})
  else()
      # if not found, use the standard find_package
      macro(lhcb_find_package)
          find_package(${ARGV})
      endmacro()
  endif()
endif()

# -- Public dependencies
lhcb_find_package(Phys REQUIRED)

find_package(Python REQUIRED
    Interpreter Development
)
find_package(ROOT REQUIRED
    Core
    Gpad
    Hist
    MathCore
    RooFit
    ROOTTPython
    TMVA
    Tree
    TreePlayer
)

# -- Private dependencies
if(WITH_Analysis_PRIVATE_DEPENDENCIES)
    find_package(AIDA REQUIRED)
    find_package(Boost REQUIRED
        headers
        regex
    )
    find_package(FastJet REQUIRED)
    find_package(FJContrib REQUIRED)
    find_package(GSL REQUIRED)
    find_package(HepMC REQUIRED)
    find_package(ROOT REQUIRED
        GenVector
        Matrix
        Physics
        RIO
    )
endif()
