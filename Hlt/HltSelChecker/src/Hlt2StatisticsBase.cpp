/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "Kernel/IANNSvc.h"

// local
#include "Hlt2StatisticsBase.h"

//-----------------------------------------------------------------------------
// Implementation file for class : Hlt2StatisticsBase
//
// 2008-06-23 : Patrick Koppenburg
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
Hlt2StatisticsBase::Hlt2StatisticsBase( const std::string& name, ISvcLocator* pSvcLocator )
    : GaudiAlgorithm( name, pSvcLocator ), m_algoCorr() {
  declareProperty( "Hlt2SelectionID", m_hlt2SelectionID = "Hlt2SelectionID" );
}
//=============================================================================
// Destructor
//=============================================================================
Hlt2StatisticsBase::~Hlt2StatisticsBase() {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode Hlt2StatisticsBase::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;              // error printed already by GaudiAlgorithm

  if ( msgLevel( MSG::DEBUG ) ) debug() << "Hlt2StatisticsBase ==> Initialize" << endmsg;
  m_algoCorr = tool<IAlgorithmCorrelations>( "AlgorithmCorrelations", this );
  return StatusCode::SUCCESS;
}

//=========================================================================
//  get selections
//=========================================================================
strings Hlt2StatisticsBase::getSelections() const {
  strings                              tofill;
  std::vector<IANNSvc::minor_key_type> keys = svc<IANNSvc>( "HltANNSvc" )->keys( m_hlt2SelectionID );
  tofill.reserve( keys.size() );
  for ( std::vector<IANNSvc::minor_key_type>::const_iterator i = keys.begin(); i != keys.end(); ++i )
    tofill.push_back( *i );
  return tofill;
}
//=============================================================================
//  Finalize
//=============================================================================
StatusCode Hlt2StatisticsBase::finalize() {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Finalize" << endmsg;
  StatusCode sc = m_algoCorr->printTable();
  if ( !sc ) return sc;
  //  sc = m_algoCorr->printList() ;
  //  if (!sc) return sc;

  return GaudiAlgorithm::finalize(); // must be called after all other actions
}

//=============================================================================
//=============================================================================
// Main execution
//=============================================================================
StatusCode Hlt2StatisticsBase::execute() {
  err() << "Hlt2StatisticsBase:: This is a base class. Do not invoke." << endmsg;
  return StatusCode::FAILURE;
}
