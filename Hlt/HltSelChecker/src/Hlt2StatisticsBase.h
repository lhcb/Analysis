/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: Hlt2StatisticsBase.h,v 1.4 2009-10-16 11:29:32 graven Exp $
#ifndef HLT2STATISTICSBASE_H
#define HLT2STATISTICSBASE_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "Kernel/IAlgorithmCorrelations.h" // Interface
/** @class Hlt2StatisticsBase Hlt2StatisticsBase.h
 *
 *  Common base class for Hlt2 algorithms making stats
 *
 *  @author Patrick Koppenburg
 *  @date   2008-06-23
 */
typedef std::vector<std::string> strings;
class Hlt2StatisticsBase : public GaudiAlgorithm {
public:
  /// Standard constructor
  Hlt2StatisticsBase( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~Hlt2StatisticsBase(); ///< Destructor

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

protected:
  IAlgorithmCorrelations* m_algoCorr; ///< Correlation tool

  strings getSelections() const;

private:
  std::string m_hlt2SelectionID; ///< majore key in ANNSvc
};
#endif // HLT2STATISTICSBASE_H
