/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef HLTCORRELATIONS_H
#define HLTCORRELATIONS_H 1

// Include files
// from Hlt
#include "HltSelectionsBase.h"
/** @class HltCorrelations HltCorrelations.h
 *
 *  A more advanced SelResultsCorrelations for the HLT
 *
 *  @author Patrick Koppenburg
 *  @date   2007-11-23
 */
class HltCorrelations : public HltSelectionsBase {
public:
  /// Standard constructor
  HltCorrelations( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

private:
  /// shortcut
  StatusCode fillResult( std::string sel, const LHCb::HltDecReports* decReports ) {
    return ( m_algoCorr->fillResult(
        sel, ( decReports->decReport( sel ) ) ? ( decReports->decReport( sel )->decision() ) : 0 ) );
  }
  // bitx
  std::string bitX( unsigned int i ) { return "Bit" + std::to_string( i ); }

private:
  StatusCode moreAlgorithms();                     ///< Fill more algorithms
  StatusCode createSelections( unsigned int TCK ); ///< get list of selections

  strings                         m_moreAlgorithms; ///< additional algorithms
  mutable IAlgorithmCorrelations* m_algoCorr;       ///< Correlation tool
  mutable ICheckSelResults*       m_selTool;        ///< Selection results tool
  unsigned int                    m_firstBit;       ///< first routing bit to look at
  unsigned int                    m_lastBit;        ///< last routing bit to look at
  bool                            m_first;          ///< first event?
};
#endif // HLTCORRELATIONS_H
